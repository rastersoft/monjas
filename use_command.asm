; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

; Manages the USE command

do_use:
    ld a, VERB_USE
    ld hl, command_use
    call print_verb
    call ask_inventory
    cp VERB_RETURN
    ret z
    cp OBJECT_RADAR
    jp z, use_radar
    cp OBJECT_REACTOR
    jp z, use_reactor
    cp OBJECT_RUBBER_CHICKEN
    jr z, use_rubber_chicken
    cp OBJECT_GALENA_DETECTOR
    jr z, use_galena_detector
    cp OBJECT_IODINE
    jp z, use_iodine
    cp OBJECT_DIARY
    jp z, use_diary
    cp OBJECT_PLANT
    jp z, use_chuck
    and a
    jr z, no_inventory
do_use_cant_do_that:
    call make_talk_main
    DEFW sentence_cant_do_that
    ret

no_inventory:
    call make_talk_main
    DEFW sentence_nothing_to_use
    ret

use_galena_detector:
    ld hl, flags5
    bit 7, (hl) ; FLAG5_7
    jr z, use_galena_detector_2
    res 7, (hl) ; turn it off
    ret
use_galena_detector_2:
    ld a, (flags3)
    bit 6, a ; FLAG3_6
    jr z, use_galena_detector_3
    call make_talk_main
    DEFW sentence_already_found_galena
    ret
use_galena_detector_3:
    ld hl, flags5
    set 7, (hl) ; FLAG5_7
    ret ; enable the detector

use_rubber_chicken:
    ld a, (object_detected)
    cp OBJECT_REACTOR
    jr z, use_rubber_chicken_with_reactor
    cp OBJECT_POT
    jr z, use_rubber_chicken_with_pot
    cp OBJECT_CHICKEN
    jp z, use_rubber_chicken_with_chicken
    call make_talk_main
    DEFW sentence_rubber_chicken_alone
    ret

use_rubber_chicken_with_pot:
    call task_make_talk_and_wait
    DEFW red_character
    DEFW sentence_rubber_chicken_pot
    call task_make_talk_and_wait
    DEFW main_character
    DEFW sentence_rubber_chicken_pot2
    call make_talk
    DEFW red_character
    DEFW sentence_rubber_chicken_pot3
    ret

use_rubber_chicken_with_reactor:
    ld a, (flags4)
    bit 7, a ; FLAG4_7
    jr nz, use_rubber_chicken_with_reactor2
    call make_talk
    DEFW yellow_character
    DEFW talk_yellow_7
    ret
use_rubber_chicken_with_reactor2:
    ld hl, flags5
    set 4, (hl) ; FLAG5_4 reactor starts to tremble
    ld a, (border_sound_byte)
    and 0x18
    or 0x02
    ld (border_sound_byte), a
    out (254),a
    ld hl, flags3
    res 1, (hl) ; FLAG3_1 we don't have the rubber chicken
    set 2, (hl) ; FLAG3_2 we put the rubber chicken in the reactor
    ld hl, do_repaint_flags
    set 4, (hl) ; new animation section REPFLAG_4
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_yellow_8
    call task_move_to_and_wait_for_walk
    DEFW main_character
    DEFW 0x2010
    scf
    call set_character_direction_and_position
    DEFW yellow_character
    DEFW 0x1C24
    call move_character_to
    DEFW yellow_character
    DEFW 0x1C10
    call task_move_to_and_wait_for_walk
    DEFW main_character
    DEFW 0x2028
    call task_make_talk_and_wait
    DEFW yellow_character
    DEFW talk_yellow_9
    call do_blank
    call do_unblank
    ld hl, flags5
    res 4, (hl) ; FLAG5_4 stop the reactor
    ld a, (border_sound_byte)
    and 0x18
    ld (border_sound_byte), a
    out (254),a
    set 5, (hl) ; FLAG5_5
    ld a, 255
    ld (engineer), a
    ld (engineer+6), a ; hide engineer sprite
    ld a, (yellow_character)
    and 0x3F
    ld (yellow_character), a
    ld a, 3
    ld (engineer_chicken), a
    ld b, 8
    call task_wait_B_frames
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_yellow_10
    call task_move_to_and_wait_for_walk
    DEFW main_character
    DEFW 0x2014
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_yellow_11
    ld hl, do_repaint_flags
    res 4, (hl) ; new animation section REPFLAG_4
    ret

use_reactor:
    call make_talk_main
    DEFW sentence_use_reactor
    ret

use_radar:
    ld a, (flags1)
    and 0x03
    cp 0x01 ; FLAG1_10 = 01
    jr z, use_radar1
    ; nothing in the radar
    call make_talk_main
    DEFW sentence_look_radar_empty2
    ret
use_radar1:
    ld a, (flags2)
    bit 3, a ; FLAGS2_3
    jr nz, use_radar2
    ; a ship is calling
    ld hl, flags6
    set 7, (hl) ; FLAG6_7
    call make_talk_main
    DEFW sentence_radar_task_for_green
    ret
use_radar2:
    ld hl, do_repaint_flags
    set 4, (hl) ; REPFLAG_4
    ld a, 255
    ld (entry_radar_screen), a
    ld a, (border_sound_byte)
    and 0x18
    ld (border_sound_byte), a
    out (254), a
    ; let's talk with the ship
    and a ; look left
    call set_character_direction_and_position
    DEFW main_character
    DEFW 0x4640
    call task_make_talk_and_wait
    DEFW main_character
    DEFW sentence_land_ship1
    call task_make_talk_and_wait
    DEFW radar_character
    DEFW sentence_land_ship2
    call task_make_talk_and_wait
    DEFW main_character
    DEFW sentence_land_ship3
    call task_make_talk_and_wait
    DEFW radar_character
    DEFW sentence_land_ship4
    call task_move_to_and_wait_for_walk
    DEFW main_character
    DEFW 0x244A
    ld a, 7
    ld (entry_spaceship), a
    ld (entry_spaceship2), a ; make visible the spaceship sprite
    ld b, 19
    ld c, 93 ; the spaceship enters
use_radar2b:
    ld a, c
    ld (entry_spaceship+2), a
    ld (entry_spaceship2+2), a
    push bc
    call task_yield ; move the spaceshipt and wait
    pop bc
    dec c
    djnz use_radar2b
    ld b, 12 ; wait one and half seconds
    call task_wait_B_frames
    ld a, 0x80 ; empty tile, and don't step over
    ld (map_array + 549), a
    ld (map_array + 550), a
    ld (map_array + 581), a
    ld (map_array + 582), a
    ld a, 6
    ld (captain_sprites), a
    ld a, 1
    ld (captain_sprites+6), a ; make visible the captain sprite
    ld hl, flags1
    set 1, (hl) ; FLAG1_1
    call refresh_objects_characters
    and a ; look to the left
    call set_character_direction_and_position
    DEFW white_character
    DEFW 0x1C4A
    call make_talk_main
    DEFW sentence_land_ship5
    ld b, 8
    call task_wait_B_frames
    call task_move_to_and_wait_for_walk
    DEFW white_character
    DEFW 0x1C42
    ld a, 255
    ld (captain_sprites), a
    ld (captain_sprites+6), a ; captain enters "the door"
    call task_wait_while_talking ; wait until MAGENTA ends talking
    ld b, 12
    call task_wait_B_frames
    call task_make_talk_and_wait
    DEFW main_character
    DEFW sentence_land_ship6
    ld b, 16
    call task_wait_B_frames
    call task_make_talk_and_wait
    DEFW white_character
    DEFW sentence_land_ship7
    ld b, 8
    call task_wait_B_frames
    ld a, 6
    ld (captain_sprites), a
    ld a, 1
    ld (captain_sprites+6), a ; make visible the captain sprite
    and a ; look to the right
    call set_character_direction_and_position
    DEFW white_character
    DEFW 0x1C42
    call task_move_to_and_wait_for_walk
    DEFW white_character
    DEFW 0x1C4A
    call task_make_talk_and_wait
    DEFW white_character
    DEFW sentence_land_ship8
    call task_make_talk_and_wait
    DEFW main_character
    DEFW sentence_land_ship6
    ld hl, do_repaint_flags
    res 4, (hl) ; REPFLAG_4
    ret

use_iodine:
    ld a, (character_detected)
    and a
    jr z, use_iodine_in_object
    cp CHARACTER_GREEN
    jr z, use_iodine_in_green
    cp CHARACTER_WHITE
    jr z, use_iodine_in_white
    call make_talk_main
    DEFW sentence_use_iodine_in_person
    ret
use_iodine_in_green:
    ld b, 6
    call task_wait_B_frames
    ld hl, flags6
    set 4, (hl) ; FLAG6_4 used iodine on GREEN
    ld a, 0x30 ; change legs color to yellow
    ld (radarist_sprites+9), a
    ld b, 6
    call task_wait_B_frames
    call task_make_talk_and_wait
    DEFW main_character
    DEFW sentence_green_iodine1
    call task_make_talk_and_wait
    DEFW green_character
    DEFW sentence_green_iodine2
    call make_talk_main
    DEFW sentence_green_iodine3
    ret

use_iodine_in_white:
    call make_talk_main
    DEFW sentence_use_iodine_in_white
    ret

use_iodine_in_object:
    call make_talk_main
    DEFW sentence_use_iodine_in_object
    ret


use_diary:
    ld a, (character_detected)
    and a
    jr z, use_diary_in_object
    cp CHARACTER_YELLOW
    jp nz, do_use_cant_do_that
    call task_make_talk_and_wait
    DEFW main_character
    DEFW sentence_eat_diary
    call make_talk_main
    DEFW sentence_eat_diary2
    ret
use_diary_in_object:
    ld a, (object_detected)
    cp OBJECT_RADAR
    jp nz, do_use_cant_do_that
    ld a, (flags4)
    bit 1, a ; FLAG4_1 check if we have the USB cable
    jr nz, use_diary2
    ld hl, flags6
    set 6, (hl) ; FLAG6_6 we tried to use the diary without the cable
    call make_talk_main
    DEFW sentence_need_usb
    ret
use_diary2:
    call make_talk_main
    DEFW sentence_transferred_data_to_diary
    ld hl, flags4
    set 2, (hl) ; FLAG4_2
    ret


use_rubber_chicken_with_chicken:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW sentence_swap_chicken1
    call make_talk
    DEFW red_character
    DEFW sentence_swap_chicken2
    ret

use_chuck:
    ld a, (object_detected)
    cp OBJECT_POT
    jr z, use_poor_chuck
    cp OBJECT_REACTOR
    jr z, use_poor_chuck
    jp do_use_cant_do_that
use_poor_chuck:
    call make_talk_main
    DEFW sentence_poor_chuck
    ret
