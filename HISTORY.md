# History of versions

* Version 1.6 (2021/05/01)
  * Fixed typo (cristal -> crystal)

* Version 1.5 (2021/04/15)
  * Now it uses Sinclair joystick instead of cursors

* Version 1.4 (2021/04/11)
  * Now it can work in "Classic 128K machines" in 128K mode.
  * Now it uses port $1D for the kepston joystick.

* Version 1.3 (2021/04/10)
  * Fixed artifacts when going up at the right border of the void

* Version 1.2 (2021/04/10)
  * Fixed bug when returning from GIVE command without giving an object

* Version 1.1 (2021/04/10)
  * Fixed typo (ussual -> usual)

* Version 1.0 (2021/04/10)
  * First public version
