; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

; Shows the text for conversations when needed

do_talk:
    ; search if there is a character talking
    ld a, (talking_time)
    and a
    ret z
    dec a
    ld (talking_time), a

IF DO_BOTTOM_BAR
    ; print bottom bar with remaining time
    ld hl, 0x57E0 ; last line of the screen
    ld (hl), 0
    ld c, 0
    ld d, a
    sra a
    sra a
    sra a
    and 0x1F
    jr z, do_talk5b
    ld b, a
    ld c, a
    ld a, 0xff
do_talk5:
    ld (hl), a
    inc hl
    djnz do_talk5
do_talk5b:
    ld a, d
    and 0x07
    jr z, do_talk6b
    ld b, a
    xor a
do_talk_6c:
    scf
    rra
    djnz do_talk_6c
do_talk6b:
    ld (hl),a
    inc hl
    ld a, 31
    sub c
    ld b, a
    xor a
do_talk6:
    ld (hl), a
    inc hl
    djnz do_talk6
ENDIF

    ; paint the text
do_talk7:
    ld a,(talking_lines)
    ld b, a
    ld ix, (talking_character)
    ld iy, current_map_x
    ; calculate the coordinates for the text
    bit 0, (ix+0) ; is it the main character?
    jr z, do_talk0
    ld a, (ix+2) ; current Y coordinate for main character
    inc a ; main character is one char lower
    jr do_talk0b
do_talk0:
    ld a, (ix+4) ; destination Y coordinate for other characters
do_talk0b:
    sub (iy+1) ; current_map_y
    add a, (iy+3) ; map_y_offset
    sub 6 ; put it over the character
    bit 7, a
    jr nz, do_talk2d ; if it is negative, put it at the top of the screen
    cp height-1 ; check if it is outside the screen
    jr c, do_talk2
    ld a, (talking_lines)
    sub height
    neg
    jr do_talk2a
do_talk2:
    sub b ; leave enough space for as many as "talking lines"
do_talk2a:
    bit 7, a ; if Y is negative, set to zero
    jr z, do_talk1
do_talk2d:
    xor a
do_talk1:
    ld d, a ; Y coordinate
    bit 0, (ix+0)
    jr z, do_talk2b
    ld a, (ix+1) ; current X coordinate for main character
    jr do_talk2c
do_talk2b:
    ld a, (ix+3) ; destination Y coordinate for other characters
do_talk2c:
    sub (iy+0) ; current_map_x
    add a, (iy+2) ; map_x_offset
    bit 7,a ; check if it is negative
    jr z, do_talk3
    xor a
do_talk3:
    cp (iy+13) ; maximum X coordinate for the text
    jr c, do_talk4
    ld a, (talking_max_x)
do_talk4:
    ld e, a
    ld hl, (talking_sentence)
    ld a, (talking_max_characters)
    add a, TALK_CHARS_PER_FRAME
    jr nc, do_talk8
    ld a, 255 ; if there is overflow, just set it to the max value
do_talk8:
    ld (talking_max_characters), a
    ld b, a
    ld a, (talking_color)
    jp do_print_sentence

; Makes the main character to talk. The sentence to say is
; set in a DEFW after the call
make_talk_main:
    pop hl
    ld e, (hl)
    inc hl
    ld d, (hl)
    inc hl
    push hl
    ex de, hl
    ld ix, main_character
    jp make_talk_hl

; Makes a character to talk. The character and the sentence to say are
; set in a DEFW after the call
; It is also possible to call to make_talk_hl, passing the sentence pointer in HL
; and the pointer to the character at IX
make_talk:
    pop hl
    ld a, (hl)
    ld ixl, a
    inc hl
    ld a, (hl)
    ld ixh, a
    inc hl
    ld e, (hl)
    inc hl
    ld d, (hl)
    inc hl
    push hl
    ex de, hl
make_talk_hl:
    call uncompress_sentence_in_buffer
    ld hl, sentence_buffer
    ld e, 31
    ld (talking_sentence), hl
    ld a, (ix+5)
    ld iyl, a
    ld a, (ix+6)
    ld iyh, a
    ld a, (ix+0)
    and 0x03
    cp 0x03
    jr nz, make_talk1
    ld a, (ix+3)
    jr make_talk5
make_talk1:
    ld a, (iy+3) ; read the replacement color
    and 0x38
    rrca
    rrca
    rrca ; move PAPER to INK
    set 6, a ; by default, set bright
    bit 2, a ; select the best PAPER color
    jr nz, make_talk5 ; black PAPER and BRIGHT for GREEN, CYAN, YELLOW and WHITE
    or 0x38 ; white PAPER for BLACK, BLUE, RED and MAGENTA
    ;and 0x3F ; remove the BRIGHT flag
make_talk5:
    ld (talking_color), a
    ld (talking_character), ix
    ; calculate maximum width and length of the sentence
    ; presume that, if a sentence has two lines, the second is always larger
    ld bc, 0 ; B maximum width; C number of characters.
    ld d, 1; D number of lines
make_talk_loop:
    ld a, (hl)
    inc hl
    and a
    jr z, make_talk3 ; end of sentence
    cp 10 ; carriage return
    jr nz, make_talk4
    ld a, 32
    sub b
    cp e
    jr nc, make_talk7
    ld e, a ; get the minimum X value
make_talk7:
    inc d
    ld b, -1
make_talk4:
    inc c
    inc b
    jp make_talk_loop
make_talk3:
    ld a, 32
    sub b
    cp e
    jr nc, make_talk8
    ld e, a ; get the minimum X value
make_talk8:
    ld a, c
    ld (talking_length), a
    srl c
    srl c ; 1/8 of second for each letter
    sub c ; 3 / 32 of second for each letter
    cp 16
    jr nc, make_talk6
    ld a, 16 ; at least two seconds
make_talk6:
    ld (talking_time), a
    ld a, d
    ld (talking_lines), a
    ld a, e
    ld (talking_max_x), a
    xor a
    ld (talking_max_characters), a
    ret


; It is called with several sentence pointers located after the CALL instruction,
; prints them in a frame, and allows the user to choose one.
; C register must contain how many sentences are in the list, and after each
; pointer there is an ID value (a single byte). If that value is zero, the sentence
; won't be printed; but if it is different, then that value will be returned in A
; on return, thus specifying which sentence was selected.

task_ask_talk:
    ld hl, do_repaint_flags
    res 0, (hl) ; disable the refresh to avoid partial updates REPFLAG_0
    call read_key
    and a
    jr nz, task_ask_talk ; wait until all keys are unpressed
    pop hl
    push hl ; get a pointer to the sentences
    ld d, 0 ; sentence counter
    ld b, c
task_ask9:
    inc hl
    inc hl ; jump over sentence pointer
    ld a, (hl) ; read ID
    inc hl
    and a
    jr z, task_ask8
    inc d
task_ask8:
    djnz task_ask9
    ; now d contains the number of active sentences
    ld c, d

    ld ixh, 6 ; color
    ld a, c
    push hl
    ld hl, do_repaint_flags
    bit 2, (hl) ; REPFLAG_2
    jr z, task_ask10
    add a, a ; reserve two lines per sentence
task_ask10:
    pop hl
    add a, 2 ; take into account the margins
    ld h, a
    ld l, 32
    push bc
    call frame_paint
    pop bc
    ld b, c ; separate current selection from number of sentences
task_ask_loop2:
    pop hl ; take the return address, which points to the first sentence's pointer
    push hl
    push bc ; preserve the number of sentences and the selection
    ld a, 12
    ld hl, do_repaint_flags
    bit 2, (hl) ; REPFLAG_2
    jr nz, task_ask12
    srl b
    jr nc, task_ask12
    inc b
task_ask12:
    sub b
    ld ixl, a ; Y coordinate
    pop bc
    pop hl
    push hl
    push bc

task_ask_loop:
    ld a, (hl)
    ld e, a
    inc hl
    ld a, (hl)
    ld d, a
    inc hl
    ld a, (hl)
    inc hl
    and a
    jr z, task_ask_loop ; ignore non-printable sentences
    ld iyh, a ; preserve the sentence ID in IYh
    push hl   ; preserve the current pointer to the list of sentences
    ex de, hl ; and move the sentence pointer from DE to HL
    ld d, ixl ; current Y coordinate
    inc ixl
    push hl
    ld hl, do_repaint_flags
    bit 2, (hl) ; REPFLAG_2
    jr z, task_ask11
    inc ixl
task_ask11:
    pop hl
    ld e, 1
    ld a, b
    cp c
    ld a, ixh
    jr nz, task_ask1
    or 0x80
    ld iyl, iyh ; copy the sentence ID into IYl to keep it
task_ask1:
    push iy
    push ix
    push bc
    call do_print_sentence_unlimited
    pop bc
    pop ix
    pop iy
    pop hl ; restore the pointer to the list of sentences
task_ask7:
    djnz task_ask_loop
    push hl
    ld hl, do_repaint_flags
    set 0, (hl) ; REPFLAG_0
task_ask3:
    bit 0, (hl) ; REPFLAG_0
    jr nz, task_ask3 ; wait until the menu is painted
    pop hl
;    ld b, 3
;task_ask2:
;    halt
;    djnz task_ask2 ; delay to reduce keyboard speed
    pop bc ; restore the number of sentences and the current selection
task_ask6:
    call read_key
    cp KEY_MENU
    jr z, task_selected
    cp KEY_UP
    jr nz, task_ask4
    ld a, c
    cp b
    jr z, task_ask6
    inc c
    jr task_ask_loop2
task_ask4:
    cp KEY_DOWN
    jr nz, task_ask6
    ld a, 1
    cp c
    jr z, task_ask6
    dec c
    jp task_ask_loop2

task_selected:
    ex (sp), hl ; store the return address, after the sentences
    ld a,iyl
    ret
