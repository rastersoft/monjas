; This file was generated by ZXSpriter
; Do not edit

;sprite_format 2

character_top_left:
    ;size 0x00
    DEFB 0x00
    DEFB 0x3f
    DEFB 0x40
    DEFB 0x4f
    DEFB 0x5f
    DEFB 0x5c
    DEFB 0x58
    DEFB 0x58

character_top:
    ;size 0x00
    DEFB 0x00
    DEFB 0xff
    DEFB 0x00
    DEFB 0xff
    DEFB 0xff
    DEFB 0x00
    DEFB 0x00
    DEFB 0x00

character_top_right:
    ;size 0x00
    DEFB 0x00
    DEFB 0xfc
    DEFB 0x02
    DEFB 0xf2
    DEFB 0xfa
    DEFB 0x3a
    DEFB 0x1a
    DEFB 0x1a

character_left:
    ;size 0x00
    DEFB 0x58
    DEFB 0x58
    DEFB 0x58
    DEFB 0x58
    DEFB 0x58
    DEFB 0x58
    DEFB 0x58
    DEFB 0x58

character_right:
    ;size 0x00
    DEFB 0x1a
    DEFB 0x1a
    DEFB 0x1a
    DEFB 0x1a
    DEFB 0x1a
    DEFB 0x1a
    DEFB 0x1a
    DEFB 0x1a

character_bottom_left:
    ;size 0x00
    DEFB 0x58
    DEFB 0x58
    DEFB 0x5c
    DEFB 0x5f
    DEFB 0x4f
    DEFB 0x40
    DEFB 0x3f
    DEFB 0x00

character_bottom:
    ;size 0x00
    DEFB 0x00
    DEFB 0x00
    DEFB 0x00
    DEFB 0xff
    DEFB 0xff
    DEFB 0x00
    DEFB 0xff
    DEFB 0x00

character_bottom_right:
    ;size 0x00
    DEFB 0x1a
    DEFB 0x1a
    DEFB 0x3a
    DEFB 0xfa
    DEFB 0xf2
    DEFB 0x02
    DEFB 0xfc
    DEFB 0x00

