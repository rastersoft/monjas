; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

task_conversations:
    call task_yield
    ld a, (conversation_id)
    and a
    jp z, task_conversations
    ld hl, do_repaint_flags
    set 3, (hl) ; we are in the middle of a conversation REPFLAG_3
    set 2, (hl) ; conversations use double height REPFLAG_2
    cp CHARACTER_RED
    jp z, task_conversations_red
    cp CHARACTER_BLUE
    jp z, task_conversations_blu
    cp CHARACTER_GREEN
    jp z, task_conversations_green
    cp CHARACTER_YELLOW
    jp z, task_conversations_yellow
    cp CHARACTER_WHITE
    jp z, task_conversations_white
    cp CHARACTER_MARVIN
    jp z, task_conversations_marvin
task_conversations_ret:
    ld hl, do_repaint_flags
    res 3, (hl) ; REPFLAG_3
    xor a
    ld (conversation_id), a
    jp task_conversations


; Conversation with YELLOW

task_conversations_yellow:
    call task_yield
    call show_sentence_on_flag
    DEFW flags3
    DEFB 0x80 ; FLAG3_7
    DEFB 0x02
    DEFW yellow_id1+2

    ld c, 3
    call task_ask_talk
    DEFW talk_yellow_1
    DEFB 1
yellow_id1:
    DEFW talk_yellow_3
    DEFB 2
    DEFW talk_yellow_5
    DEFB 3
    cp 1
    jr z, task_conversations_yellow1
    cp 2
    jr z, task_conversations_yellow2
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_yellow_5
    call task_make_talk_and_wait
    DEFW yellow_character
    DEFW talk_yellow_6
    jp task_conversations_ret

task_conversations_yellow1:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_yellow_1
    call task_make_talk_and_wait
    DEFW yellow_character
    DEFW talk_yellow_2
    jp task_conversations_yellow

task_conversations_yellow2:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_yellow_3
    ld hl, flags4
    set 7, (hl)    ; FLAG4_7
    ld hl, do_repaint_flags
    set 6, (hl) ; cancel waiting of Yellow
    call refresh_objects_characters
    jp task_conversations_ret

; Conversation with GREEN

task_conversations_green:
    call task_yield
    ld a, (flags1)
    ; ask for the spaceship arriving
    call set_sentence_id
    DEFB 0x03 ; FLAG1_10
    DEFB 0x01 ; if bit 0 set and bit 1 reset, show sentence 3
    DEFB 0x03
    DEFB 0x00
    DEFW green_id1+2
    ; ask why is drunk
    call show_sentence_on_flag
    DEFW flags1
    DEFB 0x80 ; FLAG1_7
    DEFB 0x02
    DEFW green_id0+2
    ; comment that BLUE found the alcohol storage
    ; check if we have the galena and we know that we have to clean the galena
    call show_sentence_on_flag
    DEFW flags1
    DEFB 0x20 ; FLAG1_5
    DEFB 5
    DEFW green_id3+2
    ld c, 6
    call task_ask_talk
    DEFW talk_green_1
    DEFB 1
green_id0:
    DEFW talk_green_3
    DEFB 2
green_id1:
    DEFW talk_green_7
    DEFB 3
    DEFW talk_green_10
    DEFB 4
green_id3:
    DEFW talk_green_12
    DEFB 5
    DEFW talk_green_18
    DEFB 6
    cp 1
    jr z, task_conversations_green1
    cp 2
    jr z, task_conversations_green2
    cp 3
    jr z, task_conversations_green3
    cp 4
    jr z, task_conversations_green4
    cp 5
    jp z, task_conversations_green5
    ; 6
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_green_18
    call task_make_talk_and_wait
    DEFW green_character
    DEFW talk_green_19
    jp task_conversations_ret

task_conversations_green1:
    ; hello
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_green_1
    call task_make_talk_and_wait
    DEFW green_character
    DEFW talk_green_2
    ; now we know that green is drunk
    ld hl, flags1
    set 7, (hl) ; FLAG1_7
    jp task_conversations_green

task_conversations_green2:
    ; drunk again?
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_green_3
    call task_make_talk_and_wait
    DEFW green_character
    DEFW talk_green_4
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_green_5
    call task_make_talk_and_wait
    DEFW green_character
    DEFW talk_green_6
    jp task_conversations_green

task_conversations_green3:
    ; there is a ship in the radar
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_green_7
    call task_make_talk_and_wait
    DEFW green_character
    DEFW talk_green_8
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_green_9
    ; now we know that green can't land a ship
    ; and that it is drunk
    ld hl, flags1
    set 2, (hl) ; FLAG1_2
    set 7, (hl) ; FLAG1_7
    jp task_conversations_green

task_conversations_green4:
    ; can I drink too?
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_green_10
    call task_make_talk_and_wait
    DEFW green_character
    DEFW talk_green_11
    jp task_conversations_green


task_conversations_green5:
    ; BLUE "found the alcohol"
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_green_12
    call task_make_talk_and_wait
    DEFW green_character
    DEFW talk_green_15
    call move_character_to
    DEFW green_character
    DEFW 0x4828
    xor a
    ld (current_key), a
task_conversations_green5_0:
    call task_yield
    ; paint tiles wit iodine if needed
    ld a, (flags6)
    bit 4, a ; FLAG6_4
    jr z, task_conversations_green5_3
    ld a, (green_character)
    and 0xC0
    jr nz, task_conversations_green5_3 ; only change on animation step 0
    ld a, (green_character+1) ; X coord
    rrca
    rrca ; divide the X coordinate between 4
    bit 0, a
    jr z, task_conversations_green5_3 ; only paint odd tiles
    ld e, a
    ld d, 0
    ld hl, 320
    add hl, de
    ld de, map_array
    add hl, de
    ld (hl), TILE_INDEX_FLOOR_IODINE ; change the tile
task_conversations_green5_3:
    ld a, (current_key)
    and a
    jr nz, task_conversations_green5_1
    ld a, (green_character)
    bit 2, a
    jr nz, task_conversations_green5_0
    ld a, (flags6)
    bit 4, a ; FLAG6_4                ; only set the grid as "here is the booze"
    jr z, task_conversations_green5_4 ; if GREEN has the iodine
    ld hl, 352
    ld de, map_array
    add hl, de
    ld de, 17
    add hl, de
    ld (hl), TILE_INDEX_GRID_YELLOW ; set the grid with the booze
    ld hl, flags6
    set 5, (hl) ; FLAG6_5   mark that GREEN revealed where the booze is
    ; go back again
task_conversations_green5_4:
    call move_character_to
    DEFW green_character
    DEFW 0x1828
    call task_make_talk_and_wait
    DEFW green_character
    DEFW talk_green_17
    ld ix, green_character
    call task_wait_for_walk
    jp task_conversations_green
task_conversations_green5_1:
    ; the user pressed a key while GREEN was moving
    call task_yield
    ld a, (green_character)
    and 0xC0
    jr nz, task_conversations_green5_1
task_conversations_green5_2:
    call move_character_to
    DEFW green_character
    DEFW 0x1828
    call task_make_talk_and_wait
    DEFW green_character
    DEFW talk_green_16
    ld ix, green_character
    call task_wait_for_walk
    jp task_conversations_green



; Conversation with RED

task_conversations_red:
    call task_yield
    ld a, (flags2)
    ; ask to invite WHITE
    call set_sentence_id
    DEFB 0x03 ; FLAG2_10
    DEFB 0x01 ; if WHITE is waiting for food and still doesn't have it
    DEFB 0x03
    DEFB 0x00
    DEFW red_id0+2
    ; ask to use the oven
    call set_sentence_id
    DEFB 0x03 ; FLAG2_10
    DEFB 0x01 ; if WHITE is waiting for food and still doesn't have it
    DEFB 0x04
    DEFB 0x00
    DEFW red_id1+2
    ; ask to use the chicken
    call set_sentence_id
    DEFB 0x03 ; FLAG2_10
    DEFB 0x01 ; if WHITE is waiting for food and still doesn't have it
    DEFB 0x06
    DEFB 0x00
    DEFW red_id3+2

    ; ask for the pendrive
    call show_sentence_on_flag
    DEFW flags2
    DEFB 0x04 ; FLAG2_2
    DEFB 0x05
    DEFW red_id2+2

    call show_sentence_on_flag
    DEFW flags6
    DEFB 0x40 ; FLAG6_6
    DEFB 0x08 ; check if have tried to use the diary with the radar
    DEFW red_id4+2

    ld a, (flags4)
    call set_sentence_id
    DEFB 0x01
    DEFB 0x01 ; FLAG4_0 check if the cable is in the locker
    DEFB 0xFF ; if it is in the locker, keep current the sentence ID
    DEFB 0x00
    DEFW red_id4+2

    ld c, 8
    call task_ask_talk
    DEFW talk_red_1
    DEFB 1
    DEFW talk_red_3
    DEFB 2
red_id0:
    DEFW talk_red_18
    DEFB 3
red_id1:
    DEFW talk_red_5
    DEFB 4
red_id2:
    DEFW talk_red_8
    DEFB 5
red_id4:
    DEFW talk_red_10
    DEFB 8
red_id3:
    DEFW talk_red_12
    DEFB 6
    DEFW talk_red_16
    DEFB 7
    cp 1
    jr z, task_conversations_red1
    cp 2
    jr z, task_conversations_red2
    cp 3
    jp z, task_conversations_red7
    cp 4
    jr z, task_conversations_red3
    cp 5
    jr z, task_conversations_red4
    cp 6
    jp z, task_conversations_red6
    cp 8
    jp z, task_conversations_red5
    ; 7
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_red_16
    call task_make_talk_and_wait
    DEFW red_character
    DEFW talk_red_17
    jp task_conversations_ret

task_conversations_red1:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_red_1
    call task_make_talk_and_wait
    DEFW red_character
    DEFW talk_red_2
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_red_2b
    call task_make_talk_and_wait
    DEFW red_character
    DEFW talk_red_2c
    jp task_conversations_red

task_conversations_red2:
    ; asks for dinner time
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_red_3
    call task_make_talk_and_wait
    DEFW red_character
    DEFW talk_red_4
    jp task_conversations_red

task_conversations_red3:
    ; asks to use the oven
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_red_5
    call task_make_talk_and_wait
    DEFW red_character
    DEFW talk_red_6
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_red_7
    ld hl, flags3
    set 7, (hl) ; FLAG3_7
    jp task_conversations_red

task_conversations_red4:
    ; asks for USB pendrive
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_red_8
    call task_make_talk_and_wait
    DEFW red_character
    DEFW talk_red_9
    jp task_conversations_red

task_conversations_red5:
    ; asks for USB cable
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_red_10
    call task_make_talk_and_wait
    DEFW red_character
    DEFW talk_red_11
    ; red allows us to take the USB cable
    ld hl, flags2
    set 5, (hl) ; FLAG2_5
    jp task_conversations_red

task_conversations_red6:
    ; can i take the chicken?
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_red_12
    call task_make_talk_and_wait
    DEFW red_character
    DEFW talk_red_13
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_red_14
    call task_make_talk_and_wait
    DEFW red_character
    DEFW talk_red_15
    jp task_conversations_red

task_conversations_red7:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_red_18
    call task_make_talk_and_wait
    DEFW red_character
    DEFW talk_red_19
    jp task_conversations_red

; Conversation with BLUE

task_conversations_blu:
    call task_yield
    ld a, (flags1)
    ; ask for the spaceship arriving
    call set_sentence_id
    DEFB 0x03 ; FLAG1_10
    DEFB 0x01 ; if bit 0 set and bit 1 reset, show sentence 3
    DEFB 0x03
    DEFB 0x00
    DEFW blu_id1+2
    ; set galena sentence
    call set_sentence_id
    DEFB 0x10 ; FLAG1_4
    DEFB 0x10
    DEFB 0x05
    DEFB 0x00
    DEFW blu_id2+2
    ; set alcohol sentence
    call set_sentence_id
    DEFB 0x20 ; FLAG1_5
    DEFB 0x20
    DEFB 0x06
    DEFB 0x00
    DEFW blu_id3+2

    ld c, 7
    call task_ask_talk
    DEFW talk_blu_1 ; hello
    DEFB 1
    DEFW talk_blu_7 ; what are you doing?
    DEFB 4
    DEFW talk_blu_3 ; Yellow deleted the diary
    DEFB 2
blu_id1:
    DEFW talk_blu_5 ; there is a ship
    DEFB 3
blu_id2:
    DEFW talk_blu_9 ; do we have galena?
    DEFB 5
blu_id3:
    DEFW talk_blu_11 ; do we have alcohol?
    DEFB 6
    DEFW talk_blu_17 ; bye
    DEFB 7
    cp 1
    jr z, task_conversations_blu1
    cp 2
    jr z, task_conversations_blu2
    cp 3
    jr z, task_conversations_blu3
    cp 4
    jr z, task_conversations_blu4
    cp 5
    jr z, task_conversations_blu5
    cp 6
    jp z, task_conversations_blu6
    ; 7
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_blu_17
    call task_make_talk_and_wait
    DEFW blue_character
    DEFW talk_blu_18
    jp task_conversations_ret

task_conversations_blu1:
    ; hello
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_blu_1
    call task_make_talk_and_wait
    DEFW blue_character
    DEFW talk_blu_2
    jp task_conversations_blu

task_conversations_blu2:
    ; yellow deleted the diary
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_blu_3
    call task_make_talk_and_wait
    DEFW blue_character
    DEFW talk_blu_4
    jp task_conversations_blu

task_conversations_blu3:
    ; there is a ship
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_blu_5
    call task_make_talk_and_wait
    DEFW blue_character
    DEFW talk_blu_6
    ld a, (flags1) ; FLAG1_2
    bit 2, a ; check if we know that GREEN is too much drunk
    jp z, task_conversations_blu
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_blu_6b
    call task_make_talk_and_wait
    DEFW blue_character
    DEFW talk_blu_6c
    ld hl, flags2 ; FLAG2_3
    set 3, (hl) ; now we can order the spaceship to land
    jp task_conversations_blu

task_conversations_blu4:
    ; what are you doing?
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_blu_7
    call task_make_talk_and_wait
    DEFW blue_character
    DEFW talk_blu_8
    jp task_conversations_blu

task_conversations_blu5:
    ; do we have galena?
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_blu_9
    call task_make_talk_and_wait
    DEFW blue_character
    DEFW talk_blu_10
    jp task_conversations_blu

task_conversations_blu6:
    ; do we have alcohol?
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_blu_11
    call task_make_talk_and_wait
    DEFW blue_character
    DEFW talk_blu_12
    ld hl, flags6
    set 3, (hl) ; FLAG6_3 Now we have IODINE
    jp task_conversations_blu


task_conversations_white:
    call task_yield
    ; ask to use the chicken
    ld a, (flags2)
    call set_sentence_id
    DEFB 0x02 ; FLAG2_10
    DEFB 0x02 ; if WHITE has eaten
    DEFB 0x00
    DEFB 0x03
    DEFW white_id1+2
    call set_sentence_id
    DEFB 0x02 ; FLAG2_10
    DEFB 0x02 ; if WHITE has eaten
    DEFB 0x00
    DEFB 0x04
    DEFW white_id2+2
    call set_sentence_id
    DEFB 0x02 ; FLAG2_10
    DEFB 0x02 ; if WHITE has eaten
    DEFB 0x00
    DEFB 0x05
    DEFW white_id3+2
    call set_sentence_id
    DEFB 0x02 ; FLAG2_10
    DEFB 0x02 ; if WHITE has eaten
    DEFB 0x06
    DEFB 0x00
    DEFW white_id4+2
    ld c, 7
    call task_ask_talk
    DEFW talk_white_1
    DEFB 1
    DEFW talk_white_3
    DEFB 2
white_id1:
    DEFW talk_white_5
    DEFB 3
white_id2:
    DEFW talk_white_7
    DEFB 4
white_id3:
    DEFW talk_white_11
    DEFB 5
white_id4:
    DEFW talk_white_13
    DEFB 6
    DEFW talk_white_21
    DEFB 7
    cp 1
    jr z, task_conversations_white1
    cp 2
    jr z, task_conversations_white2
    cp 3
    jr z, task_conversations_white3
    cp 4
    jr z, task_conversations_white4
    cp 5
    jr z, task_conversations_white5
    cp 6
    jp z, task_conversations_white6
    ; 7
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_21
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_22
    jp task_conversations_ret

task_conversations_white1:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_1
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_2
    jp task_conversations_white

task_conversations_white2:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_3
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_4
    jp task_conversations_white

task_conversations_white3:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_5
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_6
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_6b
    jp task_conversations_white

task_conversations_white4:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_7
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_8
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_9
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_10
    jp task_conversations_white

task_conversations_white5:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_11
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_12
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_ill_see_what_i_can_do
    ld hl, flags2
    set 0, (hl) ; FLAG2_0
    jp task_conversations_white

task_conversations_white6:
    ; ask white what things are needed to take us
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_13
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_14
    ld a, (flags3)
    bit 5, a ; FLAG3_5
    jr nz, task_conversations_white6a
    ; a galena cristal
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_15
task_conversations_white6a:
    ld a, (flags3)
    bit 4, a ; FLAG3_4
    jr nz, task_conversations_white6b
    ld a, (flags1)
    bit 5, a ; FLAG1_5
    jr z, task_conversations_white6b
    ; alcohol
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_16
task_conversations_white6b:
    ld a, (flags4)
    bit 3, a ; FLAG4_3
    jr nz, task_conversations_white6c
    ; stelar map
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_17
task_conversations_white6c:
    ld a, (flags4)
    bit 6, a ; FLAG4_6
    jr nz, task_conversations_white6d
    ; oxygen generator
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_18
task_conversations_white6d:
    jp task_conversations_white


; Conversation with MARVIN

task_conversations_marvin:
    call task_yield

    call show_sentence_on_flag
    DEFW flags1
    DEFB 0x10 ; FLAG1_4
    DEFB 3
    DEFW marvin_id1+2

    ld c, 4
    call task_ask_talk
    DEFW talk_marvin_1
    DEFB 1
    DEFW talk_marvin_3
    DEFB 2
marvin_id1:
    DEFW talk_marvin_9
    DEFB 3
    DEFW talk_marvin_14
    DEFB 4
    cp 1
    jr z, task_conversations_marvin1
    cp 2
    jr z, task_conversations_marvin2
    cp 3
    jr z, task_conversations_marvin3
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_marvin_14
    call task_make_talk_and_wait
    DEFW marvin_character
    DEFW talk_marvin_15
    jp task_conversations_ret

task_conversations_marvin1:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_marvin_1
    call task_make_talk_and_wait
    DEFW marvin_character
    DEFW talk_marvin_2
    jp task_conversations_marvin


task_conversations_marvin2:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_marvin_3
    call task_make_talk_and_wait
    DEFW marvin_character
    DEFW talk_marvin_4
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_marvin_5
    call task_make_talk_and_wait
    DEFW marvin_character
    DEFW talk_marvin_6
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_marvin_7
    call task_make_talk_and_wait
    DEFW marvin_character
    DEFW talk_marvin_8
    jp task_conversations_marvin

task_conversations_marvin3:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_marvin_9
    call task_make_talk_and_wait
    DEFW marvin_character
    DEFW talk_marvin_10
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_marvin_11
    call task_make_talk_and_wait
    DEFW marvin_character
    DEFW talk_marvin_12
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_marvin_13
    ld hl, flags2 ; we have now the galena detector
    set 4, (hl) ; FLAG2_4
    jp task_conversations_marvin
