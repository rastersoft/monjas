; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

include "definitions.asm"

org 23990
jp main

sentence_list:
include "sentences.asm"
font:
include "font.asm"
include "char_borders.asm"
include "commands.asm"
include "give_command.asm"
include "take_command.asm"
include "intro.asm"
include "menu.asm"
stack_limit:

org 32768
; from 32768 to 39679 is located the screen secondary buffer
BUFFER:
IF DODEBUG
    DEFS 6880
    DEFS 32,56
ELSE
    DEFS 6912
ENDIF

sprite_index:
old_stack:
    DEFW 0
; the table starts here because the 0 value is for "don't paint", so we use that
; first position, which will never be used for sprites, to store the current stack
; address when running the asynchronous tasks
    DEFW sprite_grid
    DEFW sprite_grid_yellow
    DEFW sprite_floor_outside
    DEFW sprite_floor_iodine
    DEFW sprite_floor1
    DEFW sprite_floor1_cut
    DEFW sprite_floor2
    DEFW sprite_floor2_cut
    DEFW sprite_floor3
    DEFW sprite_floor3_cut
    DEFW sprite_roof
    DEFW sprite_landing
    DEFW sprite_vert_wall
    DEFW sprite_horiz_wall
    DEFW sprite_vert_door2
    DEFW sprite_vert_door1
    DEFW sprite_t_wall_up
    DEFW sprite_t_wall_down
    DEFW sprite_t_wall_bottom_left
    DEFW sprite_t_wall_bottom_right
    DEFW sprite_t_wall_top_left
    DEFW sprite_t_wall_top_right
    DEFW sprite_t_wall_right
    DEFW sprite_t_wall_left
    DEFW sprite_table
    DEFW sprite_bed_left
    DEFW sprite_bed_right
    DEFW sprite_locker
    DEFW sprite_box
    DEFW sprite_bathroom
    DEFW sprite_kitchen
    DEFW sprite_medicine
    DEFW sprite_radioactive_container
    DEFW sprite_radar
    DEFW sprite_horiz_wall_broken
    DEFW sprite_floor_2b
    DEFW sprite_floor_1b
    DEFW sprite_locker_pink
    DEFW sprite_locker_open


feet_table:
    DEFW sprite_feet_right1
    DEFW sprite_feet_left1
    DEFW sprite_feet_right0
    DEFW sprite_feet_left2
    DEFW sprite_feet_right2
    DEFW sprite_feet_left0
    DEFW sprite_feet_right0
    DEFW sprite_feet_left2

screen_addresses:
    DEFW 0x4010, 0x4030, 0x4050, 0x4070
    DEFW 0x4090, 0x40b0, 0x40d0, im2_ret1
    DEFW 0x40f0, 0x4810, 0x4830, 0x4850
    DEFW 0x4870, 0x4890, 0x48b0, 0x48d0
    DEFW 0x48f0, 0x5010, 0x5030, 0x5050
    DEFW 0x5070, 0x5090, 0x50b0, 0x50d0
    DEFW im2_ret2

    DEFS 20 ; enough space for the ISR stack. Put BEFORE im2_stack tag because the stack grows downward
im2_stack:
    DEFW 0 ; will store the current stack address when entering the ISR

; these two addresses are used to emulate CALL/RET in the BUFFER COPY subroutine
stack_pt:
    DEFW 0
current_hl:
    DEFW 0

; stores the key pressed by the user in the keyboard
current_key:
    DEFB 0
last_key:
    DEFB 0
translated_key:
    DEFB 0

; these variables are used by the map painting subroutine, and they MUST be in
; this precise order because the code uses IX to address them. Also the variables
; up to TALKING_LINES
current_map_x:
    DEFB 0
current_map_y:
    DEFB 0
map_x_offset:
    DEFB 0
map_y_offset:
    DEFB 0
current_map_y_painting:
    DEFB 0

; the address of the tile in the map where the main character is.
; Useful to detect if the user is trying to move into a tile set as "don't step"
current_map_address:
    DEFW 0

; variables for talking people
talking_time:
    DEFB 0 ; if zero, no talk. If different, number of remaining frames talking
talking_color:
    DEFB 0 ; color for the talk
talking_sentence:
    DEFW 0 ; address of the sentence to show
talking_character:
    DEFW 0 ; address of the entry for the character that is talking
talking_max_x:
    DEFB 0 ; maximum X coordinate for the text
talking_lines:
    DEFB 0 ; number of lines to print
talking_max_characters:
    DEFB 0
print_max_characters:
    DEFB 0
talking_length:
    DEFB 0 ; number of characters in the current sentence

task_pointer:
    DEFW 0 ; used to store the pointer to the current task, thus freeing the IY register

character_detected:
    DEFB 0 ; the character that is near the main character, or 0 if no character is near
    DEFW 0 ; pointer to the name
object_detected:
    DEFB 0 ; the object that is near the main character, or 0 if no object is near
    DEFW 0 ; pointer to the name
last_writen:
    DEFB 255 ; last object or character id writen

conversation_id:
    DEFB 0; character with which the main character is conversating

seed:
    DEFW 0 ; seed for the RNG

final_map_address:
    DEFW 0 ; points to the map address currently occupied by the main character

do_repaint_flags: ; when this is set to 1, the ISR will copy the buffer
    DEFB 0 ; bit 0: 1 -> copy the secondary buffer into the main screen; 0 -> buffer copied
           ; bit 1: 0 -> don't paint the map; 1 -> paint the map
           ; bit 2: 0 -> sentence selector reserves 1 line per entry
           ;        1 -> sentence selector reserves 2 line per entry
           ; bit 3: the system is in the middle of a conversation
           ; bit 4: the system is in the middle of an animation
           ; bit 5: the system is inside the menu
           ; bit 6: 1->cancel task_wait_random
           ; bit 7: 1->do TAP sound

IF DO_DEBUG_FLAGS
flags1:
    DEFB 0x87
flags2:
    DEFB 0x09
flags3:
    DEFB 0x82
flags4:
    DEFB 0x11
flags5:
    DEFB 0x05
flags6:
    DEFB 0x80

ELSE
flags1:
    DEFB 0; bit 1_0: 00->waiting for spaceship
          ;          01->spaceship calling
          ;          11->spaceship landed
          ; bit 2: 1->asked GREEN to land spaceship but is drunk
          ; bit 3: 1->BLUE does know that GREEN is drunk
          ; bit 4: 1->we do know that we need galena, maps and oxigen
          ; bit 5: 1->we do know that we need alcohol
          ; bit 6: 1->we do know that we need password
          ; bit 7: 1->we do know that GREEN is drunk

flags2:
    DEFB 0 ; bit 1_0: 00->White hasn't asked for food
           ;          01->White is waiting for food
           ;          11->White already had food
           ; bit 2: 1->we already checked RED's locker
           ; bit 3: 1->BLUE asked us to land the spaceship
           ; bit 4: 1->We have the galena detector
           ; bit 5: 1->RED allows us to take the USB cable
           ; bit 6: 1->YELLOW went to the kitchen
           ; bit 7: 1->we gave YELLOWchicken to WHITE


flags3:
    DEFB 0x01 ; bit 0: 1->rubber chicken is in the locker
              ; bit 1: 1->we have the rubber chicken
              ; bit 2: 1->we put the rubber chicken in the reactor and YELLOW is cooked
              ; bit 3: 1->we have alcohol
              ; bit 4: 1->we gave alcohol to white
              ; bit 5: 1->we gave the galena cristal to WHITE
              ; bit 6: 1->we have took a galena cristal
              ; bit 7: 1->we have asked RED about the oven

flags4:
    DEFB 0x11 ; bit 0: 1->the USB cable is in the locker
              ; bit 1: 1->we have the USB cable
              ; bit 2: 1->the diary has the maps
              ; bit 3: 1->WHITE has the diary
              ; bit 4: 1->the plant is in the floor
              ; bit 5: 1->we have the plant
              ; bit 6: 1->WHITE has the plant
              ; bit 7: 1->YELLOW has to go to the kitchen

flags5:
    DEFB 0x05 ; bit 0: 1->the diary is in the locker
              ; bit 1: 1->we have the diary
              ; bit 2: 1->the galena diode is in the locker
              ; bit 3: 1->we have the galena diode
              ; bit 4: 1->the reactor is trembbling
              ; bit 5: 1->yellowchicken is in the floor
              ; bit 6: 1->we have yellowchicken
              ; bit 7: 1->the galena detector is ON

flags6:
    DEFB 0x00 ; bit 0: 1->already looked the galena detector
              ; bit 1: 1->we are over a galena cristal
              ; bit 2: 1->we have the galena cristal
              ; bit 3: 1->we have iodine
              ; bit 4: 1->used iodine in GREEN
              ; bit 5: 1->GREEN revealed where the booze is
              ; bit 6: 1->we tried to take the maps without USB
              ; bit 7: 1->disable radar sound



ENDIF
timer_land:
    DEFB TIMER_LAND_INIT ; counts the number of seconds before starting the radar alarm

border_sound_byte:
    DEFB 7 ; stores the last value sent to the port 254

radar_character:
    ; fake character for the radar, to allow to make it "talk"
    DEFB 0x03 ; fake character, to ensure that it uses the "current X" and "current Y" coordinates, and color specified here
    DEFB 0x48
    DEFB 0x44
    DEFB 0x47 ; bright white

marvin_character:
    ; fake character for marvin, to allow to make it "talk"
    DEFB 0x03 ; fake character, to ensure that it uses the "current X" and "current Y" coordinates, and color specified here
    DEFB 0x08
    DEFB MAX_MAP_Y - 11
    DEFB 0x44

frame_counter:
    DEFB 0x00

last_print_coords:
    DEFW 0 ; used to print several sentences one after another

key_table:
    DEFB 0x82
    DEFB KEY_UP
    DEFB 0x41
    DEFB KEY_UP
    DEFB 0x84
    DEFB KEY_DOWN
    DEFB 0x21
    DEFB KEY_DOWN
    DEFB 0x88
    DEFB KEY_RIGHT
    DEFB 0xA1
    DEFB KEY_RIGHT
    DEFB 0x90
    DEFB KEY_LEFT
    DEFB 0xA2
    DEFB KEY_LEFT
    DEFB 0x81
    DEFB KEY_MENU
    DEFB 0xE1
    DEFB KEY_MENU
    DEFB 0xC1
    DEFB KEY_MENU
    DEFB 0xE4
    DEFB KEY_MENU
    DEFB 0x02
    DEFB KEY_MENU

; empty space to keep INTM2 aligned
debug_address:
    DEFS 2

; ----------------- this DEFW MUST be at 0x9BFF to ensure that the IM2 handle works ------------
; ----------------- so removing bytes before it must be compensated incrementing the previous DEFS

INTM2:
    DEFW im2m

task_list:
    TASK_ENTRY 40, task1
    TASK_ENTRY 14, task2
    TASK_ENTRY 10, task3
    TASK_ENTRY 12, task4
    TASK_ENTRY 36, task_conversations
    TASK_ENTRY 10, task5
    TASK_ENTRY 10, task6
    TASK_ENTRY 10, task7
    TASK_ENTRY 12, task8
    DEFB 0 ; end of task list

include "sprites_entries.asm"


; main code

main:
    ld sp, 32768
    call init_im2
    call initialize_characters
    ld a, 255
    ld (captain_sprites), a
    ld (captain_sprites+6), a ; make the captain invisible
    jp main_menu
main_intro:
    call do_intro
main_loop_init:
    ld a, 7
    call set_last_line_color
    call refresh_objects_characters
main_loop:
IF DODEBUG
    call reset_debug
ENDIF

    call paint_map

IF DEBUG_FLAGS
    ld a, (flags1)
    call do_debug
    ld a, (flags2)
    call do_debug
    ld a, (flags3)
    call do_debug
    ld a, (flags4)
    call do_debug
    ld a, (flags5)
    call do_debug
    ld a, (flags6)
    call do_debug
ENDIF

IF DO_DEBUG_STACK
    ld hl, stack_limit
    ld d, 0
check_stack_size:
    ld a, (hl)
    and a
    jr nz, end_check_stack_size
    inc d
    inc hl
    jr check_stack_size
end_check_stack_size:
    ld a, d
    call do_debug
ENDIF

    call do_talk
    ld a, (do_repaint_flags)
    bit 3, a ; REPFLAG_3
    jr nz, main_loop2 ; only advance the timer if we aren't in the middle of a conversation
    bit 5, a ; REPFLAG_5 or if we are outside the menu
    jr nz, main_loop2
    ld a, (flags1)
    bit 1, a ; FLAG1_1
    jr nz, main_loop2 ; or if the spaceship already has started landed
    ld a, (talking_time)
    and a
    jr nz, main_loop2 ; or if someone is talking
    ld hl, timer_land
    dec (hl)
    jr nz, main_loop2
    ld hl, flags1
    bit 0, (hl) ; FLAG1_0
    jr nz, main_loop2
    set 0, (hl) ; start to ring the radar alarm
    call make_talk_main
    DEFW sentence_a_ship
main_loop2:
    ld hl, do_repaint_flags
    set 0, (hl) ; REPFLAG_0
    ei
    bit 7, (hl)
    jr z, main_loop4
    res 7, (hl)
    ld a, (border_sound_byte)
    xor 0x18 ; make TAP sound
    ld (border_sound_byte), a
    out (254), a
main_loop4:
    call detect_objects_characters
    call task_run
    xor a
    ld (current_key), a ; reset the keys
    ld (translated_key), a
    call update_characters
main_loop3:
    ld hl, do_repaint_flags
main_loop1:
    bit 0, (hl) ; REPFLAG_0
    jr nz, main_loop1
    di
    jp main_loop


include "paint_sprite.asm"
include "int_im2.asm"
include "map.asm"
include "character_manager.asm"
include "task_manager.asm"
include "talk.asm"
include "utils.asm"
include "detect_objects.asm"
include "talk_command.asm"
include "look_command.asm"
include "use_command.asm"
include "conversations.asm"
include "end_animation.asm"
include "behavior_main_character.asm"
; graphics
include "spacesuit_interleaved.asm"

END main
