; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

init_im2:
    di
IF DODEBUG
    call reset_debug
ENDIF
    ld a, 0x9B
    ld i, a
    im 2
    ret

IF DODEBUG
reset_debug:
    push hl
    push bc
    ; paint the grid for debugging
    ld hl, 0x57E0
    ld a, 0x55
    ld b, 16
debug_reset:
    ld (hl), a
    inc hl
    inc hl
    djnz debug_reset
    ld hl, 0x56E0
    ld (debug_address), hl
    pop bc
    pop hl
    ret

do_debug:
    push hl
    ld hl, (debug_address)
    ld (hl), a
    inc hl
    inc hl
    ld (debug_address), hl
    pop hl
    ret
ENDIF

; ISR. It is called on the vertical retrace

im2m:
    ld (im2_stack), sp
    ld sp, im2_stack ; will decrease when doing PUSH, so it will be stored BEFORE
    push af
    push bc
    push de
    push ix


; always read the keyboard, but store only if there is a key pressed.
; current_key must be reset after executing the tasks.
; This also works as an anti-bouncing.

im2_read_keyboard:
    ld bc, 0xFEFE
    ld de, 0
im2_loop:
    in a, (c)
    cpl
    and 0x1F
    jp z, im2_loop2
    or d
    ld e, a
im2_loop2:
    rlc b
    ld a, 0x20
    add a, d
    ld d, a
    jp nc,im2_loop
    ld a, e
IF DODEBUG
    ld (0x56FE), a ; pressed key
ENDIF
    and a
    jp z, im2_nokey
    ld (current_key), a
im2_nokey:

    ; check if we have to repaint
    ld a, (do_repaint_flags)
    bit 0, a ; REPFLAG_0
    jp z, im2_exit

    push hl
    ld hl, frame_counter
    inc (hl)

    call translate_key

im2_nokey2:
; check if a TAP sound must be emmited

    ld de, CHARACTER_ENTRY_SIZE
    ld a, (main_character_x)
    ld b, a
    ld a, (main_character_y)
    ld c, a
    ld ix, main_character
    ld a, (ix+0)
im2_tap0:
    and 0xC0
    cp 0x40 ; do TAP in the second animation frame
    jr nz, im2_tap5
    ld a, (ix+1)
    sub b
    jp p, im2_tap1
    neg
im2_tap1:
    cp MAP_OFFSET_X
    jr nc, im2_tap5
    ld a, (ix+2)
    sub c
    jp p, im2_tap2
    neg
im2_tap2:
    cp MAP_OFFSET_Y
    jr nc, im2_tap5
    ld a, (do_repaint_flags)
    set 7, a ; REPFLAG_7
    ld (do_repaint_flags), a
    jp im2_tap3
im2_tap5:
    add ix, de
    ld a, (ix+0)
    and a
    jp nz, im2_tap0

im2_tap3:

    ld a, (border_sound_byte)
IF DOFPS
    or 0x80
    out (254), a
    ld a, (border_sound_byte)
    out (254), a
ELSE
    out (254), a
ENDIF

    and 0x87
    cp 0x02
    jr nz, paint_screen

    ld a, (border_sound_byte)
    ; make radar sound
    ld b, SOUND_DUR
sound_loop3:
    ld c, b
    xor 0x18
    out (254), a
    ld b, SOUND_FREQ
sound_loop1:
    nop
    djnz sound_loop1
    ld b, c
    djnz sound_loop3

; ---------------------------------
; copies the buffer into the screen
; ---------------------------------

paint_screen:
    exx
    ex af, af'
    push af
    push bc
    push de
    push hl
    push iy

    ld hl, screen_addresses ; list of destination addresses (at screen)
    ld (stack_pt), hl
    ld hl, BUFFER ; double buffer (where we painted everything)

    ; First copy the first 7 character lines, to make time until the beam arrives at the end of the screen
    ; this prepares the sub_routine for that
    ld a, 0x87
    ld (character_compare+1), a

    ; wait for the tracing to arrive to the PAPER zone
    exx
floating_bus_value:
    ld bc, 0x0FFD ; this should allow the contention to work in +2A/+3; in other machines will be changed to 0xFFFF
    ld hl, 0x5BFF ; printer buffer, which is unused
    ld a, 0xFF
    ld (hl), a    ; it is needed to read or write a value in the bus to make the floating bus work in the +2A/+3
loop_wait_paper:
    ld e, (hl) ; this synchronizes the IN with the bus access and inserts a 0xFF in the bus
    in a, (c)
    cp e
    jp z, loop_wait_paper
    exx

; copy the screen
    jp im2_loop3 ; the return address is already in the table
im2_ret1:
    ld (stack_pt), sp
    ld (current_hl), hl

    ; Now copy all the color attributes
    ld hl, 22544 ; destination for the attributes + 16
    exx
    ld hl, 38912 ; here starts the attributes in the buffer


im2_attr_loop2:
IF !DO_FAST_ATTRIBUTES
im2_attr_loop:
ENDIF
    ld sp, hl
    ld a, 16
    add a, l
    ld l, a
    pop af
    pop bc
    pop de
    pop ix
    exx
    ex af, af'
    pop af
    pop bc
    pop de
    pop iy
    ld sp, hl
    push iy
    push de
    push bc
    push af
IF DO_FAST_ATTRIBUTES
    ld a, 16
    add a, l
    ld l, a
ELSE
    ld de, 16
    add hl, de
ENDIF
    exx
    ex af, af'
    push ix
    push de
    push bc
    push af

    ld sp, hl
IF DO_FAST_ATTRIBUTES
    ld a, 16
    add a, l
    ld l, a
ELSE
    ld de, 16
    add hl, de
ENDIF
    pop af
    pop bc
    pop de
    pop ix
    exx
    ex af, af'
    pop af
    pop bc
    pop de
    pop iy
    ld sp, hl
    push iy
    push de
    push bc
    push af
    ld a, 16
    add a, l
    ld l, a
    exx
    ex af, af'
    push ix
    push de
    push bc
    push af

IF DO_FAST_ATTRIBUTES
im2_attr_loop:
include "copy_attributes.asm"
ENDIF

    ld a, h
    cp 0x9A ; end of attributes (22 lines)
    jp nz, im2_attr_loop2
    ld a, l
    cp 0xE0
    jp nz, im2_attr_loop

    ld hl, (current_hl)
    ld a, 0x80+height ; 23 lines
    ld (character_compare+1), a
    jp im2_loop3 ; the return address is already in the table
im2_ret2:
    ld hl, do_repaint_flags
    res 0, (hl) ; reset the "repaint" flag to notify that it has ended the repaint REPFLAG_0
    ld sp, im2_stack-20 ; we don't need to store the value because we know that the stack is here
    pop iy
    pop hl
    pop de
    pop bc
    pop af
    exx
    ex af, af'
    pop hl
im2_exit:
    pop ix
    pop de
    pop bc
    pop af
    ld sp, (im2_stack)
    ei
    ret

im2_loop3:
; Copy 256 bytes from HL to HL'-16 in screen format

    ld sp, hl
    ld a, 16
    add a, l
    ld l, a
    pop af
    pop bc
    pop de
    pop ix
    exx
    ex af, af'
    pop af
    pop bc
    pop de
    pop iy
    ld sp, (stack_pt)
    pop hl
    ld (stack_pt), sp
    ld sp, hl
    push iy
    push de
    push bc
    push af
    ld de, 16
    add hl, de
    exx
    ex af, af'
    push ix
    push de
    push bc
    push af
    ld sp, hl
    ld a, 16
    add a, l
    ld l, a

    include "pushpop.asm"
    include "pushpop.asm"
    include "pushpop.asm"
    include "pushpop.asm"
    include "pushpop.asm"
    include "pushpop.asm"
    include "pushpop.asm"

    inc h ; when L is zero, the source pointer has done a full 256 trip, so H must be increased
    pop af
    pop bc
    pop de
    pop ix
    exx
    ex af, af'
    pop af
    pop bc
    pop de
    pop iy
    ld sp, hl
    push iy
    push de
    push bc
    push af
    exx
    ex af, af'
    push ix
    push de
    push bc
    push af

    ld a, h
character_compare:
    cp 0x84+height
    jp nz, im2_loop3
    ld sp, (stack_pt)
    ret
