#!/usr/bin/env python3

# (C)2021 Raster Software Vigo (Sergio Costas Rodriguez)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import imageio
import sys

colorlist = [(0,0,0), (255,0,0), (0,255,0), (255,255,0), (0,0,255), (255,0,255), (0,255,255), (255,255,255)]

im = imageio.imread(sys.argv[1])
if (im.shape[0] != 192) or (im.shape[1] != 256):
    print(f"Incorrect size: {im.shape[:2]}")
    sys.exit(-1)


def get_nearest_color(color):
    global colorlist
    if len(color) == 3:
        r,g,b = color
    else:
        r,g,b,a = color

    mindist = 196608
    mincolor = None
    for idx in range(len(colorlist)):
        color = colorlist[idx]
        d = (color[0]-b)**2 + (color[1]-r)**2 + (color[2]-g)**2
        if d < mindist:
            mincolor = idx
            mindist = d
    return mincolor

def get_colors(picture, xi, yi):
    colors = {}
    for y in range(8):
        for x in range(8):
            color = picture[yi+y][xi+x]
            if color not in colors:
                colors[color] = 0
            colors[color] += 1
    return colors


picture = []
for y in range(0,192):
    line = []
    for x in range(0,256):
        idx = get_nearest_color(im[y][x])
        line.append(idx)
    picture.append(line)

colors = []
for y in range(0,192,8):
    for x in range(0,256,8):
        colors.append(get_colors(picture, x, y))

colors2 = []
for color in colors:
    max1 = (None, None)
    max2 = (None, None)
    for idx in color:
        repeats = color[idx]
        if max1[0] is None:
            max1 = (idx, repeats)
        else:
            if repeats > max1[1]:
                max2 = max1
                max1 = (idx, repeats)
            else:
                if max2[0] is None:
                    max2 = (idx, repeats)
                else:
                    if repeats > max2[1]:
                        max2 = (idx, repeats)
    if max2[0] is None:
        colors2.append([max1[0], max1[0]])
    else:
        colors2.append([max1[0], max2[0]])

line = [0]*32
picture2 = []
for a in range(192):
    picture2.append([0]*32)
for y in range(24):
    for x in range(32):
        attr = colors2[y*32+x]
        for y2 in range(8):
            counter = 1
            yf = y*8+y2
            for x2 in range(8):
                xf = x*8+7-x2
                if picture[yf][xf] == attr[0]:
                    picture2[yf][x] += counter
                counter *= 2


with open(sys.argv[2],"w") as output:
    output.write("org 16384\n")
    for y in range(192):
        scan = y % 8
        third = int(y / 64)
        character = (int(y / 8)) % 8
        yo = character + scan*8 + third * 64
        output.write("    DEFB ")
        first = True
        for n in picture2[yo]:
            if not first:
                output.write(", ")
            first = False
            output.write(f"{n}")
        output.write("\n")

    counter = 0
    for attr in colors2:
        if counter == 0:
            output.write("\n    DEFB ")
            first = True
        ink, paper = attr
        if not first:
            output.write(", ")
        first = False
        n = ink + 8*paper + 64
        output.write(f"{n}")
        counter += 1
        if counter == 32:
            counter = 0
    output.write("""

IF USE_HALT
    di
    halt
ENDIF\n""")
