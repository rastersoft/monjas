; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

end_animation:
    call do_blank
    ld hl, 0x4000
    ld de, 0x4001
    ld bc, 6143
    xor a
    ld (hl), a
    ldir
    ld a, (border_sound_byte)
    and 0x18
    or 0x07
    out (254), a
    ld (border_sound_byte), a
    ld a, 0x38
    call set_last_line_color
    ld hl, do_repaint_flags
    res 1, (hl) ; REPFLAG_1
    res 0, (hl) ; REPFLAG_0
    scf
    call set_character_direction_and_position ; look to the left
    DEFW main_character
    DEFW 0x601A
    and a
    call set_character_direction_and_position ; look to the right
    DEFW white_character
    DEFW 0x541A
    ld a, 255
    ld (main_character_sprites), a
    ld (main_character_sprites+6),a
    ld (captain_sprites), a
    ld (captain_sprites+6), a
    call do_end1_clear
    ld b, 100
    call do_end1_wait
    call make_talk_and_wait
    DEFW main_character
    DEFW sentence_end1
    call do_end1_clear
    call make_talk_and_wait
    DEFW white_character
    DEFW sentence_end2
    call do_end2_clear
    ld b, 1
    call do_end1_wait ; repaint the screen
    ld a, 0x82
    ld (border_sound_byte), a
    out (254), a ; put the border in red color, but without sound
    ld b, 99     ; and wait
    call do_end1_wait
    call make_talk_and_wait
    DEFW main_character
    DEFW sentence_end3
    call do_end2_clear
end_animation_loop:
    ld b, 100
    call do_end1_wait
    ld a, 0x07
    ld (border_sound_byte), a
    out (254), a
    call do_blank
    ld a, 0x38
    call do_end_clear
    call make_talk_and_wait
    DEFW main_character
    DEFW sentence_end4
    jp end_animation_loop

do_end1_wait:
    ld hl, do_repaint_flags
    set 0, (hl) ; REPFLAG_0
    ei
do_end2_wait:
    halt
    djnz do_end2_wait
    ret


do_end1_clear:
    ld a, 0x38
    call do_end1_base
    ld hl, sprite_base_right
    ld d, MAP_OFFSET_Y - 5
    ld e, MAP_OFFSET_X - 7
    ld iyl, 0x78
    call paint_sprite
    ret

do_end2_clear:
    ld a, 0x10
    call do_end1_base
    ld hl, sprite_base_right_impostor
    ld d, MAP_OFFSET_Y - 6
    ld e, MAP_OFFSET_X - 8
    call paint_sprite
    ret

do_end_clear:
    ld hl, 0x8000
    ld de, 0x8001
    ld bc, 6144
    ld (hl), 0
    ldir
    ld hl, 0x9800
    ld de, 0x9801
    ld bc, 767
    ld (hl), a
    ldir ; clear the screen
    call dump_last_line
    ret

do_end1_base:
    call do_end_clear
    ld hl, sprite_base_left
    ld d, MAP_OFFSET_Y - 5
    ld e, MAP_OFFSET_X + 1
    ld iyl, 0x58
    call paint_sprite
    ld hl, sprite_feet_left1
    ld d, MAP_OFFSET_Y
    ld e, MAP_OFFSET_X + 1
    ld iyl, 0x58
    call paint_sprite
    ld hl, sprite_feet_right1
    ld d, MAP_OFFSET_Y
    ld e, MAP_OFFSET_X - 7
    ld iyl, 0x48
    call paint_sprite
    ret
