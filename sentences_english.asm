sentence_intro1:
    DEFB "Personal log: a new promising\n"
    DEFB "day of mission in space begins.",0
sentence_intro2:
    DEFB "What new adventures will I\n"
    DEFB "encounter today? A battle\n"
    DEFB "against some ferocious\n"
    DEFB "life form?",0
sentence_intro3:
    DEFB "Rescuing a spaceship?\n"
    DEFB "Or maybe...?",0
sentence_intro4:
    DEFB "You, dork! The toilet won't\n"
    DEFB "clean itself!",0
sentence_intro5:
    DEFB "Leave me alone!\nI'm busy!",0
sentence_intro6:
    DEFB "Haha! Still with your\n"
    DEFB "stupid diary? Captain Pink!",0
sentence_intro7:
    DEFB "Hey! Give it back!",0
sentence_intro8:
    DEFB "Oops! I pressed\n"
    DEFB "the reset button. Haha!",0
sentence_intro9:
    DEFB "Get lost, Yellow.",0
sentence_intro10:
    DEFB "Really? And what are you going\n"
    DEFB "to do, report me? Haha!",0
sentence_intro11:
    DEFB "Forget him, he's a psycho.\n"
    DEFB "Sooner or later karma will\n"
    DEFB "do its job.",0
sentence_intro12:
    DEFB "He erased my diary...",0
sentence_intro13:
    DEFB "Pink... Seriously? Are you\n"
    DEFB "still with those fantasies?",0
sentence_intro14:
    DEFB "Those aren't fantasies! There\n"
    DEFB "are thousands of real\n"
    DEFB "adventures out there!",0
sentence_intro15:
    DEFB "This is not a movie, Pink, it's\n"
    DEFB "real life. There's only work,\n"
    DEFB "and some fun now and then.",0
sentence_intro16:
    DEFB "I refuse to accept it. Life\n"
    DEFB "must be more than wasting\n"
    DEFB "away on a...",0
sentence_intro17:
    DEFB "a damn lost space station\n"
    DEFB "falling to pieces.",0
sentence_intro18:
    DEFB "Come back to earth,\n"
    DEFB "Pink... for your own good.",0
sentence_intro19:
    DEFB "I'll get out of this,\n"
    DEFB "station, no matter how.",0



sentence_poor_chuck:
    DEFB "No! Poor Chuck!",0



sentence_not_now:
    DEFB "I don't want to.\n"
    DEFB "In every sense.",0

sentence_too_far:
    DEFB "Better not go further away.",0

sentence_nothing_to_take:
    DEFB "There's nothing to take.",0
sentence_cant_take_that:
    DEFB "I can't take that.",0
sentence_cant_do_that:
    DEFB "I can't do that.",0
sentence_cant_take_people:
    DEFB "I can't take people.",0
sentence_nothing_to_see:
    DEFB "I see nothing.",0
sentence_nothing_to_use:
    DEFB "There's nothing I can use.",0
sentence_nothing_to_talk_to:
    DEFB "My psychologist told me not\n"
    DEFB "to talk to the void.",0
sentence_rubber_chicken_alone:
    DEFB "I would need a cable\n"
    DEFB "between two poles.",0
sentence_rubber_chicken_pot:
    DEFB "Keep that away from the\n"
    DEFB "kitchen! It might blow up!",0
sentence_rubber_chicken_pot2:
    DEFB "(He is a superb cook, but\n"
    DEFB "also a drama queen).",0
sentence_rubber_chicken_pot3:
    DEFB "I heard that!",0


sentence_no_one_near:
    DEFB "Give to whom?\nThere's no one.",0


sentence_dont_give:
    DEFB "Better not.",0


sentence_take_box:
    DEFB "It's too heavy.",0
sentence_take_pot:
    DEFB "I don't want get burnt.",0

sentence_already_found_galena:
    DEFB "Why? I already\n"
    DEFB "have the galena.",0

sentence_look_grid:
    DEFB "I think I saw\n"
    DEFB "something moving.",0
sentence_look_box:
    DEFB "It's a container built with\n"
    DEFB "cellulose fibers and sealed\n"
    DEFB "with sharp stainless steel\n"
    DEFB "cylinders inserted by\n"
    DEFB "percussion.",0
sentence_look_pot:
    DEFB "How come water isn't\n"
    DEFB "evaporating in this vacuum?",0
sentence_look_plant:
    DEFB "It's Chuck. Looks a bit\n"
    DEFB "withered from exposure to\n"
    DEFB "vacuum.",0
sentence_talk_plant:
    DEFB "Hi Chuck!",0
sentence_look_rubber_chicken:
    DEFB "How odd... it has\n"
    DEFB "a puller in the middle.",0
sentence_look_diary:
    DEFB "It's my electronic diary.\n"
    DEFB "It's empty. It has an USB\n"
    DEFB "connector on the side.",0
sentence_look_diary2:
    DEFB "It's my electronic diary\n"
    DEFB "with the stellar maps.",0
sentence_look_usb_cable:
    DEFB "A normal USB cable.",0
sentence_look_galena_diode:
    DEFB "It is pure\n"
    DEFB "crystalline galena.",0
sentence_look_radar_empty:
    DEFB "The radar covers one sector,\n"
    DEFB "but the maps cover nine, and\n"
    DEFB "they are upgradable via USB.",0
sentence_look_radar_empty2:
    DEFB "There's nothing on the radar.",0
sentence_look_radar_active:
    DEFB "A ship wants to land!\n"
    DEFB "I'll warn Green.",0
sentence_look_reactor:
    DEFB "It's the station's nuclear\n"
    DEFB "reactor. It never really\n"
    DEFB "worked properly.",0
look_detector_1:
    DEFB "ACME galena detector!",0
look_detector_2:
    DEFB "Thank you for buying the\n"
    DEFB "fantastic ACME galena\n"
    DEFB "detector.",0
look_detector_3:
    DEFB "With it, your work in the mine\n"
    DEFB "will be as fun and easy as\n"
    DEFB "a stroll in the countryside.",0
look_detector_4:
    DEFB "Now, compatible with\n"
    DEFB "deaf people.",0
sentence_look_galena_cristal:
    DEFB "It's a galena crystal\n"
    DEFB "the size of a fist.",0
sentence_look_iodine:
    DEFB "It's a bottle of idodine\n"
    DEFB "for disinfection.\n"
    DEFB "Agh, it's gooey!",0
sentence_look_chicken:
    DEFB "Mmmm... it looks yummy.",0


sentence_a_ship:
    DEFB "It's the radar alert!\n"
    DEFB "And it's Green's shift!",0
sentence_radar_task_for_green:
    DEFB "I'll turn off the alarm, but\n"
    DEFB "this is a job for Green.",0
sentence_take_radar:
    DEFB "I'm not that kleptomaniac.",0
sentence_take_radar2:
    DEFB "(and it's screwed to the wall)",0

sentence_take_reactor:
    DEFB "I REFUSE to put a nuclear\n"
    DEFB "reactor in my pocket!",0
sentence_use_reactor:
    DEFB "I'd better not touch it.\n"
    DEFB "It's very hot.",0

sentence_look_locker_yellow:
    DEFB "It's Yellow's locker. Here he\n"
    DEFB "stores all his old junk.\n"
    DEFB "Hmm... is that a galena radio?",0
sentence_take_locker_yellow1:
    DEFB "I don't want anything\n"
    DEFB "from Yellow.",0
sentence_take_locker_yellow2:
    DEFB "I'll take only the\n"
    DEFB "galena diode.",0

sentence_look_locker_red_with_cable:
    DEFB "This is Red's locker. All\n"
    DEFB "his things are tidy. There\n"
    DEFB "are some cookery books, a\n"
    DEFB "pendrive and an USB cable.",0
sentence_look_locker_red_no_cable:
    DEFB "This is Red's locker. All\n"
    DEFB "his stuff is tidy. There\n"
    DEFB "are some cookery books and\n"
    DEFB "a pendrive.",0
sentence_take_locker_red1:
    DEFB "I should ask Red\n"
    DEFB "before taking it.",0
sentence_take_locker_red2:
    DEFB "I'll take the cable.",0
sentence_take_locker_red3:
    DEFB "I can't take anything else.",0



sentence_look_locker_green_with_chicken:
    DEFB "It's Green's locker. It's\n"
    DEFB "full of empty botles, and...\n"
    DEFB "a rubber chicken!",0
sentence_look_locker_green_no_chicken:
    DEFB "It's Green's locker. It's\n"
    DEFB "full of empty botles.",0
sentence_take_locker_green:
    DEFB "I'll take only the chicken.\n"
    DEFB "I don't think he will mind.",0
sentence_take_locker_green1:
    DEFB "I don't need\n"
    DEFB "any of that.",0
sentence_take_locker_green2:
    DEFB "I don't need\n"
    DEFB "an empty botle.",0

sentence_look_locker_blue:
    DEFB "It's Blue's locker. It's full\n"
    DEFB "of medical books and...\n"
    DEFB "erotic novels?",0
sentence_take_locker_blue:
    DEFB "I'm not interested.",0

sentence_look_yellowchicken:
    DEFB "It looks yummy...",0
sentence_take_yellowchicken:
    DEFB "I don't think\n"
    DEFB "he would mind...",0

sentence_look_locker_main_with_diary:
    DEFB "It's my locker. There's\n"
    DEFB "only my diary inside.",0
sentence_look_locker_main_no_diary:
    DEFB "It's my locker. It's empty.",0
sentence_take_locker_main1:
    DEFB "I'll take the diary.",0

sentence_land_ship1:
    DEFB "This is the Monjas-type Epsilon\n"
    DEFB "9 station. Please, respond.",0
sentence_land_ship2:
    DEFB "This is cargo ship X-Rays\n"
    DEFB "Delta 1 asking permission for\n"
    DEFB "emergency landing.",0
sentence_land_ship3:
    DEFB "This is the Monjas-type Epsilon\n"
    DEFB "9 station. Acknowledged.\n"
    DEFB "You have permission to land\n"
    DEFB "in the hangar.",0
sentence_land_ship4:
    DEFB "Thanks! I don't know how much\n"
    DEFB "longer I could hold on.",0
sentence_land_ship5:
    DEFB "Welcome to th...",0
sentence_land_ship6:
suspensive_dots:
    DEFB "...",0
sentence_land_ship7:
    DEFB "FLUSHHHHhhhhh...",0
sentence_land_ship8:
    DEFB "Thanks! That was\n"
    DEFB "a close call.",0


sentence_look_green:
    DEFB "Drunk, as usual.\n"
    DEFB "Such a pity...",0
talk_green_1:
    DEFB "Hi Green.",0
talk_green_2:
    DEFB "What a shurprishe\n"
    DEFB "-hic- Pfffink!!!!",0
talk_green_3:
    DEFB "Drinking again?",0
talk_green_4:
    DEFB "I am shober.\n"
    DEFB "You're the one who'sh\n"
    DEFB "drunk... -hic-\n"
    DEFB "yo are pished.",0
talk_green_5:
    DEFB "I wonder where you hide\n"
    DEFB "your booze...",0
talk_green_6:
    DEFB "You will never ffffind out\n"
    DEFB "I shwear.",0
talk_green_7:
    DEFB "There's a ship on the radar.",0
talk_green_8:
    DEFB "Well I am sheeing... -hic-\n"
    DEFB "two. Tee hee hee.",0
talk_green_9:
	DEFB "I'd better talk with Blue.",0
talk_green_10:
    DEFB "Can I take a shot?",0
talk_green_11:
    DEFB "Thish ish -hic- only\n"
    DEFB "for -hic- grown\n"
    DEFB "upsh... -hic-.",0
talk_green_12:
    DEFB "Blue found a stash of\n"
    DEFB "bottles of booze...",0
talk_green_15:
    DEFB "¡Nooo... my precioushh...!\n"
    DEFB "Let me go look... But don't\n"
    DEFB "you... dare -hic-\n"
    DEFB "ffffollow me...",0
talk_green_16:
	DEFB "I shaid don't -hic- you dare\n"
    DEFB "-hic- ffffollow me!!!",0
talk_green_17:
    DEFB "Naaahhhh... everyfing wash\n"
    DEFB "in order.",0
talk_green_18:
    DEFB "Goodbye.",0
talk_green_19:
    DEFB "-hic-",0

; Here, PINK used iodine on GREEN's spacesuit, staining it
sentence_green_iodine1:
    DEFB "Oops... Sorry.",0
sentence_green_iodine2:
    DEFB "Never mind... it won't\n"
    DEFB "show becaushe there'sh\n"
    DEFB "all thish puke.",0
sentence_green_iodine3:
    DEFB "But the puke is\n"
    DEFB "inside your suit...",0

sentence_use_iodine_in_object:
    DEFB "I don't want to soil it.",0
sentence_use_iodine_in_person:
    DEFB "I can't see any injury.",0
sentence_use_iodine_in_white:
    DEFB "I don't want to spoil the\n"
    DEFB "pure white of his suit.",0
sentence_give_iodine_to_white:
    DEFB "Can we use this to\n"
    DEFB "clean the galena crystal?",0
sentence_give_iodine_to_white2:
    DEFB "Do you intend to clean\n"
    DEFB "something with IODINE?",0

sentence_eat_diary:
    DEFB "I wonder if the diary would\n"
    DEFB "pass through his throat...",0
sentence_eat_diary2:
    DEFB "Better not... It would\n"
    DEFB "probably end in my stomach...\n"
    DEFB "or lower down.",0

sentence_transferred_data_to_diary:
    DEFB "I copied the maps in my diary.\n"
    DEFB "I hope the antipiracy league\n"
    DEFB "doesn't find out.",0

sentence_need_usb:
    DEFB "I can't connect it without\n"
    DEFB "the right cable.",0

sentence_look_blue:
    DEFB "It's Blue, the physisican\n"
    DEFB "and station commander.",0
talk_blu_1:
    DEFB "Hello, commander.",0
talk_blu_2:
    DEFB "Only urgent stuff, Pink.\n"
    DEFB "I'm very busy.",0
talk_blu_3:
    DEFB "Yellow erased my personal log.",0

talk_blu_4:
    DEFB "I will file a report later,\n"
    DEFB "though you know I can't do much,\n"
    DEFB "he has friends in high places.",0




talk_blu_5:
    DEFB "A ship is coming.",0
talk_blu_6:
    DEFB "Tell Green,\n"
    DEFB "it's his shift.",0
talk_blu_6b:
    DEFB "Green is drunk again...",0
talk_blu_6c:
    DEFB "Then do it yourself.\n"
    DEFB "My hands are full.",0
talk_blu_7:
    DEFB "What are you up to?",0
talk_blu_8:
    DEFB "Urgent business,\n"
    DEFB "Pink. I'm really busy.",0
talk_blu_9:
    DEFB "Do we have galena?",0
talk_blu_10:
    DEFB "This is the infirmary, Pink.\n"
    DEFB "Besides, we're not\n"
    DEFB "running a mining station.",0
talk_blu_11:
    DEFB "Do we have any alcohol?",0
talk_blu_12:
    DEFB "We only have iodine.\n"
    DEFB "Here, take this bottle and\n"
    DEFB "do it yourself. I'm very busy.",0
talk_blu_17:
    DEFB "Never mind, it's not\n"
    DEFB "important.",0
talk_blu_18:
    DEFB "Then let me work, please.\n"
    DEFB "I'm very busy.",0


sentence_look_red:
    DEFB "Red cooks like nobody else.",0
talk_red_1:
    DEFB "Hi Red.",0
talk_red_2:
    DEFB "Hi Pink. You mustn't nibble.\n"
    DEFB "We are short of food.",0
talk_red_2b:
    DEFB "But I didn't even ask...",0
talk_red_2c:
    DEFB "Yeah, but I know your\n"
    DEFB "stomach very well.",0
talk_red_3:
    DEFB "How much longer for lunch?",0
talk_red_4:
    DEFB "Already hungry? We\n"
    DEFB "just had breakfast...",0
talk_red_5:
    DEFB "Can I use the oven?",0
talk_red_6:
    DEFB "Remember that you damaged it\n"
    DEFB "when you tried to charge your\n"
    DEFB "phone. Yellow still has to\n"
    DEFB "fix it.",0
talk_red_7:
	DEFB "I read on a web site...",0
talk_red_8:
    DEFB "I need an USB pendrive.\n"
    DEFB "Can you lend me one?",0
talk_red_9:
    DEFB "I have one, but it\n"
    DEFB "contains data. Sorry.",0
talk_red_10:
    DEFB "Do you have an USB cable?",0
talk_red_11:
    DEFB "There's one in my locker,\n"
    DEFB "you can take it.",0
talk_red_12:
    DEFB "Can I take that chicken?",0
talk_red_13:
    DEFB "No, you would eat it.",0
talk_red_14:
    DEFB "oh, c'mon...",0
talk_red_15:
    DEFB "Don't even think about it.\n"
    DEFB "It's today's lunch.",0
talk_red_16:
    DEFB "I want nothing.",0
talk_red_17:
    DEFB "That I may offer you.\n"
    DEFB "Help yourself.",0
talk_red_18:
    DEFB "Could we invite Captain\n"
    DEFB "White to lunch?",0
talk_red_19:
    DEFB "Blue has forbidden guests.\n"
    DEFB "We are short of food.",0


sentence_look_yellow:
    DEFB "He always hated me,\n"
    DEFB "I don't know why.",0
talk_yellow_1:
    DEFB "Hello.",0
talk_yellow_2:
    DEFB "Get lost, dork. Or I'll\n"
    DEFB "roast you in the reactor.",0
talk_yellow_3:
    DEFB "The oven in the kitchen\n"
    DEFB "doesn't work.",0
talk_yellow_4:
    DEFB "That damn oven again! I'm\n"
    DEFB "fed up. There's no fixing it!",0
talk_yellow_5:
    DEFB "Bye.",0
talk_yellow_6:
    DEFB "At last you say\n"
    DEFB "something interesting.",0
talk_yellow_7:
    DEFB "Don't touch the reactor!",0
talk_yellow_8:
    DEFB "Oh, oh...",0
talk_yellow_9:
    DEFB "What the ...?!",0
talk_yellow_10:
    DEFB "That was the reactor!!!!",0
talk_yellow_11:
    DEFB "Oops...",0

sentence_look_white:
    DEFB "What does he use to\n"
    DEFB "launder his spacesuit?",0
talk_white_1:
    DEFB "Welcome.",0
talk_white_2:
    DEFB "Thank you. It was\n"
    DEFB "a close call.",0
talk_white_3:
    DEFB "By the way, I didn't hear the\n"
    DEFB "sink, only the toilet...",0
talk_white_4:
    DEFB "Do you realize\n"
    DEFB "we don't have hands?",0
talk_white_5:
    DEFB "Do you need anything?",0
talk_white_6:
    DEFB "I have to do some repairs.\n"
    DEFB "And something to eat would\n"
    DEFB "be great, too.",0
talk_white_6b:
    DEFB "I'm afraid we\n"
    DEFB "are short of food.",0
talk_white_7:
    DEFB "Nice ship. A cargo ship, you\n"
    DEFB "said? Which kind of cargo?",0
talk_white_8:
    DEFB "You know. A little bit of\n"
    DEFB "this, a little bit of that...\n"
    DEFB "A little bit of everything.",0
talk_white_9:
    DEFB "It looks quite small...",0
talk_white_10:
    DEFB "Because I only transport\n"
    DEFB "little this and that.",0
talk_white_11:
    DEFB "Hmmm... Listen, do you have\n"
    DEFB "room for company?",0
talk_white_12:
    DEFB "Company...? Hmmm...\n"
    DEFB "Maybe, but I'm so hungry\n"
    DEFB "I can't make a decision.",0
talk_white_13:
    DEFB "What do you need to\n"
    DEFB "take me on board?",0
talk_white_14:
    DEFB "I still need:",0
talk_white_15:
    DEFB "A galena crystal.",0
talk_white_16:
    DEFB "Alcohol.",0
talk_white_17:
    DEFB "A stellar map.",0
talk_white_18:
    DEFB "An oxygen generator.",0

talk_white_19:
	DEFB "We are ready to go.\n"
    DEFB "Shall we?",0
talk_white_20:
	DEFB "Of course!!!",0
talk_white_21:
    DEFB "I have things to do.",0
talk_white_22:
    DEFB "See you later.",0



talk_white_generator1:
    DEFB "Will it be able to\n"
    DEFB "generate oxygen?",0
talk_white_generator2:
    DEFB "Good idea.",0
talk_white_generator3:
    DEFB "I think we'd better\n"
    DEFB "just be friends.",0

talk_white_cristal1:
    DEFB "I have the galena crystal.",0
talk_white_cristal2:
    DEFB "That crystal is useless,\n"
    DEFB "it's too small. We need\n"
    DEFB "a bigger one.",0
talk_white_cristal3:
    DEFB "It's perfect, but it's dirty.\n"
    DEFB "We need to clean it with a\n"
    DEFB "solvent. Do you have any\n"
    DEFB "alcohol?",0
talk_white_cristal4:
	DEFB "I don't know... I'll check.",0

talk_white_food1:
    DEFB "Ehm... I found\nthis food.",0
talk_white_food2:
    DEFB "Oh! What is it?",0
talk_white_food3:
    DEFB "It's... cow trotters.",0
talk_white_food4:
    DEFB "Cow trotters are the best\n"
    DEFB "delicacy, not even pheasant\n"
    DEFB "tastes the same to me.",0
talk_white_food5:
    DEFB "Do we have a deal, then?",0
talk_white_food6:
    DEFB "Yes, we have a deal.\n"
    DEFB "Unfortunately, the engine's\n"
    DEFB "galena crystal is damaged.\n"
    DEFB "I'll need a new one.",0
talk_white_food7:
    DEFB "Also the life support system\n"
    DEFB "is barely enough for one. We'll\n"
    DEFB "need an extra oxygen generator.",0
talk_white_food8:
    DEFB "And my stellar map is quite\n"
    DEFB "limited, we wouldn't go\n"
    DEFB "very far.",0
talk_ill_see_what_i_can_do:
    DEFB "Hmmm... I'll see\n"
    DEFB "what I can do.",0
talk_white_food10:
    DEFB "This rubber is too\n"
    DEFB "raw for my taste.",0

talk_white_maps1:
    DEFB "I have the maps.",0
talk_white_maps2:
    DEFB "Wow, an edition from\n"
    DEFB "twenty years ago. You're\n"
    DEFB "into vintage...",0

sentence_give_no_maps:
    DEFB "An empty diary?\n"
    DEFB "What am I to do with it?",0


sentence_look_marvin:
    DEFB "It's Marvin, our\n"
    DEFB "Martian neighbour.",0
talk_marvin_1:
    DEFB "Hi Marvin.",0
talk_marvin_2:
    DEFB "Hi Pink.",0
talk_marvin_3:
	DEFB "What are you up to?",0
talk_marvin_4:
    DEFB "I'm resting.\n"
    DEFB "I retired yesterday.",0
talk_marvin_5:
	DEFB "You retired from the mine?",0
talk_marvin_6:
    DEFB "Yep. Two hundred years with a\n"
    DEFB "pick and a shovel is my limit.",0
talk_marvin_7:
	DEFB "What are you going\n"
    DEFB "to do now?",0
talk_marvin_8:
    DEFB "I'm going to rest. And I'll\n"
    DEFB "rest. And maybe I'll rest too.\n"
    DEFB "Did I already say 'rest'?",0
talk_marvin_9:
	DEFB "Can you get me\n"
    DEFB "a galena crystal?",0
talk_marvin_10:
	DEFB "I'm sorry, I'm retired.\n"
    DEFB "You will have to find\n"
    DEFB "it by yourself.",0
talk_marvin_11:
    DEFB "I don't know how to\n"
    DEFB "find a crystal.",0
talk_marvin_12:
	DEFB "Here, use my galena\n"
    DEFB "detector. I won't need\n"
    DEFB "it anymore.",0
talk_marvin_13:
    DEFB "Thanks... I think.",0
talk_marvin_14:
	DEFB "See you later, Marvin.",0
talk_marvin_15:
	DEFB "Enjoy.",0


sentence_take_galena_cristal:
    DEFB "Agh, I broke a nail!\n"
    DEFB "But I got it.",0

sentence_already_have_galena_cristal:
    DEFB "I don't need more galena.",0

sentence_gave_alcohol_1:
    DEFB "Here, alcohol.",0
sentence_gave_alcohol_2:
    DEFB "Mmm... Yes, I\n"
    DEFB "think it will do.",0


sentence_end1:
    DEFB "Ok, where do\n"
    DEFB "we go now?",0
sentence_end2:
    DEFB "As for me, I'm still\n"
    DEFB "undecided, but as for you...",0
sentence_end3:
    DEFB "This doesn't look\n"
    DEFB "good at all...",0
sentence_end4:
    DEFB "Turn off the computer\n"
    DEFB "and go to sleep.",0

; object and character names
name_yellow:
    DEFB "Yellow",0
name_red:
    DEFB "Red",0
name_green:
    DEFB "Green",0
name_blue:
    DEFB "Blue",0
name_white:
    DEFB "White",0
name_marvin:
    DEFB "Marvin",0
name_reactor:
    DEFB "Reactor",0
name_radar:
    DEFB "Radar",0
name_grid:
    DEFB "Grid",0
name_box:
    DEFB "Box",0
name_pot:
    DEFB "Pot",0
name_plant:
    DEFB "Chuck the plant",0
name_locker:
    DEFB "Locker",0
name_diary:
    DEFB "Electronic diary",0
name_usb_cable:
    DEFB "USB cable",0
name_rubber_chicken:
    DEFB "Rubber chicken",0
name_galena_diode:
    DEFB "Galena diode",0
name_chicken:
    DEFB "Chicken",0
name_yellowchicken:
    DEFB "Yellow spit-roast",0
name_galena_detector:
    DEFB "Galena detector",0
name_galena_cristal:
    DEFB "Galena crystal",0
name_iodine:
    DEFB "Iodine bottle",0
name_booze:
    DEFB "Booze",0

sentence_look_booze:
    DEFB "I can't believe Green can\n"
    DEFB "drink this pipe cleaner.",0




sentence_look_tinted_grid:
    DEFB "It's Green's stash!",0




sentence_take_alcohol:
    DEFB "I'll take a botle.",0
sentence_already_took_alcohol:
    DEFB "I don't need more.",0

sentence_swap_chicken1:
    DEFB "(Nothing up my\n"
    DEFB "sleeve... Presto!)",0
sentence_swap_chicken2:
    DEFB "Don't you dare.\n"
    DEFB "I'm watching you.",0

sentence_cover_1:
    DEFB "Escape from M.O.N.J.A.S.",0
sentence_cover_2:
    DEFB "1. Play",0
sentence_cover_3:
    DEFB "2. Skip the intro",0
sentence_cover_5:
    DEFB "\c2021 Raster Software Vigo",0
sentence_cover_6:
    DEFB "http://www.rastersoft.com",0
sentence_cover_7:
    DEFB "Sinclair1/OPQA/Kempston: move",0
sentence_cover_8:
    DEFB "Space/Enter/0/M/Z: menu",0

; verbs/commands
command_take:
    DEFB "Take ",0
command_look:
    DEFB "Look ",0
command_talk:
    DEFB "Talk ",0
command_use:
    DEFB "Use ",0
command_give:
    DEFB "Give ",0
command_return:
    DEFB "Return",0
