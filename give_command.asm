; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

; Manages all the GIVE actions

do_give:
    ld a, (character_detected)
    and a
    jp nz, do_give2
    call make_talk_main
    DEFW sentence_no_one_near
    ret

do_give2:
    ld a, (character_detected)
    push af
    xor a
    ld (character_detected), a
    call ask_inventory
    cp VERB_RETURN
    jr nz, do_give3
    pop af
    ret
do_give3:
    ld b, a
    pop af
    ld (character_detected), a
    cp CHARACTER_WHITE
    jr z, do_give_white
    cp CHARACTER_RED
    jr z, do_give_red
dont_give:
    call make_talk_main
    DEFW sentence_dont_give
    ret

do_give_red:
    ld a, b
    cp OBJECT_PLANT
    jr nz, dont_give
    call make_talk_main
    DEFW sentence_poor_chuck
    ret

do_give_white:
    ld a, b
    cp OBJECT_YELLOWCHICKEN
    jr z, do_give_white_yellowchicken
    cp OBJECT_GALENA_DIODE
    jr z, do_give_white_diode
    cp OBJECT_RUBBER_CHICKEN
    jp z, do_give_white_rubber
    cp OBJECT_PLANT
    jp z, do_give_white_plant
    cp OBJECT_GALENA_CRISTAL
    jp z, do_give_white_cristal
    cp OBJECT_BOOZE
    jp z, do_give_white_booze
    cp OBJECT_IODINE
    jp z, do_give_white_iodine
    cp OBJECT_DIARY
    jp z, do_give_white_diary
    jr dont_give


do_give_white_yellowchicken:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_food1
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_food2
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_food3
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_food4
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_food5
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_food6
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_food7
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_food8
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_ill_see_what_i_can_do
    ld hl, flags5
    res 6, (hl) ; FLAG5_6
    ld hl, flags2
    set 7, (hl) ; FLAG2_7
    set 1, (hl) ; FLAG2_1
    ld hl, flags1
    set 4, (hl) ; FLAG1_4
    ret

do_give_white_diode:
    ld a, (flags2)
    bit 7, a ; FLAG2_7
    jp z, dont_give
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_cristal1
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_cristal2
    ret

do_give_white_rubber:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_food1
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_food10
    ret

do_give_white_plant:
    ld a, (flags1)
    bit 4, a ; FLAG1_4 Check if we know that white needs an oxygen generator
    jr nz, do_give_white_plant2
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_generator3
    ret
do_give_white_plant2:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_generator1
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_generator2
    ld hl, flags4
    res 5, (hl) ; FLAG4_5
    set 6, (hl) ; FLAG4_6
    call check_have_all
    ret


do_give_white_cristal:
    ld hl, flags6
    res 2, (hl) ; FLAG6_2
    ld hl, flags3
    set 5, (hl) ; FLAG3_5
    ld hl, flags1
    set 5, (hl) ; FLAG1_5
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_cristal3
    call make_talk_main
    DEFW talk_white_cristal4
    ret

do_give_white_booze:
    ld hl, flags3
    res 3, (hl) ; FLAG3_3
    set 4, (hl) ; FLAG3_4
    call task_make_talk_and_wait
    DEFW main_character
    DEFW sentence_gave_alcohol_1
    call task_make_talk_and_wait
    DEFW white_character
    DEFW sentence_gave_alcohol_2
    call check_have_all
    ret

do_give_white_iodine:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW sentence_give_iodine_to_white
    call task_make_talk_and_wait
    DEFW white_character
    DEFW sentence_give_iodine_to_white2
    call task_make_talk_and_wait
    DEFW main_character
    DEFW suspensive_dots
    ret

do_give_white_diary:
    ld hl, flags4
    bit 2, (hl) ; FLAG4_2
    jr z, do_give_white_diary_no_maps
    set 3, (hl) ; FLAG4_3
    ld hl, flags5
    res 1, (hl) ; FLAG5_1
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_maps1
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_maps2
    call check_have_all
    ret

do_give_white_diary_no_maps:
    call make_talk
    DEFW white_character
    DEFW sentence_give_no_maps
    ret

; returns flag Z set if we miss any of the four main elements
; or NZ if we have the four ones.

check_have_all:
    ld a, (flags4)
    bit 6, a ; FLAG4_6
    ret z
    bit 3, a ; FLAG4_3
    ret z
    ld a, (flags3)
    bit 5, a ; FLAG3_5
    ret z
    bit 4, a ; FLAG3_4
    ret z
    ; code animation for the end
    call task_make_talk_and_wait
    DEFW white_character
    DEFW talk_white_19
    call task_make_talk_and_wait
    DEFW main_character
    DEFW talk_white_20
    ld b, 8
    call task_wait_B_frames
    ld a, 255
    ld (captain_sprites), a
    ld (captain_sprites + 6), a ; hide WHITE character
    call task_move_to_and_wait_for_walk
    DEFW main_character
    DEFW 0x1C4A
    ld a, 255
    ld (main_character_sprites), a
    ld (main_character_sprites + 6), a ; hide MAIN character
    ld b, 16
    call task_wait_B_frames
    ld a, TILE_INDEX_LANDING
    ld (map_array + 549), a
    ld (map_array + 550), a
    ld (map_array + 581), a
    ld (map_array + 582), a ; restore the landing tiles
    ld b, 19
    ld c, 74 ; the spaceship exits
check_have_all2:
    ld a, c
    ld (entry_spaceship+2), a
    ld (entry_spaceship2+2), a
    push bc
    call task_yield ; move the spaceshipt and wait
    pop bc
    inc c
    djnz check_have_all2
    ; the spaceship has parted
    jp end_animation

