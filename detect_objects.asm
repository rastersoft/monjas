; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

; fully refresh the objects and characters nearby
refresh_objects_characters:
    call detect_characters
    call detect_objects
    jp check_objects ; uses the RET at the end


; check which objects and characters are near the main character

detect_objects_characters:

    ld a, (main_character)
    and 0xC0 ; get the animation stage
    ret z ; in animation stage 0 do nothing
    cp 0xC0
    jp z, check_objects ; print the text (if needed) in animation stage 3
    cp 0x80
    jp z, detect_objects ; detect isolated objects in animation stage 2
    ; in animation stage 1 detect other characters

detect_characters:
    xor a ; by default, no object detected
    ld (character_detected), a
    ld (object_detected), a
    call prepare_detect
    ; Yellow
    ld a, (flags4)
    bit 7, a ; FLAG4_7
    jr nz, detect_characters2
    ld bc, 0x1824
    ld de, 0x0C10
    call detect_is_in_box
    jr nc, detect_yellow
detect_characters2:
    ; Red
    ld bc, 0x4878
    ld de, 0x4068
    call detect_is_in_box
    jr nc, detect_red
    ; Blue
    ld bc, 0x184C
    ld de, 0x0C40
    call detect_is_in_box
    jr nc, detect_blue
    ; Green
    ld bc, 0x2820
    ld de, 0x2418
    call detect_is_in_box
    jr nc, detect_green
    ; Marvin
    ld bc, 0xBC11
    ld de, 0xB80C
    call detect_is_in_box
    jr nc, detect_marvin
    ; White
    ld a, (flags1)
    and 0x03
    cp 0x03 ; FLAG1_10
    ret nz
    ld bc, 0x5024
    ld de, 0x4014
    call detect_is_in_box
    ret c ; no more characters
detect_white:
    ld a, CHARACTER_WHITE
    ld hl, name_white
    jp store_character
detect_marvin:
    ld a, CHARACTER_MARVIN
    ld hl, name_marvin
    jp store_character
detect_yellow:
    ld a, CHARACTER_YELLOW
    ld hl, name_yellow
    jp store_character
detect_red:
    ld a, CHARACTER_RED
    ld hl, name_red
    jp store_character
detect_blue:
    ld a, CHARACTER_BLUE
    ld hl, name_blue
    jp store_character
detect_green:
    ld a, CHARACTER_GREEN
    ld hl, name_green
    jp store_character


detect_objects:
    call prepare_detect
    ld a, (flags6)
    bit 1, a ; FLAG6_1
    jp nz, detect_galena_cristal
    ; Reactor
    ld bc, 0x1018
    ld de, 0x1010
    call detect_is_in_box
    jp nc, detect_reactor
    ; Chicken
    ld de, 0x4C6C
    call check_coords_hl
    jp z, detect_chicken
    ld de, 0x4C74
    call check_coords_hl
    jp z, detect_chicken
    ; Radar
    ld de, 0x4044
    call check_coords_hl
    jp z, detect_radar
    ; locker BLUE
    ld de, 0x1060
    call check_coords_hl
    jp z, detect_locker_blue
    ; locker YELLOW
    ld de, 0x1064
    call check_coords_hl
    jp z, detect_locker_yellow
    ; locker GREEN
    ld de, 0x1068
    call check_coords_hl
    jp z, detect_locker_green
    ; locker RED
    ld de, 0x106C
    call check_coords_hl
    jr z, detect_locker_red
    ; locker MAIN
    ld de, 0x1070
    call check_coords_hl
    jr z, detect_locker_me
    ; pot
    ld de, 0x4074
    call check_coords_hl
    jp z, detect_pot
    ; plant
    ld a, (flags4)
    bit 4, a ; FLAG4_4
    jp z, detect_objects2
    ld bc, 0x2448
    ld de, 0x2044
    call detect_is_in_box
    jr nc, detect_plant
detect_objects2:
    ld a, (flags5)
    bit 5, a ; FLAG5_5
    jr z, detect_objects3
    ld bc, 0x1020
    ld de, 0x101C
    call detect_is_in_box
    jr nc, detect_yellowchicken
detect_objects3:
    ; boxes around
    ld hl, (final_map_address)
    inc hl
    ld a, (hl)
    ; boxes have bit 7 set
    cp TILE_INDEX_BOX+128
    jr z, detect_box
    dec hl
    dec hl
    ld a, (hl)
    cp TILE_INDEX_BOX+128
    jr z, detect_box
    ; grids in the floor
    inc hl
    ld a, (hl)
    cp TILE_INDEX_GRID
    jp z, detect_grids
    cp TILE_INDEX_GRID_YELLOW
    jp nz, check_objects ; no more objects

detect_grid_iodine:
    ld a, OBJECT_GRID_IODINE
    ld hl, name_grid
    jp store_object

detect_grids:
    ld a, OBJECT_GRID
    ld hl, name_grid
    jp store_object

detect_yellowchicken:
    ld a, OBJECT_YELLOWCHICKEN
    ld hl, name_yellowchicken
    jp store_object
detect_chicken:
    ld a, OBJECT_CHICKEN
    ld hl, name_chicken
    jp store_object
detect_locker_me:
    ld a, OBJECT_LOCKER_MAIN
    ld hl, name_locker
    jp store_object
detect_locker_red:
    ld a, OBJECT_LOCKER_RED
    ld hl, name_locker
    jp store_object
detect_locker_green:
    ld a, OBJECT_LOCKER_GREEN
    ld hl, name_locker
    jp store_object
detect_locker_blue:
    ld a, OBJECT_LOCKER_BLUE
    ld hl, name_locker
    jp store_object
detect_locker_yellow:
    ld a, OBJECT_LOCKER_YELLOW
    ld hl, name_locker
    jp store_object
detect_plant:
    ld a, OBJECT_PLANT
    ld hl, name_plant
    jp store_object
detect_pot:
    ld a, OBJECT_POT
    ld hl, name_pot
    jp store_object
detect_box:
    ld a, OBJECT_BOX
    ld hl, name_box
    jp store_object
detect_reactor:
    ld a, OBJECT_REACTOR
    ld hl, name_reactor
    jp store_object
detect_radar:
    ld a, OBJECT_RADAR
    ld hl, name_radar
    jp store_object

detect_galena_cristal:
    ld a, OBJECT_GALENA_CRISTAL
    ld hl, name_galena_cristal
    jp store_object

store_object:
    ld (object_detected), a
    ld (object_detected + 1), hl
    ret

store_character:
    ld (character_detected), a
    ld (character_detected + 1), hl
    ret

check_objects:
    ld a, (object_detected)
    and a
    jp z, check_character
    ld hl, (object_detected + 1)
    jp detect_objects_end1
check_character:
    ld a, (character_detected)
    ld hl, (character_detected + 1)
detect_objects_end:
    and a
    jr nz, detect_objects_end1
    ld hl, 0 ; empty sentence
detect_objects_end1:
    ld b, a
    ld a, (last_writen)
    cp b
    ret z ; there is no change
    ld a, b
    ld (last_writen), a
    push hl
    call clear_last_line
    pop hl
    ld a, h
    or l
    jr z, detect_objects_end2
    ld a, 7
    ld de, 0x1708
    call do_print_sentence_unlimited
detect_objects_end2:
    call dump_last_line
    ret



; receives two pairs of coordinates in DE (minor pair)
; and BC (major pair) and returns Carry flag unset if the main character
; is inside that box (not counting the major pair sides)
; HL must contain the main character X and Y coordinates
; with the bits 0 and 1 set to 0
detect_is_in_box:
    ld a, l
    cp d
    ret c
    ld a, h
    cp e
    ret c
    ld a, b
    cp l
    ret c
    ld a, c
    cp h
    ret

; returns Z set if the current coordinates (rounded)
; of the main character are the ones passed in DE (YX)
; HL must contain the main character X and Y coordinates

check_coords_hl:
    ld a, h ; current X
    cp e
    ret nz
    ld a, l ; current Y
    cp d
    ret

; returns Z set if the current coordinates (rounded)
; of the main character are the ones passed in DE (YX)

check_coords:
    ld a, (main_character_x) ; current X
    and 0xFC
    cp e
    ret nz
    ld a, (main_character_y) ; current Y
    and 0xFC
    cp d
    ret


; reads the current location of the character in HL

prepare_detect:
    ld a, (main_character_x)
    and 0xFC
    ld h, a
    ld a, (main_character_y)
    and 0xFC
    ld l, a
    ret
