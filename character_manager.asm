; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

initialize_characters:
    ld ix, characters_table
    ld bc, CHARACTER_ENTRY_SIZE
uci_loop:
    ld a, (ix+0)
    and 0xE3
    ret z
    res 2, (ix+0)
    ld a, (ix+5)
    ld iyl, a
    ld a, (ix+6)
    ld iyh, a
    ld a, (ix+1)
    ld (iy+1), a
    ld a, (ix+2)
    ld (iy+2), a
    ld a, 5
    bit 0, (ix+0)
    jr nz, uci_loop1
    inc a
uci_loop1:
    ld (iy+0), a
    ld a, (ix+7)
    ld iyl, a
    ld a, (ix+8)
    ld iyh, a
    ld a, (ix+1)
    ld (iy+1), a
    ld a, (ix+2)
    ld (iy+2), a
    xor a
    bit 0, (ix+0)
    jr nz, uci_loop2
    inc a
uci_loop2:
    ld (iy+0), a
    ld a, (ix+1)
    sub (ix+3)
    ld d, a
    ld a, (ix+2)
    sub (ix+4)
    or d
    jr z, uci_next_character
    set 2, (ix+0)
uci_next_character:
    add ix, bc
    jp uci_loop


update_characters:
    ld ix, characters_table
uc_loop:
    ld a, (ix+0)
    and 0xE3
    ret z
    ld c, a
    ld a, (ix+1)
    cp (ix+3)
    jr z, uc_x_continue
    set 2, c ; update needed
    set 3, c ; update left-right
    jr c, uc_x_is_lower
    set 5, c ; look to the left
    dec (ix+1)
    jr uc_refresh
uc_x_is_lower:
    res 5, c ; look to the right
    inc (ix+1)
    jr uc_refresh
uc_x_continue:
    ld a, (ix+2)
    cp (ix+4)
    jp z, uc_next_character
    set 2, c ; update needed
    jr c, uc_y_is_lower
    dec (ix+2)
    jr uc_refresh
uc_y_is_lower:
    inc (ix+2)
uc_refresh:
    bit 2, c
    jp z, uc_next_character ; no update is needed
    ld a, c
    add a, 0x40 ; next animation cycle
    ld c, a
    ld d, (ix+1) ; new X
    ld e, (ix+2) ; new Y
    bit 5, c
    jr z, uc_not_left
    dec d
uc_not_left:
    ld a, (ix+5)
    ld iyl, a
    ld a, (ix+6)
    ld iyh, a
    ; now IY points to the body sprite entry
    ld (iy+1), d
    ld (iy+2), e ; body
    ld a, 5
    bit 7, c
    jr nz, uc_set_z0
    bit 6, c
    jr z, uc_set_z1
uc_set_z0:
    inc a
    bit 3, c
    jr nz, uc_set_z1
    bit 0, c
    jr z, uc_set_z1
    inc a ; double jump only if it is movind up-down and is not the main character
uc_set_z1:
    bit 0, c
    jr nz,uc0
    inc a
uc0:
    ld (iy+0), a
    ld hl, sprite_base_right
    bit 5, c
    jr z, uc_is_right
    ld hl, sprite_base_left
uc_is_right:
    ld (iy+4), l
    ld (iy+5), h
    ld a, (ix+7)
    ld iyl, a
    ld a, (ix+8)
    ld iyh, a
    ; now IY points to the feet sprite entry
    ld (iy+1), d
    ld (iy+2), e ; feet
    xor a
    bit 7, c
    jr nz, uc_set_z3
    bit 6, c
    jr z, uc_set_z2
uc_set_z3:
    inc a
    bit 3, c
    jr nz, uc_set_z2
    bit 0, c
    jr z, uc_set_z2
    inc a ; double jump only if it is movind up-down and is not the main character
uc_set_z2:
    bit 0, c
    jr nz,uc1
    inc a
uc1:
    ld (iy+0), a
    ld a, c
    rra
    rra
    rra
    rra
    and 0x0E
    ld de, feet_table
    add a, e
    ld e, a
    ld a, (de)
    ld (iy+4), a
    inc de
    ld a, (de)
    ld (iy+5), a
    res 2, c ; now check if the sprite is in the final place
    ld a, (ix+1)
    sub (ix+3)
    ld d, a
    ld a, (ix+2)
    sub (ix+4)
    or d
    jr z, uc_next_character
    set 2, c
uc_next_character:
    ld (ix+0), c
    ld de, CHARACTER_ENTRY_SIZE
    add ix, de
    jp uc_loop
