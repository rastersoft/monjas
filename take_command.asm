; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

; Manages all the TAKE actions

do_take:
    ld a, VERB_TAKE
    ld hl, command_take
    call print_verb
    call do_take_sub ; after taking anything, refresh the detected objects
    jp refresh_objects_characters ; uses the RET at the end of that subroutine

do_take_sub:
    ld a, (object_detected)
    and a
    jr nz, do_take2
    ld a, (character_detected)
    and a
    jr nz, do_take3
    call make_talk_main
    DEFW sentence_nothing_to_take
    ret
do_take2:
    ; take the object in register A
    cp OBJECT_BOX
    jp z, do_take_box
    cp OBJECT_POT
    jp z, do_take_pot
    cp OBJECT_PLANT
    jp z, do_take_plant
    cp OBJECT_LOCKER_BLUE
    jr z, do_take_locker_blue
    cp OBJECT_LOCKER_RED
    jr z, do_take_locker_red
    cp OBJECT_LOCKER_GREEN
    jr z, do_take_locker_green
    cp OBJECT_LOCKER_YELLOW
    jp z, do_take_locker_yellow
    cp OBJECT_LOCKER_MAIN
    jp z, do_take_locker_main
    cp OBJECT_CHICKEN
    jr z, do_take_chicken
    cp OBJECT_REACTOR
    jr z, do_take_reactor
    cp OBJECT_YELLOWCHICKEN
    jp z, do_take_yellowchicken
    cp OBJECT_GALENA_CRISTAL
    jp z, do_take_galena_cristal
    cp OBJECT_GRID_IODINE
    jp z, do_take_alcohol
    cp OBJECT_RADAR
    jp z, do_take_radar
    call make_talk_main
    DEFW sentence_cant_take_that
    ret

do_take3:
    ; take a character
    call make_talk_main
    DEFW sentence_cant_take_people
    ret

do_take_reactor:
    call make_talk_main
    DEFW sentence_take_reactor
    ret

do_take_chicken:
    call make_talk
    DEFW red_character
    DEFW talk_red_15
    ret

do_take_locker_blue:
    call do_open_locker
    call make_talk_main
    DEFW sentence_take_locker_blue
    ret

do_take_locker_green:
    call do_open_locker
    ld a, (flags2)
    bit 0, a ; FLAG2_0
    jr nz, do_take_locker_green1
    call make_talk_main
    DEFW sentence_take_locker_green1
    ret
do_take_locker_green1:
    ld hl, flags3
    bit 0, (hl) ; FLAG3_0
    jr z, do_take_locker_green2
    res 0, (hl)
    set 1, (hl)
    call make_talk_main
    DEFW sentence_take_locker_green
    ret
do_take_locker_green2:
    call make_talk_main
    DEFW sentence_take_locker_green2
    ret

do_take_locker_red:
    call do_open_locker
    ld a, (flags2)
    bit 5, a ; FLAG2_5
    jr nz, do_take_locker_red2
    call make_talk_main
    DEFW sentence_take_locker_red1
    ret
do_take_locker_red2:
    ld hl, flags4
    bit 0, (hl) ; FLAG4_0
    jr z, do_take_locker_red3
    res 0, (hl) ; FLAG4_0
    set 1, (hl) ; FLAG4_1
    call make_talk_main
    DEFW sentence_take_locker_red2
    ret
do_take_locker_red3:
    call make_talk_main
    DEFW sentence_take_locker_red3
    ret

do_take_locker_yellow:
    ld a, (flags1)
    bit 4, a ; FLAG1_4
    jr nz, do_take_locker_yellow2
do_take_locker_yellow3
    call make_talk_main
    DEFW sentence_take_locker_yellow1
    ret
do_take_locker_yellow2:
    call do_open_locker
    ld hl, flags5
    bit 2, (hl) ; FLAG5_2
    jr z, do_take_locker_yellow3
    res 2, (hl) ; FLAG5_2
    set 3, (hl) ; FLAG5_3
    call make_talk_main
    DEFW sentence_take_locker_yellow2
    ret

do_take_locker_main:
    call do_open_locker
    ld hl, flags5
    bit 0, (hl) ; FLAG5_0
    jr z, do_take_locker_main2
    res 0, (hl) ; FLAG5_0
    set 1, (hl) ; FLAG5_1
    call make_talk_main
    DEFW sentence_take_locker_main1
    ret
do_take_locker_main2:
    call make_talk_main
    DEFW sentence_nothing_to_take
    ret

do_take_plant:
    ld hl, flags4
    res 4, (hl) ; FLAG4_4
    set 5, (hl) ; FLAG4_5
    ld a, 255
    ld (entry_chuck_the_plant), a ; make the sprite of Chuck dissappear
    ret

do_take_yellowchicken:
    call make_talk_main
    DEFW sentence_take_yellowchicken
    ld hl, flags5
    res 5, (hl) ; FLAG5_5
    set 6, (hl) ; FLAG5_6
    ld a, 255
    ld (engineer_chicken), a
    ret

do_take_box:
    call make_talk_main
    DEFW sentence_take_box
    ret
do_take_pot:
    call make_talk_main
    DEFW sentence_take_pot
    ret

do_take_galena_cristal:
    ld hl, flags3
    bit 6, (hl) ; FLAG3_6
    jr z, do_take_galena_cristal1
    call make_talk_main
    DEFW sentence_already_have_galena_cristal
    ret
do_take_galena_cristal1:
    set 6, (hl) ; FLAG3_6
    ld hl, flags6
    set 2, (hl) ; FLAG6_2
    ld hl, flags5
    res 7, (hl) ; FLAG5_7 Turn off the galena detector
    call make_talk_main
    DEFW sentence_take_galena_cristal
    ret

do_take_alcohol:
    ld hl, flags3
    bit 3, (hl)
    jr nz, do_take_alcohol_already_have
    bit 4, (hl)
    jr nz, do_take_alcohol_already_have
    set 3, (hl)
    call make_talk_main
    DEFW sentence_take_alcohol
    ret
do_take_alcohol_already_have:
    call make_talk_main
    DEFW sentence_already_took_alcohol
    ret

do_take_radar:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW sentence_take_radar
    call make_talk_main
    DEFW sentence_take_radar2
    ret
