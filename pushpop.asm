    pop af
    pop bc
    pop de
    pop ix
    exx
    ex af, af'
    pop af
    pop bc
    pop de
    pop iy
    ld sp, hl
    push iy
    push de
    push bc
    push af
    ld de, 240
    add hl, de
    exx
    ex af, af'
    push ix
    push de
    push bc
    push af


; Copy 32 bytes from HL to HL'-16

    ld sp, hl
    ld a, 16
    add a, l
    ld l, a
    pop af
    pop bc
    pop de
    pop ix
    exx
    ex af, af'
    pop af
    pop bc
    pop de
    pop iy
    ld sp, hl
    push iy
    push de
    push bc
    push af
    ld de, 16
    add hl, de
    exx
    ex af, af'
    push ix
    push de
    push bc
    push af

    ld sp, hl
    ld a, 16
    add a, l
    ld l, a
