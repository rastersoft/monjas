; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

characters_table:
main_character:
    ; the main character must have this label, because its coordinates are used to set the map position
    DEFB 0x01 ; bit 0: is main character
              ; bit 1 is not main character
              ; bit 4: being updated
              ; bit 5: 1 = looks to the left; 0 = looks to the right
              ; bit 6-7: current step animation
              ; if the byte is 0, end of table
    DEFB STARTX ; current X coordinate
    DEFB STARTY ; current Y coordinate
main_character_x:
    DEFB STARTX ; desired X coordinate
main_character_y:
    DEFB STARTY ; desired Y coordinate
    DEFW (main_character_sprites + 0) ; pointer to the entry in the sprite table for body. The main character must use the last one
    DEFW (main_character_sprites + 6) ; pointer to the entry in the sprite table for feet. The main character must use the last one

    ; engineer
yellow_character:
    DEFB 0x02 ; not main character
    DEFB 24 ; current X coordinate
    DEFB 18 ; current Y coordinate
    DEFB 24 ; desired X coordinate
    DEFB 18 ; desired Y coordinate
    DEFW (engineer); pointer to the entry in the sprite table for body
    DEFW (engineer + 6) ; pointer to the entry in the sprite table for feet

    ; chef
red_character:
    DEFB 0x02 ; not main character
    DEFB 116 ; current X coordinate
    DEFB 66 ; current Y coordinate
    DEFB 116 ; desired X coordinate
    DEFB 66 ; desired Y coordinate
    DEFW (chef); pointer to the entry in the sprite table for body
    DEFW (chef + 6) ; pointer to the entry in the sprite table for feet

    ; radarist
green_character:
    DEFB 0x02 ; not main character
    DEFB 24 ; current X coordinate
    DEFB 42 ; current Y coordinate
    DEFB 24 ; desired X coordinate
    DEFB 42 ; desired Y coordinate
    DEFW (radarist_sprites); pointer to the entry in the sprite table for body
    DEFW (radarist_sprites + 6) ; pointer to the entry in the sprite table for feet

    ; station comander
blue_character:
    DEFB 0x02 ; not main character
    DEFB 64 ; current X coordinate
    DEFB 22 ; current Y coordinate
    DEFB 64 ; desired X coordinate
    DEFB 22 ; desired Y coordinate
    DEFW (comander_sprites); pointer to the entry in the sprite table for body
    DEFW (comander_sprites + 6) ; pointer to the entry in the sprite table for feet

    ; spaceship captain
white_character:
    DEFB 0x02 ; not main character
    DEFB 160 ; current X coordinate
    DEFB 22 ; current Y coordinate
    DEFB 160 ; desired X coordinate
    DEFB 22 ; desired Y coordinate
    DEFW (captain_sprites); share the entries with the spaceship
    DEFW (captain_sprites + 6)

    ; table end
    DEFB 0

; contains a list with the sprites to be painted over the floor
sprites_table:
chef:
    ; chef character body and feet
    DEFB 6 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 40 ; X coordinate in the map
    DEFB 32 ; Y coordinate in the map
    DEFB 0x50 ; replacement color
    DEFW sprite_base_right ; pointer to the sprite

    DEFB 1 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 40 ; X
    DEFB 32 ; Y
    DEFB 0x50 ; replacement color
    DEFW sprite_feet_right1 ; pointer to the sprite

engineer:
    ; engineer (yellow) character body and feet
    DEFB 6 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 40 ; X coordinate in the map
    DEFB 38 ; Y coordinate in the map
    DEFB 0x70 ; replacement color
    DEFW sprite_base_right ; pointer to the sprite

    DEFB 1 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 40 ; X
    DEFB 38 ; Y
    DEFB 0x70 ; replacement color
    DEFW sprite_feet_right1 ; pointer to the sprite

engineer_chicken:
    DEFB 255 ;
    DEFB 28 ; X
    DEFB 16 ; Y
    DEFB 0x70 ; replacement color
    DEFW sprite_amongchicken

    ; comander (blue) sprites
comander_sprites:
    DEFB 6 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 40 ; X coordinate in the map
    DEFB 38 ; Y coordinate in the map
    DEFB 0x48 ; replacement color
    DEFW sprite_base_right ; pointer to the sprite

    DEFB 1 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 40 ; X
    DEFB 38 ; Y
    DEFB 0x48 ; replacement color
    DEFW sprite_feet_right1 ; pointer to the sprite

    ; spaceship captain (white) sprites
captain_sprites:
    DEFB 255 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 20 ; X coordinate in the map
    DEFB 66 ; Y coordinate in the map
    DEFB 0x78 ; replacement color
    DEFW sprite_base_right ; pointer to the sprite

    DEFB 255 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 20 ; X
    DEFB 66 ; Y
    DEFB 0x48 ; replacement color
    DEFW sprite_feet_right1 ; pointer to the sprite


    ; radarist (green) sprites
radarist_sprites:
    DEFB 6 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 40 ; X coordinate in the map
    DEFB 38 ; Y coordinate in the map
    DEFB 0x20 ; replacement color
    DEFW sprite_base_right ; pointer to the sprite

    DEFB 1 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 40 ; X
    DEFB 38 ; Y
    DEFB 0x20 ; replacement color
    DEFW sprite_feet_right1 ; pointer to the sprite

marvin_sprites:
    DEFB 4 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 8 ; X
    DEFB MAX_MAP_Y - 12 ; Y
    DEFB 0x00 ; replacement color
    DEFW sprite_marvin_left ; pointer to the sprite
    DEFB 4 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 12 ; X
    DEFB MAX_MAP_Y - 12 ; Y
    DEFB 0x00 ; replacement color
    DEFW sprite_marvin_right ; pointer to the sprite

entry_spaceship:
    ; WHITE's spaceship
    DEFB 255 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 20 ; X
    DEFB 73 ; Y
    DEFB 0x70 ; replacement color
    DEFW sprite_ship_left ; pointer to the sprite
entry_spaceship2:
    DEFB 255 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 24 ; X
    DEFB 73 ; Y
    DEFB 0x70 ; replacement color
    DEFW sprite_ship_right ; pointer to the sprite

entry_chuck_the_plant:
    ; plant
    DEFB 4
    DEFB 68
    DEFB 36
    DEFB 0x00
    DEFW sprite_plant

entry_chicken:
    ; chicken
    DEFB 2
    DEFB 112
    DEFB 76
    DEFB 0x30
    DEFW sprite_chicken

main_character_sprites:
    ; main character body and feet. The main character is at Z=0 to avoid overlapping with other characters
    DEFB 5 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 40 ; X coordinate in the map
    DEFB 38 ; Y coordinate in the map
    DEFB 0x58 ; replacement color
    DEFW sprite_base_right ; pointer to the sprite

    DEFB 0 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 40 ; X
    DEFB 38 ; Y
    DEFB 0x58 ; replacement color
    DEFW sprite_feet_right1 ; pointer to the sprite

entry_pot:
    ; pot
    DEFB 5 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 120 ; X
    DEFB 64 ; Y
    DEFB 0x70 ; replacement color
    DEFW sprite_pot1 ; pointer to the sprite

entry_nucelar_reactor:
    ; nucelar reactor
    DEFB 4 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 18 ; X
    DEFB 12 ; Y
    DEFB 0x70 ; replacement color
    DEFW sprite_reactor_left ; pointer to the sprite
    DEFB 4 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 22 ; X
    DEFB 12 ; Y
    DEFB 0x70 ; replacement color
    DEFW sprite_reactor_right ; pointer to the sprite

entry_radar_screen:
    ; radar_screen
    DEFB 255 ; Z. if 255, this entry is unused; if 254, it is the end of the table
    DEFB 72 ; X
    DEFB 64 ; Y
    DEFB 0x70 ; replacement color
    DEFW sprite_radar_active ; pointer to the sprite

    DEFB 254 ; end of table
