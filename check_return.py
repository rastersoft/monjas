#!/usr/bin/env python3

import glob

for name in glob.glob("*.asm"):
    with open(name, "rb") as f:
        line = 0
        for l in f.readlines():
            l = l.decode('latin1')
            line += 1
            pos = l.find(";")
            if pos != -1:
                l = l[:pos]
            l = l.strip()
            if l.startswith("return"):
                print(f"File {name} has a return at line {line}")
