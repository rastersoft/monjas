; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

; Manages all the LOOK commands

do_look:
    ld a, VERB_LOOK
    ld hl, command_look
    call print_verb
    call ask_inventory
    cp VERB_RETURN
    ret z
    and a
    jp z, do_look_nothing
    cp CHARACTER_BLUE
    jp z, do_look_blu
    cp CHARACTER_RED
    jp z, do_look_red
    cp CHARACTER_YELLOW
    jp z, do_look_yellow
    cp CHARACTER_GREEN
    jp z, do_look_green
    cp CHARACTER_WHITE
    jp z, do_look_white
    cp CHARACTER_MARVIN
    jp z, do_look_marvin
    cp OBJECT_GRID
    jp z, do_look_grid
    cp OBJECT_RADAR
    jp z, do_look_radar
    cp OBJECT_BOX
    jp z, do_look_box
    cp OBJECT_POT
    jp z, do_look_pot
    cp OBJECT_PLANT
    jp z, do_look_plant
    cp OBJECT_LOCKER_YELLOW
    jp z, do_look_locker_yellow
    cp OBJECT_LOCKER_BLUE
    jp z, do_look_locker_blue
    cp OBJECT_LOCKER_GREEN
    jp z, do_look_locker_green
    cp OBJECT_LOCKER_RED
    jp z, do_look_locker_red
    cp OBJECT_LOCKER_MAIN
    jp z, do_look_locker_main
    cp OBJECT_RUBBER_CHICKEN
    jp z, do_look_rubber_chicken
    cp OBJECT_DIARY
    jp z, do_look_diary
    cp OBJECT_USB_CABLE
    jr z, do_look_usb_cable
    cp OBJECT_GALENA_DIODE
    jr z, do_look_galena_diode
    cp OBJECT_CHICKEN
    jr z, do_look_chicken
    cp OBJECT_REACTOR
    jr z, do_look_reactor
    cp OBJECT_YELLOWCHICKEN
    jr z, do_look_yellowchicken
    cp OBJECT_GALENA_DETECTOR
    jp z, do_look_galena_detector
    cp OBJECT_GALENA_CRISTAL
    jp z, do_look_galena_cristal
    cp OBJECT_IODINE
    jp z, do_look_iodine
    cp OBJECT_GRID_IODINE
    jp z, do_look_grid_iodine
    cp OBJECT_BOOZE
    jp z, do_look_booze
    call make_talk_main
    DEFW sentence_nothing_to_see
    ret

do_look_print_name:
    ld de, (last_print_coords)
    ld a, 7
    call do_print_sentence_unlimited
    call dump_last_line
    ret

do_look_nothing:
    call make_talk_main
    DEFW sentence_nothing_to_see
    ret

do_look_yellowchicken:
    call make_talk_main
    DEFW sentence_look_yellowchicken
    ld hl, name_yellowchicken
    jp do_look_print_name

do_look_reactor:
    call make_talk_main
    DEFW sentence_look_reactor
    ld hl, name_reactor
    jp do_look_print_name

do_look_chicken:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW sentence_look_chicken
    call make_talk
    DEFW red_character
    DEFW talk_red_15
    ld hl, name_chicken
    jp do_look_print_name

do_look_galena_diode:
    call make_talk_main
    DEFW sentence_look_galena_diode
    ld hl, name_galena_diode
    jp do_look_print_name

do_look_usb_cable:
    call make_talk_main
    DEFW sentence_look_usb_cable
    ld hl, name_usb_cable
    jp do_look_print_name

do_look_diary:
    ld a, (flags4)
    bit 2, a ; FLAG4_2
    jr nz, do_look_diary2
    call make_talk_main
    DEFW sentence_look_diary
    jr do_look_diary3
do_look_diary2:
    call make_talk_main
    DEFW sentence_look_diary2
do_look_diary3:
    ld hl, name_diary
    jp do_look_print_name

do_look_rubber_chicken:
    call make_talk_main
    DEFW sentence_look_rubber_chicken
    ld hl, name_rubber_chicken
    jp do_look_print_name

do_look_locker_yellow:
    call do_open_locker
    call make_talk_main
    DEFW sentence_look_locker_yellow
    ld hl, name_locker
    jp do_look_print_name

do_look_locker_red:
    call do_open_locker
    ld a, (flags4)
    bit 0, a ; FLAG4_0
    jr z, do_look_locker_red2
    call make_talk_main
    DEFW sentence_look_locker_red_with_cable
    jr do_look_locker_red3
do_look_locker_red2:
    call make_talk_main
    DEFW sentence_look_locker_red_no_cable
do_look_locker_red3:
    ld hl, flags2 ; FLAG2_2
    set 2, (hl) ; set that we saw the USB pendrive and cable
    ld hl, name_locker
    jp do_look_print_name

do_look_locker_green:
    call do_open_locker
    ld a, (flags3)
    bit 0, a ; FLAG3_0
    jr z, do_look_locker_green2
    call make_talk_main
    DEFW sentence_look_locker_green_with_chicken
    jr do_look_locker_green3
do_look_locker_green2:
    call make_talk_main
    DEFW sentence_look_locker_green_no_chicken
do_look_locker_green3:
    ld hl, name_locker
    jp do_look_print_name


do_look_locker_blue:
    call do_open_locker
    call make_talk_main
    DEFW sentence_look_locker_blue
    ld hl, name_locker
    jp do_look_print_name


do_look_locker_main:
    call do_open_locker
    ld a, (flags5)
    bit 0, a ; FLAG5_0
    jr z, do_look_locker_main2
    call make_talk_main
    DEFW sentence_look_locker_main_with_diary
    jr do_look_locker_main3
do_look_locker_main2:
    call make_talk_main
    DEFW sentence_look_locker_main_no_diary
do_look_locker_main3:
    ld hl, name_locker
    jp do_look_print_name


do_look_plant:
    call make_talk_main
    DEFW sentence_look_plant
    ld hl, name_plant
    jp do_look_print_name
do_look_box:
    call make_talk_main
    DEFW sentence_look_box
    ld hl, name_box
    jp do_look_print_name
do_look_pot:
    call make_talk_main
    DEFW sentence_look_pot
    ld hl, name_pot
    jp do_look_print_name
do_look_grid:
    call make_talk_main
    DEFW sentence_look_grid
    ld hl, name_grid
    jp do_look_print_name
do_look_radar:
    ld a, (flags1)
    and 0x03
    cp 0x01 ; FLAG1_10 = 01
    jr z, do_look_radar2
    call make_talk_main
    DEFW sentence_look_radar_empty
    ld hl, name_radar
    jp do_look_print_name
do_look_radar2:
    call make_talk_main
    DEFW sentence_look_radar_active
    ld hl, name_radar
    jp do_look_print_name

do_look_blu:
    call make_talk_main
    DEFW sentence_look_blue
    ld hl, name_blue
    jp do_look_print_name

do_look_red:
    call make_talk_main
    DEFW sentence_look_red
    ld hl, name_red
    jp do_look_print_name

do_look_yellow:
    call make_talk_main
    DEFW sentence_look_yellow
    ld hl, name_yellow
    jp do_look_print_name

do_look_green:
    call make_talk_main
    DEFW sentence_look_green
    ld hl, name_green
    jp do_look_print_name

do_look_white:
    call make_talk_main
    DEFW sentence_look_white
    ld hl, name_white
    jp do_look_print_name

do_look_marvin:
    call make_talk_main
    DEFW sentence_look_marvin
    ld hl, name_marvin
    jp do_look_print_name

do_look_galena_detector:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW look_detector_1
    ld hl, (flags6)
    bit 0, (hl) ; FLAG6_0
    set 0, (hl)
    jr nz, do_look_galena_detector2
    call task_make_talk_and_wait
    DEFW main_character
    DEFW look_detector_2
    call task_make_talk_and_wait
    DEFW main_character
    DEFW look_detector_3
do_look_galena_detector2:
    call make_talk_main
    DEFW look_detector_4
    ret

do_look_galena_cristal:
    call make_talk_main
    DEFW sentence_look_galena_cristal
    ld hl, name_galena_cristal
    jp do_look_print_name

do_look_iodine:
    call make_talk_main
    DEFW sentence_look_iodine
    ld hl, name_iodine
    jp do_look_print_name

do_look_grid_iodine:
    call make_talk_main
    DEFW sentence_look_tinted_grid
    ld hl, name_grid
    jp do_look_print_name

do_look_booze:
    call make_talk_main
    DEFW sentence_look_booze
    ld hl, name_booze
    jp do_look_print_name

do_open_locker:
    ld b, 4
    call task_wait_B_frames
    ld hl, (final_map_address)
    ld de, MAP_WIDTH
    and a
    sbc hl, de
    ld a, (hl)
    push af
    push hl
    ld (hl), TILE_INDEX_LOCKER_OPEN
    ld b, 8
    call task_wait_B_frames
    pop hl
    pop af
    ld (hl), a
    ret
