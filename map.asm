; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

paint_map:
    ld a, (main_character + 1)
IF DEBUG_COORDS
IF DODEBUG
    call do_debug
ENDIF
ENDIF
    sub MAP_OFFSET_X
    and 0xFC
    ld (current_map_x), a
    rra ; it is the same than srl a because in line 53 the bits 0 and 1, and C are reset
    rra ; but it is twice as fast
    ld c, a
    ld a, (main_character + 1)
    sub MAP_OFFSET_X
    and 0x03
    neg
    ld (map_x_offset), a
    ld a, (main_character + 2)
IF DEBUG_COORDS
IF DODEBUG
    call do_debug
ENDIF
ENDIF
    sub MAP_OFFSET_Y
    and 0xFC
    ld (current_map_y_painting), a
    ld (current_map_y), a
    rra
    rra ; the same
    ld l, a
    ld a, (main_character + 2)
    sub MAP_OFFSET_Y
    and 0x03
    neg
    ld (map_y_offset), a
    ld d, a
    ld b, 0
    ld a, (main_character + 2)
    ; If we arrive to this Y coordinate, just repeat the last 9 rows
    ; This allows to make a big "external" zone but without having to store the
    ; whole map.
    cp MAP_LAST_STATION_LINE*4+MAP_OFFSET_Y
    jr c, paint_map0
    ld hl, map_array + 32*MAP_LAST_STATION_LINE
    add hl, bc
    jr paint_map00
paint_map0:
    ld h, 0
    add hl, hl
    add hl, hl
    add hl, hl
    add hl, hl
    add hl, hl ; Y * 32
    add hl, bc ; map offset
    ld bc, map_array
    add hl, bc ; map address
paint_map00:
    ld (current_map_address), hl
    ld c, 6+EXTRA_Y ; height
paint_map1: ; external loop
    ld a, (map_x_offset)
    ld e, a
    ld a, 9 ; width
    push bc
    di ; while painting the background, disable interrupts to make it as fast as possible
paint_map2: ; internal loop
    ex af, af'
    ld a, (hl)
    and 0x7F ; ignore the "don't step" flag
    jr z, paint_map4 ; 0 is "don't paint"
    push de
    push hl
    add a, a ; multiply a by 2, to use it as an offset in a 16-bit table
    ld h, 0x9B ; the upper-part of the sprite table
    ld l, a
    ld a, (hl)
    inc hl
    ld h, (hl)
    ld l, a ; now HL points to the sprite to paint here
    ld a, (hl) ; read sprite size
    bit 2, a ; check if the height is greater than 4
    jp z, paint_map5 ; the mayority has 4, so better optimize for the most common case
    and 0x07
    sub 3
    sub d
    neg
    ld d, a
paint_map5:
    call paint_sprite
    pop hl
    pop de
paint_map4:
    inc hl ; next element in map
    ld a, 4
    add a, e
    ld e, a
    ex af, af'
    dec a
    jp nz, paint_map2
    ei ; to read more times the keyboard
    ; now paint the elements that are over the floor
    push hl
    push de
    ld ix, sprites_table
    ld iy, current_map_x
    ld d, (iy+4) ; current map Y position being painted
    ld e, 254 ; compare value
paint_map8:
    ; paint all the out-of-the-map sprites (main character, other characters, objects...)
    ld a, (ix+0) ; Z coordinate
    cp e ; compare with 254
    jr z, paint_map6 ; A == 254 -> end of table
    jr nc, paint_map7 ; C=0 -> A >= 254 So if A=255 will jump this entry ("don't paint this sprite")
    ld a, (ix+2) ; Y coordinate
    and 0xFC ; an element can be in any of the four Y coordinates of each map element
    cp d ; D contains the current map Y position being painted
    jp nz, paint_map7 ; the most common case is to DON'T paint the sprites, so optimize for that case
    push de ; preserve DE values
    ld a, (ix+2) ; Y coordinate
    sub (iy+1) ; current_map_y
    add a, (iy+3) ; map_y_offset
    sub (ix+0) ; adjust Y coordinate with Z coordinate
    ld d, a
    ld a, (ix+1) ; X coordinate
    sub (iy+0) ; current_map_x
    add a, (iy+2) ; map_x_offset
    ld e, a
    ld l, (ix+4)
    ld h, (ix+5) ; sprite address
    ld a, (ix+3) ; replacement color
    push iy
    ld iyl, a
    push ix
    call paint_sprite
    ei
    pop ix
    pop iy
    pop de ; restore the current map Y position being painted and the compare value
paint_map7:
    ld bc, SPRITE_ENTRY_SIZE
    add ix, bc ; next entry
    jp paint_map8
paint_map6:
    pop de
    pop hl
    ld a, 4
    add a, d
    ld d, a
    ld b, 0
    ld c, MAP_JUMP ; jump to the next map line
    add hl, bc
    ld a, (current_map_y_painting)
    add a, 4
    ld (current_map_y_painting), a
    pop bc
    dec c
    ; next row of the map
    jp nz, paint_map1
    ret

