name_white:
    DEFB "Blanc",0
sentence_use_iodine_in_white:
    DEFB "No quiero estropear el b",144,1,"o\nnuclear de su traje.",0
sentence_look_yellowchicken:
    DEFB "Pues se ve apetitoso...",0
talk_red_19:
    DEFB "Azu ha prohibido los invitados.\nEstamos muy justos de comida.",0
sentence_look_radar_empty2:
    DEFB "No hay nada en el radar.",0
sentence_land_ship5:
    DEFB "Bienvenido a la...",0
talk_marvin_11:
    DEFB "No se como buscar\nun cristal.",0
talk_red_12:
    DEFB "%Puedo coger ese pollo?",0
sentence_take_radar:
    DEFB 144,185,"oy TAN ",128,36,"ptomano.",0
talk_blu_6c:
    DEFB "Entonces hazlo tu,\nyo tengo mucho lio.",0
sentence_look_iodine:
    DEFB "Es un bote de yodo\ndesinfectante.\n&Agh, pringa!",0
talk_white_11:
    DEFB "Hmmm... Oiga %Tendria espacio\npara un acompa*a",129,71,"?",0
talk_yellow_7:
    DEFB "&No toques el reactor!",0
talk_white_cristal1:
    DEFB "Tengo el cristal de galena.",0
talk_white_22:
    DEFB "Hasta luego.",0
talk_green_3:
    DEFB "%Otra vez",128,41," borrachera?",0
command_give:
    DEFB "Dar ",0
sentence_cover_7:
    DEFB "Sinclair1/OPQA/Kemp",128,125,"n: mover",0
talk_blu_4:
    DEFB "L",129,198,"o le abrire otro expediente,\npero ya sabes que no puedo hacer\nmas, es familia de",160,95,"jefes.",0
sentence_take_yellowchicken:
    DEFB "No creo que\nle importe y",160,180
sentence_take_reactor:
    DEFB "%En serio ",130,102,194,50,"me\nvoy a meter un",209,153,224,33,176,153,"bolsi",160,234
talk_blu_5:
    DEFB "S",128,64,"cerca una nave.",0
sentence_look_locker_red_no_cable:
    DEFB "Esta es la taquilla de Roj.\nEsta todo muy ordenado. Hay\nvarios libros de cocina y un\npendri",146,184
talk_marvin_2:
    DEFB "Hola Ros.",0
talk_blu_7:
    DEFB "%Que hace?",0
talk_red_2c:
    DEFB "Ya, pero conozc",162,224,"\nbien a tu estomago.",0
sentence_look_locker_red_with_cable:
    DEFB 242,188,242,198,242,208,242,218,242,228,242,238,242,248,227,2,",",243,13,128,63,"y un cable USB.",0
sentence_look_radar_active:
    DEFB "&U",194,179,176,8,"e aterrizar!\nAvisare a Verd.",0
sentence_nothing_to_see:
    DEFB "No veo nada.",0
sentence_take_box:
    DEFB "Pesa demasiado.",0
talk_red_7:
    DEFB "Es que en aquella\nweb decian que...",0
sentence_land_ship1:
    DEFB "Aqui estacion Tipo-Monjas\nEpsilon 9. R",129,112,"on",144,137
command_use:
    DEFB "Us",145,229
sentence_cover_6:
    DEFB "http://www.rastersoft.",128,133,0
talk_white_1:
    DEFB 240,166,".",0
talk_marvin_10:
    DEFB "Lo siento, estoy\nr",128,68,"r",162,235,161,104,"as que\n",176,196,161,15," mism",129,0
talk_red_13:
    DEFB "No, que te lo ",128,133,146,95
talk_white_18:
    DEFB "Un generador de oxigeno.",0
sentence_end3:
    DEFB "Esto no pinta\n",160,148,147,64,144,75
talk_white_17:
    DEFB "Un mapa estelar.",0
sentence_swap_chicken1:
    DEFB "(Nada por a",128,9,",\nn",228,167,"l",160,179,")",0
sentence_take_locker_green2:
    DEFB "No necesito una botella vacia.",0
talk_white_19:
    DEFB 160,55,"y",148,169,131,174,"os\nirnos. %Vamos?",0
talk_red_9:
    DEFB 177,163,"uno, pero",226,56,"\ndejartel",132,50,"l",212,43,".",0
talk_red_18:
    DEFB "%Po",131,174,224,97,132,173,"l\ncapitan ",160,0," a comer?",0
talk_marvin_15:
    DEFB "Disfruta.",0
name_booze:
    DEFB "Aguar",178,31,0
talk_blu_8:
    DEFB "Cosas urgentes, Ros.\nT",241,26,177,36
talk_green_9:
    DEFB "S",129,223," mejor que hable con Azu.",0
talk_white_20:
    DEFB "&&&C",132,160,"o!!!",0
sentence_intro17:
    DEFB "Una maldita",243,225,"espacial",146,38,"dida que se ca",147,146,"c",129,220,147,31
sentence_look_locker_green_no_chicken:
    DEFB "E",242,194,"lla de Verd. Esta\nllena de botellas vacias.",0
talk_marvin_14:
    DEFB 241,191,"o Marvin.",0
talk_yellow_3:
    DEFB "El horno de la co",163,7,"no\nfunciona.",0
talk_marvin_5:
    DEFB "%",129,191," dejado la mina?",0
talk_white_maps1:
    DEFB "Ya",161,24,"o los mapas.",0
sentence_rubber_chicken_pot3:
    DEFB "&Te he oido!",0
command_talk:
    DEFB "H",131,111,145,229
sentence_use_reactor:
    DEFB "Mejor no lo toc",130,237,131,36,"ma.",0
sentence_look_blue:
    DEFB "Es",149,115,", medico y comandante\n",165,244,227,225,".",0
talk_blu_6b:
    DEFB 147,150,163,225,209,215,160,74
sentence_take_locker_red1:
    DEFB "N",149,242,"beria",160,221,"rlo\nsin permiso.",0
sentence_look_booze:
    DEFB 240,185,146,59,197,183," be",134,132,"se\nes",161,50,"satasc",212,111,"ca*er",165,218
sentence_take_locker_yellow2:
    DEFB "Cogere solo el\ndiodo",241,179,".",0
sentence_look_green:
    DEFB "B",177,217,"o,",176,190,"siempre.\n",147,36,"t",144,207,163,216
talk_green_12:
    DEFB 192,79,"encontrad",148,198,130,0,145,4,"\n",245,203,"s con alcohol...",0
talk_white_food2:
    DEFB "&Oh! ",163,35,"e",132,241
sentence_too_far:
    DEFB 230,66,"me alejo mas.",0
sentence_intro2:
    DEFB 163,35,"nueva aventura ",135,29,"deparara\nhoy? %Luchar ",182,240,129,24,"mi",131,112,"s\nseres extra",147,130,144,16,"es...?",0
sentence_green_iodine2:
    DEFB 145,141,128,230,"reee... ocupesh... con\nel vo",167,124,"mit",144,74," no sh",146,55,149,54
talk_green_8:
    DEFB "Puessshhh i",163,157,"... -hic-\ndosssshhhh. Jijij",135,201,".",0
sentence_green_iodine1:
    DEFB "Ups... Perdo",133,232
talk_white_food1:
    DEFB "Eh",161,93,"E",182,239,"e\nesta",224,132
talk_green_6:
    DEFB "E",151,168,"ooo nun",167,135,133,195,151,241,167,169,"a",130,15,"a",151,190,".",0
talk_marvin_4:
    DEFB "Descansar.\nM",166,50,"jubil",163,180
talk_blu_1:
    DEFB "Buenas,",246,103,128,52
talk_white_9:
    DEFB "S",128,61,"e un poco\npeque*",160,180
sentence_give_iodine_to_white:
    DEFB "%Servira para limpiar\n",241,169,241,179,"?",0
sentence_intro8:
    DEFB "&",135,209,"! Vaya, le",134,203," a",145,152,"set. Juas.",0
sentence_look_chicken:
    DEFB "M",177,91,240,59,192,69,0
name_rubber_chicken:
    DEFB "Pollo de goma",0
sentence_not_now:
    DEFB "Ahora n",209,23,"gana",144,108,"n",162,220,"s",160,95,"s",130,33,"i",144,106,0
talk_red_10:
    DEFB "%Tienes",243,106,131,116,"?",0
talk_red_2b:
    DEFB "Pero si no he\n",146,29,"d",195,161,128,76
name_radar:
    DEFB "R",144,160,0
sentence_look_diary2:
    DEFB "Es mi diario electronico con\n",230,37,212,155,146,95
sentence_talk_plant:
    DEFB "&",147,25," Chuck!",0
talk_white_cristal4:
    DEFB 160,185,145,94,162,142,192,195,".",0
talk_white_12:
    DEFB "%U",241,126,145,71,135,112," ",193,90,"\n",144,216,149,162,"r",243,48,164,155," hambre\nn",176,142,144,9,"n",129,143,"me",162,177,131,207,"ision.",0
sentence_intro7:
    DEFB "&Oye!\n&Dev",131,197,"vemel",134,58
sentence_a_ship:
    DEFB "&La",151,31,"r",129,194,"d",208,156,137,61,"Y ",177,148,"turno",229,182,"..!",0
talk_marvin_13:
    DEFB "Gracias...",162,101,".",0
sentence_intro13:
    DEFB "Ro",167,211,242,125,"sig",128,56,"\n",149,112,131,170,"s fantasias?",0
sentence_cover_5:
    DEFB 127,"2021 R",164,25," S",132,31,"w",147,144,"Vigo",0
talk_green_5:
    DEFB "Me pregunto donde esc",153,181,132,24,"\n",145,69,"o",247,5,".",0
talk_red_5:
    DEFB 192,215,"us",144,23,197,236,"?",0
talk_green_2:
    DEFB 133,121,"&B",168,183,"hi ",199,167,"\n-hic- R",167,189,"s!",149,128
sentence_cover_2:
    DEFB "1",152,111,"gar",0
talk_yellow_2:
    DEFB "Pierdete Imbecil.\nO",148,92,"aso en",241,150,128,163
talk_white_food3:
    DEFB "E",167,211,"u*",146,86,148,211,128,138
sentence_land_ship6:
suspensive_dots:
    DEFB 144,75
sentence_take_pot:
    DEFB 240,6,152,9,"ldarme.",0
talk_marvin_9:
    DEFB "%Me puedes consegui",240,201,177,177,"G",161,184,"?",0
talk_white_14:
    DEFB "Me falta aun:",0
talk_white_food10:
    DEFB "Esa",168,135,144,15,243,172,"o\ncruda para ",136,210,"g",144,124,".",0
talk_yellow_4:
    DEFB "&",209,205,"! Ya",180,52,128,82,"r",153,177,"e\n",130,211,131,11,"su",197,135,"o",181,237,".\n&No s",202,45," arregla",129,160
talk_blu_11:
    DEFB "%Tenemos alcohol?",0
name_galena_cristal:
    DEFB "C",241,173,177,183,0
talk_blu_3:
    DEFB "Amarill me ha borra",129,58,248,210,"personal.",0
sentence_look_reactor:
    DEFB "E",241,149,132,113,240,34," la\n",211,226,". N",134,2,"a",144,82,214,1,129,58,227,174," bien.",0
look_detector_2:
    DEFB 201,104,180,170,"d",128,9,"ri",144,24,"\nfabuloso detector",241,179,"\nMiKas",128,138
sentence_look_usb_cable:
    DEFB "U",243,108,"B",130,56,"rm",144,211
sentence_cant_take_that:
    DEFB "No p",240,217," eso.",0
talk_marvin_1:
    DEFB 147,25,229,226
sentence_poor_chuck:
    DEFB 129,140,"! &Po",137,32,216,245
sentence_nothing_to_take:
    DEFB 240,141,181,158,160,222,".",0
talk_yellow_8:
    DEFB "O",129,79,"o",151,134,0
sentence_take_locker_yellow1:
    DEFB 240,6,128,148,"a de Amarill.",0
talk_red_2:
    DEFB 227,25," No",218,46,"pi",128,199,160,147,240,109,240,123,208,133
sentence_look_radar_empty:
    DEFB "E",192,157,182,195,"cubr",168,43,"s",171,19,194,37,"su",198,39," ",155,128,128,153,151,45,129,54,"\ns",151,3,"mpli",147,111,"s",166,94,129,70,195,114
sentence_look_tinted_grid:
    DEFB "&E",161,149,"z",139,10,213,182,"!",0
sentence_intro15:
    DEFB 212,128,"es una pelicula",165,80,146,75,"\nla vida",145,153,"l. Solo hay trabajo,\ny algo de",136,212,130,3,153,51,135,127,131,177,186,211
sentence_land_ship2:
    DEFB 163,221,130,182,161,51,128,199,129,100,"Rayos-X\nDe",154,76,"1",132,135,146,31,"do",214,146,168,69,"\n",211,129,"j",161,51,137,70,149,73,164,214
sentence_look_galena_cristal:
    DEFB 129,42,240,203,241,179,129,60,"l",130,198,"ma*",133,242,184,43,"u*",129,0
talk_marvin_6:
    DEFB "Si. Dosc",164,46,138,167,"*os",215,138,"p",150,98,"e",168,208,136,75,"i",129,72,0
sentence_intro18:
    DEFB "Pon",160,95,"pies",192,152,"s",131,197,"o",181,80,".",133,85,164,94,"d",137,164,164,170,"tu",202,244
sentence_look_locker_green_with_chicken:
    DEFB "E",242,194,245,179,245,189,245,199,245,209,132,67,"y",128,75,"\n&",152,45,248,128,"m",129,87
sentence_end1:
    DEFB 152,29,"o %",130,144,152,142,"\na",201,179,"v",180,238
sentence_cover_8:
    DEFB "S",129,114,"e/",129,3,"er/0/M/Z",129,255,"enu",0
talk_green_19:
    DEFB 167,182,0
sentence_take_radar2:
    DEFB "(ade",194,72,154,77,129,158,"n",146,203,"do)",0
talk_green_7:
    DEFB 130,240,226,177,240,152,144,162
sentence_end2:
    DEFB "Yo,",154,79,198,71,136,193,147,207,133,155,"o",167,214,145,16,144,75
command_take:
    DEFB 166,189," ",0
talk_white_7:
    DEFB "B",136,226,"t",194,180," %D",149,165,"rg",131,47,"dijo?\n",163,35,"tip",165,242,128,199,"g",129,225
talk_red_11:
    DEFB "Ha",147,12,154,10,132,78,226,198,",\n",234,47,128,224,"l",129,0
talk_red_17:
    DEFB "D",131,190,"s",168,185,"te",194,59,"ofr",132,192,136,16,"Sirv",153,248,"cu",129,70,192,7,149,219
talk_yellow_5:
    DEFB "Adi",147,31
talk_red_16:
    DEFB 160,6,150,133,195,162
talk_red_15:
    DEFB "Ni s",164,91,135,128,129,218,149,190,154,224,144,133,218,99,135,71,".",0
sentence_end4:
    DEFB "Apa",129,100,"el",226,229,"r",139,231,"v",130,149,160,176,131,109,150,85
sentence_already_took_alcohol:
    DEFB 244,188,199,36
talk_white_food8:
    DEFB "A",163,174,134,93,"i",244,150,"l",144,23,160,117,"\n",136,75,160,103,",",162,56,"o",161,107,133,197,"v",149,5,"\n",144,119,151,33,130,96
sentence_already_found_galena:
    DEFB "%Par",165,158,"?",138,118,"\n",218,54,128,178,225,182
talk_red_8:
    DEFB "N",244,192,148,135,129,33,163,115,"\n",250,43,166,14,146,152,128,236
talk_yellow_6:
    DEFB "P",132,113,"f",134,144,134,97,"es\n",171,234,132,137,151,95,161,70,0
sentence_land_ship4:
    DEFB "&",201,104,"! ",144,185,"ab",129,109,"cu",129,70,"o\nt",150,221,167,36,128,231,161,107,135,96,128,208,"i",128,163
sentence_take_locker_green:
    DEFB 246,189,145,167,176,231,".\n",242,99,"e ",242,111,".",0
sentence_look_galena_diode:
    DEFB 129,42,"c",131,177,162,220,"\n",177,183,208,205,"i",145,187
name_box:
    DEFB "Caja",0
name_iodine:
    DEFB "B",241,49,"o",0
sentence_already_have_galena_cristal:
    DEFB 244,188,167,36,225,182
sentence_swap_chicken2:
    DEFB "Ni",148,95,132,137,197,75,134,112,164,53,139,204,"gi",128,1,147,181
sentence_use_iodine_in_person:
    DEFB 211,156,129,84,146,178,129,222,160,136
talk_blu_17:
    DEFB 176,185,134,224,167,128,",",146,56,"e",229,70,".",0
name_diary:
    DEFB "D",248,214,200,224,0
talk_marvin_8:
    DEFB 248,8," Y",130,198,"m",163,64,"d",248,9,"Y",162,59,"e",162,52,145,234,"u",155,13,184,9,129,73,"%",182,49,134,97,"h",162,42,"'d",216,9,"'?",0
talk_red_6:
    DEFB "S",226,48,"l",240,14,132,26,"\n",132,137,130,33,"an",144,219,"arg",176,23,130,1,138,1," Y\n",218,184,138,80,198,71,"ha",218,150,147,181
look_detector_1:
    DEFB "&D",203,17,241,179," Mikas",129,87
sentence_eat_diary2:
    DEFB "Na",167,134,154,57,"r",162,105,129,127,139,225,129,108,"\n",128,153,"mi",243,73,129,95,167,36," ",171,225,".",0
sentence_intro12:
    DEFB "M",250,193,129,58,232,210,144,75
talk_red_3:
    DEFB "%F",154,75,193,30,129,120,229,39
talk_green_15:
    DEFB 129,140,"o",144,74,152,209,133,77,"hh",135,241,"roo",144,74,"!\nVo",162,143,"v",135,121,"r",183,212,144,12,"noo",144,74,"\nm",135,121,183,181," ",167,190,129,99,199,190,144,75
talk_white_6:
    DEFB 161,163,197,101,130,68,155,233,130,178,"s\nr",167,63,147,230,130,95," Y",219,233,"\n",165,41,130,198,"m",163,64,135,29,"v",193,105,186,245
sentence_gave_alcohol_1:
    DEFB "A",199,7,0
talk_green_10:
    DEFB 154,43,"da",161,43,128,48,"g",128,236
sentence_rubber_chicken_alone:
    DEFB "M",147,38,129,108,202,73,131,14,179,110,130,33,130,19,128,106,128,231,"st",146,95
talk_white_generator3:
    DEFB "C",194,103,128,15,245,95,135,93,144,114,182,195,130,81,"g",147,31
talk_red_1:
    DEFB 195,25,"j.",0
sentence_land_ship8:
    DEFB 201,104,",",130,43,162,56,"o",136,213,"\nagu",153,137,"r",183,37
sentence_look_locker_main_no_diary:
    DEFB 168,207,226,198,165,190,228,210
talk_white_cristal2:
    DEFB "Ese",225,171,151,156,"irve",162,75,128,119,200,52,130,237,"N",244,192,"o gr",134,107,128,52
talk_white_3:
    DEFB "P",132,113,"c",128,11,132,49,184,189,153,185,151,78,"d",166,199,"lavabo,",182,195,149,247,"i",148,26,"n",160,180
name_marvin:
    DEFB 181,227,0
talk_yellow_10:
    DEFB 133,121,"Es",144,142,132,44,"do",241,150,"r",149,128
sentence_use_iodine_in_object:
    DEFB 240,6,128,253,151,79,"l",129,0
name_galena_detector:
    DEFB "D",203,17,241,179,0
sentence_take_alcohol:
    DEFB 182,189,244,199,128,179,0
sentence_dont_give:
    DEFB 214,66,".",0
talk_white_cristal3:
    DEFB "Es",147,49,145,66,196,248,163,225,128,44,129,116,".\n",130,240,146,52,216,74,"lo",199,1,137,174,134,203,134,196,151,52,"e. ",184,169,250,167,0
sentence_look_locker_main_with_diary:
    DEFB 168,207,226,198,203,212,147,226,"\n",248,210,248,220,129,0
sentence_no_one_near:
    DEFB "%",129,228,146,12,160,8,"n?\n",240,141,"ie ",162,172,".",0
talk_white_maps2:
    DEFB 184,94,130,30,163,230,"d",179,38,128,62,"i",150,111,"a*",148,232,"O",149,213,148,95,"v",148,137,"g",163,216
talk_yellow_9:
    DEFB "&%",152,182,195,213,"?!",0
sentence_intro3:
    DEFB "%R",152,9,"tar",226,177,"?\n%O ",145,176,129,210,167,111
talk_green_18:
    DEFB 241,191,129,0
sentence_give_no_maps:
    DEFB "%Un",216,212,148,212,"o?\n",160,185,197,101,130,68,165,111,"e",167,12
sentence_intro4:
    DEFB "&E",129,79,"i",185,253,139,62,133,235,"v",147,129,"\nn",160,186,135,48,232,73,166,195,"!",0
sentence_cant_take_people:
    DEFB 155,44,240,217," ",202,207,130,96
sentence_cant_do_that:
    DEFB "N",242,58,130,68,187,50
sentence_take_locker_blue:
    DEFB 128,6,135,29,132,137,151,95,"a",133,232
sentence_intro11:
    DEFB "Pa",163,171,176,227,"s",134,98,"p",134,175,". Tar",148,116,"\nt",150,222,"an",178,10,"l",136,11,"z",145,121,128,26,"kar",150,85
talk_white_21:
    DEFB 177,163,"c",149,67,197,101,130,68,".",0
sentence_look_red:
    DEFB 130,211,211,4,160,191,128,148,"i",128,52
talk_green_16:
    DEFB "&&",150,48,"diiii",183,181," j",135,121,146,52,146,55,"o",201,222,135,29,151,169,"i",138,59,"iiiesra",151,169,149,128
talk_marvin_3:
    DEFB 227,35,132,241
sentence_look_locker_blue:
    DEFB "E",242,194,194,204,128,79,245,190,165,200,242,250,182,94,163,8,128,75,"\n%n",130,2,149,211,128,12,"tic",153,143
sentence_need_usb:
    DEFB "N",210,58,131,55,145,67,198,139,243,106,"a",131,207,"u",163,180
sentence_nothing_to_use:
    DEFB 240,141,181,158,146,60,129,123,152,14,0
sentence_intro1:
    DEFB "B",128,103,"c",152,143,218,207,": ",129,249,"ieza\n",162,22,128,86,146,148,164,112,162,85,"\nm",169,50,176,152,209,111,".",0
sentence_look_box:
    DEFB 177,42,150,240,132,107,148,112,"f",146,15,"cad",184,230,"f",130,252,"a",160,129,"el",155,10,147,10,"s",147,198,"d",184,230,138,0,"ind",194,254,146,67,128,175,"fil",144,105,"\ni",138,56,"r",160,104,164,170,130,39,"cu",185,51
talk_blu_9:
    DEFB 234,160,177,183,"?",0
talk_green_4:
    DEFB "I",148,131,183,181," ",151,190,132,55,177,215,151,170,128,109,"re",199,190,129,17,180,87,135,132,247,178,135,132,134,176,"h",161,215,"o",183,168,".",0
sentence_green_iodine3:
    DEFB 168,182,167,143,135,150,160,14,"a",129,60,"n",146,23,"del",192,47,128,76
name_green:
    DEFB 147,150,0
talk_green_11:
    DEFB "E",199,191,"t",144,14,135,168,183,181,135,158,"ol",193,118,185,223,132,106,"n",150,111,"adul",231,178," ",149,54
sentence_look_grid:
    DEFB "M",170,193,129,120,131,208,129,58,130,3,178,0,"se",171,233,".",0
talk_red_14:
    DEFB "V",129,26,131,47,"v",160,180
sentence_intro14:
    DEFB 170,143,"on",249,135,139,62,"A",137,217,"afue",151,68,128,145,"gr",134,107,"es",231,50,"s",139,62,"Y",129,55,"\n",165,211,"iv",146,18,139,234,"un",152,212,"!",0
sentence_gave_alcohol_2:
    DEFB "M",177,92,"Si,",162,101,"\n",181,160,168,64,".",0
talk_white_13:
    DEFB 147,35,212,190,"ab",184,68,"\npod",128,225,133,197,"v",154,37,"?",0
talk_white_food5:
    DEFB "%",209,3,",\n",203,219,"t",128,236
sentence_intro5:
    DEFB 137,63,"jam",153,60,148,128,"y",167,127,"a",150,57
sentence_intro10:
    DEFB "%Y",197,101,132,24,184,186,135,74,"A",146,16,130,139,242,22,162,32,"?",200,112
sentence_give_iodine_to_white2:
    DEFB "%L",184,76,187,233,149,112,"YODO?",0
talk_white_food7:
    DEFB "Y",144,25,"s",128,208,"em",155,203,145,176,"es",160,122,225,118,132,50,131,177,146,52,212,190,130,245,"a\nu",244,104,244,114,"no",183,99,".",0
talk_white_4:
    DEFB "%Er",202,51,"c",162,32,128,41,"\n",194,53,"t",202,162,144,253,132,241
sentence_eat_diary:
    DEFB 249,168,168,185,"el",200,212,129,119,131,143,"i",180,169,128,45,137,239,136,150,"t",160,180
sentence_look_locker_yellow:
    DEFB "E",242,194,"ll",251,90,139,100," ",147,221,"\n",149,59,"d",162,219,"a",144,44,"bas",151,56,129,70,"i",133,59,".\n",209,90,"%E",138,9,187,182,"\n",128,159,"io",241,179,"?",0
talk_white_15:
    DEFB "Un",241,171,241,181
name_red:
    DEFB 130,211,0
sentence_nothing_to_talk_to:
    DEFB "L",128,84,"s",134,98,"loga",202,191,"re",128,133,129,105,128,105,181,1," d",181,104,128,23,129,177,129,238,128,52
sentence_look_white:
    DEFB "%Co",163,212,128,178,"v",151,67,208,45," ",213,145,"?",0
talk_white_generator2:
    DEFB 168,29," ide",128,138
sentence_take_locker_main1:
    DEFB 198,189,"el",200,212,".",0
sentence_transferred_data_to_diary:
    DEFB "H",144,131,136,78,"d",246,35,"s",144,152,"mi\n",184,213,149,190,146,39,210,52,150,167,129,71,"r",165,245,"SGAE.",0
sentence_cover_3:
    DEFB "2",139,212,154,75,"rs",165,245,132,137,"ro",0
talk_blu_10:
    DEFB 148,128,194,192,"e",129,65,"rm",150,133,197,80,"Y",167,155,128,192,171,183,243,225,"mi",148,108,".",0
talk_white_16:
    DEFB "A",199,7,0
talk_white_5:
    DEFB "%N",180,192,"a",171,233,"?",0
sentence_land_ship7:
    DEFB "FLUSHHHHh",167,194,128,76
talk_white_generator1:
    DEFB 248,61,151,67,180,106,"r",212,118,"?",0
talk_blu_6:
    DEFB 163,140,195,147,162,75,128,45,169,93,".",0
command_look:
    DEFB "Mir",145,229
sentence_cover_1:
    DEFB "E",128,198,"p",138,72,"rom M.O.N.J.A.S.",0
talk_blu_2:
    DEFB 171,214,"c",245,67,245,77,"T",241,26,177,36
sentence_intro6:
    DEFB "&",152,113,"! %Sigu",186,51,195,70,"up",128,92,"\n",184,213,"? &C",197,29,213,145,147,29,"!",0
talk_blu_18:
    DEFB 225,3,149,2,"me",203,222,"ar,",148,170,139,6,"vor. T",241,26,177,36
sentence_intro9:
    DEFB 217,243,",\n",235,95
sentence_rubber_chicken_pot2:
    DEFB "(C",179,6,160,191,128,148,"ie",179,48,146,110,134,238,153,137,144,25,"drama).",0
name_chicken:
    DEFB 168,127,0
sentence_intro19:
    DEFB "C",202,55,150,193,"ali",128,40,183,230,243,225,160,191,"se",128,138
sentence_take_locker_green1:
    DEFB 244,188,179,161,129,60,178,220,171,51
command_return:
    DEFB "Vol",146,3
name_usb_cable:
    DEFB "C",211,111,0
look_detector_4:
    DEFB 168,141,", ",161,129,"ti",197,108,"\n",202,207,128,58,130,230,149,219
talk_white_food4:
    DEFB 129,233," d",154,98,"el",197,95,"boc",150,18,"d",134,201,"mun",146,236,192,141,"fais",179,211," ",131,177,"\nm",149,162,"pa. &D",155,190,130,247,134,58
sentence_take_locker_red2:
    DEFB 198,189,"el",179,109,".",0
name_yellow:
    DEFB 202,184,0
talk_white_8:
    DEFB 144,55,216,44,128,41,162,220,". U",200,46,169,183,132,49,178,21,"de",195,194,160,74
talk_green_17:
    DEFB "Naaa",167,194,129,95,183,167,"tab",146,219,"o",145,57,128,153,"shu ",151,170,"iti",135,241,129,0
talk_white_10:
    DEFB 195,185,166,196,128,48,"ns",146,116,160,14,132,226,"y",195,194,"os",197,159,131,211,184,53,147,31
sentence_look_yellow:
    DEFB "S",182,221,202,191,"od",147,179,132,178,160,186,"p",181,99,".",0
name_reactor:
    DEFB "R",177,155,0
talk_white_6b:
    DEFB "M",148,91,"mo",196,88,186,163,131,200,176,133,160,118,153,105,"o",179,163
name_locker:
    DEFB "T",194,200,0
name_yellowchicken:
    DEFB 218,184,"Al-Ast",0
talk_marvin_12:
    DEFB "T",128,252,", us",150,23,235,15,"\n",241,180," Y",162,42,133,254,"l",244,189,".",0
sentence_look_diary:
    DEFB 248,207,248,217,152,227,194,214,148,212,130,237,168,170,163,106,"on",171,19,"\n",131,116,144,152,128,203,"l",147,129,144,211
sentence_look_rubber_chicken:
    DEFB 147,36,135,66,132,50,"t",152,171,146,177,"\n",128,232,"e",208,151,150,95,129,0
talk_green_1:
    DEFB 163,25,"v",163,151
sentence_look_plant:
    DEFB "Es",184,245,181,190,160,118,129,61,165,96,128,149,"\n",148,171,"la",146,26,"osi",163,230,"al",164,211,129,0
talk_ill_see_what_i_can_do:
    DEFB 152,94,145,94,130,3,"e",162,106,242,60,128,163
name_blue:
    DEFB 128,79,0
sentence_look_pot:
    DEFB "%C",144,192,194,50,128,26,"a",133,59,167,155,134,168,"vap",152,143,136,187,"e",192,112,176,153,148,212,128,236
talk_blu_12:
    DEFB 251,214,"i",183,54,128,42,"Y",129,57,133,85,128,252,225,44,134,102,135,56,129,50,"tu.\nY",241,23,225,33
talk_yellow_11:
    DEFB 183,209,0
look_detector_3:
    DEFB "Co",144,154,",",128,44,219,222,128,152,214,21,151,93,146,197,146,127,134,3,152,129,"y",136,212,130,3,"t",128,92,"\n",160,191,152,45,"aseo",164,170,145,169,"a",130,115,".",0
sentence_intro16:
    DEFB 240,6,130,67,"ptarl",130,237,"L",187,203,"\nt",152,171,197,159,"r",187,233,"m",196,67,128,1,138,59,131,207,144,225,"n",129,44,160,180
sentence_take_locker_red3:
    DEFB 155,44,240,217,176,147,167,38
sentence_rubber_chicken_pot:
    DEFB "&A",129,120,162,190,245,242,147,7,"!\n%Q",144,10,194,50,130,27,"l",129,49,164,239
sentence_radar_task_for_green:
    DEFB "Si",129,185,131,209,"r",165,245,"a",132,160,"m",195,47,"\n",147,74,128,15,219,222,152,69,211,148
sentence_land_ship3:
    DEFB 243,221,243,231,243,241,195,251,"Me",136,13,"je",129,153,"c",160,90,".\n",168,170,214,146,168,69,"\n",227,129,192,152,"hang",144,162
talk_red_4:
    DEFB "%Y",130,197,168,171,201,28,"? Si\na",131,110,"am",176,127,129,61,"ay",130,178,"r",144,75
talk_yellow_1:
    DEFB 147,25,".",0
talk_white_2:
    DEFB 217,104," Por\n",152,48,198,71,154,54,"i",145,200
talk_white_food6:
    DEFB 133,235,"c",154,8,178,50,241,168,145,178,"\nga",197,198,138,190,"o",129,158," s",170,193,"d",129,133,"d",155,229,212,190,147,144,"un",151,243,"ev",129,0
sentence_look_marvin:
    DEFB "Es",197,226,", nu",160,16,"\nv",131,208,"n",151,36,"rci",160,254
talk_marvin_7:
    DEFB "%Y",162,52,"v",132,67,"a",194,65,168,68,"ti",160,40,"a",152,142,"?",0
sentence_take_galena_cristal:
    DEFB 177,76,"m",166,50,"ro",196,197,"u*a!\nP",194,40,"l",193,23,".",0
name_grid:
    DEFB "Rej",146,203,0
name_pot:
    DEFB "O",130,204,0
name_plant:
    DEFB 168,246,",",146,195,"pl",153,137,0
name_galena_diode:
    DEFB "D",150,205,241,179,0
