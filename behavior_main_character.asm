; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.


; task1 manages the main character
task1:
    call task_yield
    ld ix, main_character
    bit 2, (ix+0)
    jp nz, task1
    ld a, (do_repaint_flags)
    and 0x18
    jp nz, task1 ; REPFLAG_4 REPGLAG_3
    ld a, (translated_key)
    and a
    call nz, manage_keyboard_main_character
    ld hl, do_repaint_flags
    res 5, (hl) ; REPFLAG_5
    jp task1


manage_keyboard_main_character:

    ld hl, (current_map_address)
    ld de, MAP_CHARACTER_OFFSET
    add hl, de
    ld (final_map_address), hl ; update the address of the tile where the main character is
    ld de, MAP_WIDTH
    ld ix, main_character
    cp KEY_MENU
    jp z, ask_command
    cp KEY_LEFT ; cursor left
    jr z, keyboard_left
    cp KEY_RIGHT ; cursor right
    jr z, keyboard_right
    cp KEY_UP ; cursor up
    jr z, keyboard_up
    cp KEY_DOWN ; cursor down
    ret nz

keyboard_down:
    add hl, de
    bit 7, (hl)
    ret nz
    ld (final_map_address), hl
    ld a, (ix+4)
    cp MAX_MAP_Y
    jr nc, task1_5
    add a, 4
    ld (ix+4), a
    ret
task1_5:
    ; arrived to the limit of the map
    call task_make_talk_and_wait
    DEFW main_character
    DEFW sentence_too_far
    ret

keyboard_right:
    ld a, (main_character)
    bit 5, a
    jp nz, main_character_look_left
    inc hl
    bit 7, (hl)
    jp nz, check_border_outside
    ld (final_map_address), hl
    ld a, 4
    add a, (ix+3)
    ld (ix+3), a
    ret

keyboard_left:
    ld a, (main_character)
    bit 5, a
    jp z, main_character_look_right
    dec hl
    bit 7, (hl)
    jp nz, check_border_outside
    ld (final_map_address), hl
    ld a, -4
    add a, (ix+3)
    ld (ix+3), a
    ret

keyboard_up:
    exx
    ld de, 0x401C
    call check_coords
    exx
    jr z, keyboard_up2
    and a ; C to zero
    sbc hl, de
    bit 7, (hl)
    ret nz
    ld (final_map_address), hl
    ld a, -4
    add a, (ix+4)
    ld (ix+4), a
    ret
keyboard_up2:
    call task_make_talk_and_wait
    DEFW main_character
    DEFW sentence_not_now
    ret

check_border_outside:
    ld a, (hl)
    and 0x7F
    cp TILE_INDEX_FLOOR_OUTSIDE
    ret nz
    ld a, (talking_time)
    and a
    ret nz
    call make_talk_main
    DEFW sentence_too_far
    ret
