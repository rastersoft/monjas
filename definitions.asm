; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

height EQU 23
coord_x EQU 20
coord_y EQU 10

; Put the sentence buffer in the printer buffer
sentence_buffer EQU 23296

STARTX EQU 100
STARTY EQU 26

MAX_MAP_Y EQU 200

MAX_SENTENCE_SIZE EQU 120

; Debugging defines

DODEBUG EQU 0
DOFPS   EQU 0
DEBUG_COORDS EQU 0
; print the flags inthe bottom of the screen
DEBUG_FLAGS EQU 0
; print bottom bar with remaining time during talk
DO_BOTTOM_BAR EQU 0
; use the alternative flags configuration to test specific parts
DO_DEBUG_FLAGS EQU 0
; show how many free bytes as much had each task
DEBUG_TASKS EQU 0
DO_DEBUG_STACK EQU 0

; use loop unroll for the attributes
DO_FAST_ATTRIBUTES EQU 1

; How many letters per frame are painted when a character talks
TALK_CHARS_PER_FRAME EQU 3


; MAP defines
EXTRA_Y EQU 2
MAP_WIDTH  EQU 32
MAP_OFFSET_X EQU 16
MAP_OFFSET_Y EQU 14
MAP_CHARACTER_OFFSET EQU (MAP_OFFSET_Y - 2) / 4 * MAP_WIDTH + MAP_OFFSET_X / 4
MAP_LAST_STATION_LINE EQU 22

; 9 is the number of patches painted per line (8 are really displayed, but the 9th is needed when there are offsets)
MAP_JUMP EQU MAP_WIDTH-9


; Table sizes
SPRITE_ENTRY_SIZE EQU 6
CHARACTER_ENTRY_SIZE EQU 9

; Alarm sound characteristics
SOUND_FREQ EQU 30
SOUND_DUR  EQU 22

; how many frames must wait until the spaceship alarm
; starts
TIMER_LAND_INIT EQU 180

; galena coordinates must be divided by two
GALENA_POS_X            EQU 8
GALENA_POS_Y            EQU 18

; character definition
CHARACTER_YELLOW        EQU 3
CHARACTER_RED           EQU 4
CHARACTER_BLUE          EQU 5
CHARACTER_GREEN         EQU 6
CHARACTER_WHITE         EQU 7
CHARACTER_MARVIN        EQU 8

; object definition
OBJECT_REACTOR          EQU 10
OBJECT_RADAR            EQU 11
OBJECT_GRID             EQU 12
OBJECT_BOX              EQU 13
OBJECT_POT              EQU 14
OBJECT_PLANT            EQU 15
OBJECT_LOCKER_RED       EQU 16
OBJECT_LOCKER_BLUE      EQU 17
OBJECT_LOCKER_MAIN      EQU 18
OBJECT_LOCKER_YELLOW    EQU 19
OBJECT_LOCKER_GREEN     EQU 20
OBJECT_DIARY            EQU 21
OBJECT_USB_CABLE        EQU 22
OBJECT_RUBBER_CHICKEN   EQU 23
OBJECT_GALENA_DIODE     EQU 24
OBJECT_CHICKEN          EQU 25
OBJECT_YELLOWCHICKEN    EQU 26
OBJECT_GALENA_DETECTOR  EQU 27
OBJECT_GALENA_CRISTAL   EQU 28
OBJECT_IODINE           EQU 29
OBJECT_BOOZE            EQU 30
OBJECT_GRID_IODINE      EQU 31


; command definition
VERB_TAKE               EQU 122
VERB_LOOK               EQU 123
VERB_TALK               EQU 124
VERB_USE                EQU 125
VERB_GIVE               EQU 126
VERB_RETURN             EQU 127

KEY_RIGHT               EQU 1
KEY_LEFT                EQU 2
KEY_DOWN                EQU 4
KEY_UP                  EQU 8
KEY_MENU                EQU 16

; Task entry:
;DEFB 37    ; total size of the entry in bytes (total stack size + 3)
;DEFW $+34  ; current SP address (stack size)
;DEFS 32    ; free space for task's stack (stack size-2)
;DEFW task1 ; task start address (entry point)

MACRO TASK_ENTRY, STACK_SIZE, TASK_FUNCTION
    DEFB STACK_SIZE+3
    DEFW $+STACK_SIZE
    DEFS STACK_SIZE-2
    DEFW TASK_FUNCTION
ENDM
