#!/usr/bin/env python3

# (C)2021 Raster Software Vigo (Sergio Costas Rodriguez)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import random
import signal
import copy
import types
import glob
import datetime

do_exit = False
do_signal = False
last_time = datetime.datetime.now()

def signal_handler(sig, frame):
    global do_signal
    global do_exit
    global last_time
    now = datetime.datetime.now()
    if (now - last_time) < datetime.timedelta(seconds=5):
        do_exit = True
    else:
        do_signal = True
        last_time = now

signal.signal(signal.SIGINT, signal_handler)

bits_for_size = 3

with open(sys.argv[1], "r") as input_file:
    lines = input_file.readlines()

class ReadSentences(object):
    def _add_sentence(self, labels, line):

        while -1 != line.find("\\n"):
            line = line.replace("\\n","\n")
        while -1 != line.find("\\c"):
            line = line.replace("\\c", chr(127)) # copyright symbol
        line2 = ""
        for ch in line:
            if ch in self._replacements:
                line2 += self._replacements[ch]
            else:
                if ord(ch) > 127:
                    print(f"Invalid character in {line}")
                    sys.exit(-1)
                line2 += ch
        entry = types.SimpleNamespace()
        entry.label = labels
        entry.sentence = line2.encode('latin1')
        entry.idx = self._idx_counter
        self._idx_counter += 1
        self.sentences.append( entry )

    def __init__(self):
        self._replacements = {'¡': '&', '¿': '%', 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u', 'ü': 'u', 'ñ': '*'}

    def check_labels(self, exclude):
        """ Checks which labels in the origin file aren't used in the sources """
        labels = []
        for sentence in self.sentences:
            for l in sentence.label:
                labels.append([l, False])
        for fname in glob.glob("*.asm"):
            if fname in exclude:
                continue
            with open(fname, "r") as f:
                content = f.read()
            for label in labels:
                if -1 != content.find(f"DEFW {label[0]}\n"):
                    label[1] = True
                    continue
                if -1 != content.find(f"ld hl, {label[0]}\n"):
                    label[1] = True
                    continue
        p = False
        for l in labels:
            if l[1]:
                continue
            if not p:
                print("Labels not used:")
                p = True
            print(l[0])

    def read(self, lines):
        self.sentences = []
        self._idx_counter = 0
        label = []
        string = ""
        line_number = 0
        for line in lines:
            line_number += 1
            line = line.strip()
            if line == "":
                continue
            if line[0] == ';':
                continue
            if line.endswith(":"): # label
                if string != "":
                    if string[-1] != "\x00":
                        print(f"Line incorrectly ended at {line_number-1}")
                        sys.exit(-1)
                    if string[:-1].find("\x00") != -1:
                        print(f"Invalid end of line at {line_number-1}")
                        sys.exit(-1)
                    self._add_sentence(label, string)
                    string = ""
                    label = [line[:-1]]
                else:
                    label.append(line[:-1])
                continue
            if line.startswith("DEFB"):
                line = line[4:].strip()
                if (line != "0") and (not line.endswith('",0')) and (not line.endswith('\\n"')):
                    print(f"Line incorrectly ended at {line_number}")
                    sys.exit(-1)
                #if (line.endswith('",0')) and (not line.endswith('.",0')) and (not line.endswith('?",0')) and (not line.endswith('!",0')):
                #    print(f"Line not ended in dot, exclamation or question mark at {line_number}")
            if line[0]=='"':
                line = line[1:]
            if line.endswith('",0'):
                line = line[:-3]
                line += "\000" # cambiar a 0
            else:
                line = line[:-1]
            string += line
        if (label is not None) and (string != ""):
            if string[-1] != "\x00":
                print(f"Line incorrectly ended at {line_number-1}")
                sys.exit(-1)
            if string[:-1].find("\x00") != -1:
                print(f"Invalid end of line at {line_number-1}")
                sys.exit(-1)
            self._add_sentence(label, string)
        return self.sentences


class compressor(object):
    def __init__(self, bfs):
        self._min_len = 3
        self._max_len = self._min_len + 2**bfs - 1
        self._max_offset = 2**(15-bfs)
        print(f"Minimum length: {self._min_len}; Maximum length: {self._max_len}; Maximum offset: {self._max_offset}")
        self.c_size = None
        self.u_size = None

    def compress(self, lsentences):
        self._uncompressed_data = b""
        self.sentences = []
        idx = 0
        for entry in lsentences:
            e = types.SimpleNamespace()
            e.idx = idx
            e.label = entry.label
            e.sentence = entry.sentence[:]
            self._uncompressed_data += e.sentence
            self.sentences.append(e)
            idx += 1
        # 0: unprocessed character
        # X: the character has been compressed in a block of size X
        # -1: the character is used by a compressed block
        self._compressed_blocks = [0] * len(self._uncompressed_data)
        self.sizes = {}
        for a in range(self._max_len, self._min_len - 1, -1):
            self.sizes[a] = 0
        self.u_size = len(self._uncompressed_data)

        # prepare the compression for each size
        for length in range(self._max_len, self._min_len-1, -1):
            offset = 0
            zero_block = [0] * length
            while offset < (len(self._uncompressed_data) - length):
                block = self._uncompressed_data[offset:offset+length]
                if 0 in block[:-1]:
                    offset += 1
                    continue # don't join several sentences
                # if any the characters being scanned are compressed, or used in a compression, don't compress the block
                if self._compressed_blocks[offset:offset+length] != zero_block:
                    offset += 1
                    continue
                pos = self._uncompressed_data[:offset].find(block)
                if pos == -1:
                    offset += 1
                    continue
                if pos >= self._max_offset:
                    offset += 1
                    continue
                is_compressed = False
                for v in self._compressed_blocks[pos:pos+length]:
                    if v > 0:
                        is_compressed = True
                        break
                if is_compressed:
                    offset += 1
                    continue
                for v in range(length):
                    self._compressed_blocks[pos+v] = -1
                    self._compressed_blocks[offset+v] = length
                offset += length
        # now, do the compression
        self._compressed_data = b""
        self._repetitions = {}
        offset = 0
        while offset < len(self._uncompressed_data):
            v = self._compressed_blocks[offset]
            if v <= 0:
                self._compressed_data += bytes([self._uncompressed_data[offset]])
                offset += 1
                continue
            block = self._uncompressed_data[offset:offset+v]
            pos = self._compressed_data.find(block)
            self._compressed_data += self._codify(pos, v)
            offset += v
            self.sizes[v] += 1

        self.c_size = len(self._compressed_data)
        sorted(self._repetitions, key = lambda s: self._repetitions[s])
        s = []
        for k in self._repetitions:
            if self._repetitions[k] < 3:
                continue
            s.append([ k,self._repetitions[k] ])
        self._repetitions = s

        idx = 0
        offset = 0
        sentence = b""
        ended = False
        while offset < len(self._compressed_data):
            v = self._compressed_data[offset]
            sentence += bytes([v])
            offset += 1
            if (v>0) and (v<128):
                continue
            elif v == 0:
                ended = True
            else: # v >= 128
                w = self._compressed_data[offset]
                sentence += bytes([w])
                offset += 1
                o, l = self._decode(v, w)
                if 0 in self._compressed_data[o:o+l]:
                    ended = True
            if ended:
                self.sentences[idx].sentence = sentence
                sentence = b""
                idx += 1
                ended = False
        if (not self.check_data(lsentences)):
            print("Error in check")
            sys.exit(-1)
        return self.sentences

    def check_data(self, o_sentences):

        for idx in range(len(o_sentences)):
            entry = self.sentences[idx]
            a = 0
            expanded_data = b""
            while a < len(entry.sentence):
                v1 = entry.sentence[a]
                a += 1
                if v1 < 128:
                    expanded_data += bytes([v1])
                    continue
                v2 = entry.sentence[a]
                a += 1
                o, l = self._decode(v1, v2)
                expanded_data += self._compressed_data[o:o+l]
            if expanded_data != o_sentences[idx].sentence:
                print(idx)
                print(expanded_data)
                print(o_sentences[idx].sentence)
                return False
        return True



    def _decode(self, b1, b2):
        v = (b1 * 256 + b2) - 32768
        length = int(v / self._max_offset) + self._min_len
        offset = int(v % self._max_offset)
        return offset, length


    def _codify(self, pos, length):
        o = 32768 + pos + ((length - self._min_len) * self._max_offset)
        if o not in self._repetitions:
            self._repetitions[o] = 0
        self._repetitions[o] += 1
        b1 = int(o / 256)
        b2 = int(o % 256)
        return bytes([b1, b2])


    def store_sentences(self, filename, sentences=None):
        if sentences is None:
            sentences = self.sentences
        with open(filename, "wb") as output_file:
            for entry in sentences:
                self._write_sentence(output_file, entry.label, entry.sentence)

    def _write_sentence(self, output_file, label, line):
        for l in label:
            output_file.write((l+":\n").encode('latin1'))
        put_defb = False
        quotes = False
        first = True
        next_as_is = False
        if line == b"\000":
            output_file.write(b'    DEFB "",0')
        else:
            for ch in line:
                if not put_defb:
                    output_file.write(b"    DEFB ")
                    put_defb = True
                if (not next_as_is) and ((ch >=32) or (ch == 10)) and (ch <=127):
                    if ch == 127:
                        if quotes:
                            output_file.write(b'"')
                            quotes = False
                        if not first:
                            output_file.write(b',')
                        first = False
                        output_file.write(b"127")
                    else:
                        if not quotes:
                            if not first:
                                output_file.write(b',')
                            first = False
                            output_file.write(b'"')
                            quotes = True
                        if ch == 10:
                            output_file.write(b"\\n")
                        else:
                            output_file.write(bytes([ch]))
                else:
                    if quotes:
                        output_file.write(b'"')
                        quotes = False
                    if not first:
                        output_file.write(b',')
                    first = False
                    output_file.write(f"{ch}".encode('latin1'))
                    if (ch > 127) and (not next_as_is):
                        next_as_is = True
                    else:
                        next_as_is = False
        output_file.write(b"\n")

def print_data(osize, lsize, csizes, bsizes, bratios, repetitions):
    print(f"    {osize} -> {lsize} ({100 * lsize / osize:.2f}%)", end="")
    print(f" ; best size: {bsizes} ({100 * bsizes / original_size:.2f}%)  ",end="")
    for l in bratios:
        print(f" {l}->{bratios[l]}", end="")
    freed = 0
    for k in repetitions:
        freed += k[1] - 2
    print(f"  Freed: {freed}  ({len(repetitions)})   ", end="\r")

def swap_element(array, origin, destination):
    element = array[origin]
    return array[:destination] + [element] + array[destination:origin] + array[origin+1:]


s =ReadSentences()
sentences = s.read(lines)
s.check_labels([sys.argv[1], sys.argv[2]])
comp = compressor(bits_for_size)
comp.compress(sentences)
comp.store_sentences(sys.argv[2])
original_size = comp.u_size
last_size = comp.c_size

def get_max_idx(sentences, bfs):
    idx = 0
    length = 0
    max_len = 2**(15-bfs)
    for entry in sentences:
        length += len(entry.sentence)
        if length>=max_len:
            return idx
        idx += 1
    return idx

if True:
    temperature = 1.0
    best_size = last_size
    best_sentences = sentences[:]
    best_ratios = comp.sizes.copy()
    do_full_swap = True
    max_idx = get_max_idx(sentences, bits_for_size)
    while not do_exit:
        if do_signal:
            do_signal = False
            sentences = s.read(lines)
            comp.compress(sentences)
            last_size = comp.c_size
        print_data(original_size, last_size, comp.sizes, best_size, best_ratios, comp._repetitions)
        old_sentences = sentences[:]
        temperature *= 0.9
        i = random.randint(0, len(sentences)-1)
        if do_full_swap:
            print("*", end="")
            sentences = swap_element(sentences, i, 0)
        else:
            print(" ", end="")
            j = random.randint(0, len(sentences)-1)
            if i <= j:
                continue
            if j>max_idx:
                continue
            sentences = swap_element(sentences,i,j)
        do_full_swap = not do_full_swap
        comp.compress(sentences)
        new_size = comp.c_size
        if new_size < best_size:
            best_size = new_size
            best_sentences = sentences[:]
            best_ratios = comp.sizes.copy()
            comp.store_sentences(sys.argv[2])
        if new_size >= last_size:
            if random.random() >= max(temperature / 2, 0.001):
                sentences = old_sentences
            else:
                last_size = new_size
                max_idx = get_max_idx(sentences, bits_for_size)
        else:
            last_size = new_size
            max_idx = get_max_idx(sentences, bits_for_size)
    print("\n")

max_len = 0
max_sentence = None
second_max = 0
second_max_sentence = None
for entry in sentences:
    if len(entry.sentence) > max_len:
        second_max = max_len
        second_max_sentence = max_sentence
        max_len = len(entry.sentence)
        max_sentence = entry.sentence


print(original_size)
print(last_size)
print(f"Original size: {original_size}; compressed size: {best_size} ({100 * best_size / original_size:.2f}%)")
print(f"Maximum length: {max_len} in sentence {max_sentence}")
print(f"Second maximum length: {second_max} in sentence {second_max_sentence}")
