// (C)2021 Raster Software Vigo (Sergio Costas Rodriguez)

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

unsigned char buffer[49152];

void set_value(int index, unsigned int value) {
  buffer[index] = value % 256;
  buffer[index + 1] = value / 256;
}

int main(int argc,char **argv) {

  int f_in;
  int f_out;
  int address;
  int run;

  if (argc != 5) {
    printf("Usage: bin2z80 bin_file output_file dump_address run_address\n");
    return -1;
  }

  address = atoi(argv[3]);
  if (address == 0) {
    printf("Invalid address.\n");
    return -1;
  }

  run = atoi(argv[4]);
  if (run == 0) {
    printf("Invalid run address.\n");
    return -1;
  }

  f_in = open(argv[1],O_RDONLY);
  if (-1 == f_in) {
    printf("Can't open file %s. Aborting.\n",argv[1]);
    return -1;
  }

  f_out = open(argv[2],O_WRONLY|O_CREAT|O_TRUNC,S_IRUSR|S_IWUSR);
  if (-1 == f_out) {
    printf("Can't open file %s. Aborting.\n",argv[2]);
    return -1;
  }

  memset(buffer,0,49152);
  unsigned char b2[3];

  set_value(6, run);
  set_value(8, address - 1);
  buffer[10]=0x3F; // interrupt register
  buffer[12] = 0x0E;
  buffer[29]=0x01; // IM 1
  write(f_out,buffer, 30);
  memset(buffer, 0, 49152);
  memset(buffer + 6144, 56, 768);
  read(f_in, buffer + address - 16384, 65536-address);
  write(f_out,buffer, 49152);
  close(f_in);
  close(f_out);
  return 0;
}
