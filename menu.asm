; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

version_text:
    DEFB "v1.6",0

main_menu:
    ; check if it has true floating bus or not
    ld bc, 16000
main_menu_loop0:
    in a, (255)
    inc a
    jr nz, has_floating_bus
    dec bc
    ld a, b
    or c
    jr nz, main_menu_loop0
    ; if we arrive here, that means that we are in a +2A/+3
    jr main_menu2
has_floating_bus:
    ; if we arrive here, we are in a machine with true floating bus
    ld hl, 0xFFFF
    ld (floating_bus_value+1), hl
main_menu2:
    ld a, (border_sound_byte)
    and 0x18
    out (254), a
    ld (border_sound_byte), a
    ld a, 7
    call set_last_line_color
    ld hl, 0x8000
    ld de, 0x8001
    ld bc, 6911
    xor a
    ld (hl), a
    ldir
    ld hl, version_text
    ld a, 0x06
    ld d, 0x00
    call do_print_centered
    ld hl, sentence_cover_1
    ld a, 0x06
    ld d, 0x02
    call do_print_centered
    ld hl, sentence_cover_7
    ld a, 0x06
    ld d, 0x06
    call do_print_centered
    ld hl, sentence_cover_8
    ld a, 0x06
    ld d, 0x08
    call do_print_centered

    ld hl, sentence_cover_2
    ld a, 0x06
    ld d, 0x0C
    call do_print_centered
    ld hl, sentence_cover_3
    ld a, 0x06
    ld d, 0x0E
    call do_print_centered

    ld hl, sentence_cover_5
    ld a, 0x06
    ld d, 0x15
    call do_print_centered
    ld hl, sentence_cover_6
    ld a, 0x06
    ld d, 0x16
    call do_print_centered

    xor a
    ld (current_key), a
    ld hl, do_repaint_flags
    ei
main_menu_loop1:
    set 0, (hl) ; REPFLAG_0
    halt
    bit 0, (hl) ; REPFLAG_0
    jr nz, main_menu_loop1
    ld a, (current_key)
    and a
    jr z, main_menu_loop1
    cp 0x61 ; 1
    jp z, main_intro
    cp 0x62 ; 2
    jp z, main_loop_init
    jr main_menu_loop1
