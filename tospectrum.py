#!/usr/bin/env python3

import sys

with open(sys.argv[1],"rb") as data_f:
    data = data_f.read()

output = ""
pos = 0

for line in range(192):
    output += "  DEFB "
    for col in range(32):
        c = 0
        for pixel in range(8):
            c *= 2
            if data[pos] == 0:
                c += 1
            pos += 1
        if col != 0:
            output += ', '
        output += f"{c}"
    output += "\n"
for line in range(24):
    output += "  DEFB "
    for col in range(32):
        if col != 0:
            output += ', '
        output += "48"
    output += "\n"


with open(sys.argv[2], "w") as data_out:
    data_out.write(output)
