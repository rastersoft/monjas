; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.


; Sets the destination for a character and yields until it has arrived.
; The character and the destination coordinates are passed in two DEFW
; following the CALL statement.

task_move_to_and_wait_for_walk:
    pop hl
    ld a, (hl)
    ld ixl, a
    inc hl
    ld a, (hl)
    ld ixh, a
    inc hl
    ld a, (hl) ; Y coordinate
    or 0x02 ; all the Y coordinates have the bit 1 set
    and 0xFE ; and the bit 0 reset
    ld (ix+4), a
    inc hl
    ld a, (hl)
    and 0xFC ; all X coordinates have bits 0 and 1 reset
    ld (ix+3), a ; X coordinate
    inc hl
    push hl ; return address
    ; jp task_wait_for_walk

; After setting new coordinates for the caracter pointed by IX, call this
; function to yield and wait until the character has arrived to the coordinates

task_wait_for_walk:
    push ix
    call task_yield
    pop ix
    bit 2, (ix+0)
    jp nz, task_wait_for_walk
    ret

task_make_talk_and_wait:
    pop hl
    ld a, (hl)
    ld ixl, a
    inc hl
    ld a, (hl)
    ld ixh, a
    inc hl
    ld e, (hl)
    inc hl
    ld d, (hl)
    inc hl
    push hl
    ex de, hl
    call make_talk_hl

task_wait_while_talking:
    ld a, (talking_time)
    and a
    ret z
    call task_yield
    jr task_wait_while_talking

task_check_if_in_conversation:
    ld hl, do_repaint_flags
    bit 3, (hl) ; REPFLAG_3
    ret z
    call task_yield
    jr task_check_if_in_conversation

; waits as many frames as the value in B
task_wait_B_frames:
    push bc
    call task_yield
    pop bc
    djnz task_wait_B_frames
    ret

do_print_centered:
    ld ixh, a
    push de
    call uncompress_sentence_in_buffer
    pop de
    ld c, 255
    ld hl, sentence_buffer
do_print_centered2:
    ld a, (hl)
    inc c
    inc hl
    and a
    jr nz, do_print_centered2
    ld a, 32
    sub c
    srl a
    ld ixl, a
    ld e, a
    ld hl, sentence_buffer
    ld a, ixh

do_print_sentence_unlimited:
    ld b, 255
; do_print_sentence prints a sentence at DE coordinates (YX), with color
; attributes in A, pointed by HL, and NUL terminated. B will contain the
; maximum number of characters to print
;
; at exit, DE will contain the next coordinate, which can be used to append
; a text to the last sentence
do_print_sentence:
    ld ixl, e
    ld ixh, a
do_talk_loop:
    ld a, (hl)
    and a
    ret z
    jp m, do_uncompress ; if sign is negative, is because bit 7 is set
    cp 10
    jr nz, do_talk_paint
    inc d
    ld e, ixl ; next line
    jr do_talk_continue
do_talk_paint:
    push bc
    push de
    push hl
    call paint_letter
    pop hl
    pop de
    pop bc
    inc e
do_talk_continue:
    inc hl
    djnz do_talk_loop
     ; since neither INC HL nor DJNZ modify Z flag, and INC E
     ; will always be non-zero because we never print up to coordinate 255,
     ; it is possible to use the Z flag to know whether it returns because
     ; it found the END OF SENTENCE character, or because it printed the
     ; maximum number of characters allowed.
    ret
do_uncompress:
    push bc
    ld b, a
    inc hl
    ld c, (hl)
    inc hl
    push hl ; preserve the rest of the sentence
    ld l, c
    and 0x0F
    ld h, a ; hl has the offsett from the start
    push de
    ld de, sentence_list ; start of the list of sentences
    add hl, de ; offset
    pop de
    ld a, b
    rrca
    rrca
    rrca
    rrca
    and 0x07
    add a, 3
    ld b, a ; length
do_uncompress2:
    call do_talk_loop ; print the piece
    pop hl
    pop bc
    and a
    ret z
    jp do_talk_loop


uncompress_sentence_in_buffer:
    ld de, sentence_buffer
uncompress_sentence_in_buffer2:
    ld b, 255
uncompress_sentence_in_buffer1:
    ld a, (hl)
    ld (de), a
    and a
    ret z
    bit 7, a
    jr nz, do_uncompress_in_buffer
    inc hl
    inc de
    dec b
    ret z
    jr uncompress_sentence_in_buffer1
do_uncompress_in_buffer:
    ld b, a
    inc hl
    ld c, (hl)
    inc hl
    push hl
    ld l, c
    and 0x0F
    ld h, a ; hl has the offsett from the start
    push de
    ld de, sentence_list ; start of the list of sentences
    add hl, de ; offset
    pop de
    ld a, b
    rrca
    rrca
    rrca
    rrca
    and 0x07
    add a, 3
    ld b, a ; length
    call uncompress_sentence_in_buffer1
    pop hl
    and a
    ret z
    jp uncompress_sentence_in_buffer2

; paint letter paints a letter at YX coordinates passed in DE,
; the letter ASCII code passed in A, and the attributes at IXh
paint_letter:
    ld b, a
    ld a, d
    cp 24
    ret nc
    ld a, e
    cp 32
    ret nc
    set 7, d ; now DE points to the first scanline of the text
    ld l, b
    ld h, 0
    add hl, hl
    add hl, hl
    add hl, hl
    ld bc, font-256
    add hl, bc ; starting address of the letter to paint
    ld a, e
    and 0x1F
    ld c, a
    ld a, d
    rrca
    rrca
    rrca
    ld b, a
    and 0xE0
    or c
    ld c, a
    ld a, b
    and 3
    or 0x98
    ld b, a
    ld a, ixh
    ld (bc), a ; set attribute colors
    ld b, 8
    ld c, 32
paint_letter1:
    ld a, (hl)
    ld (de), a
    ld a, e
    add a, c
    ld e, a
    inc hl
    djnz paint_letter1
    ret



; random number generator
; It uses a 16-bit linear feedback shift register
; applying the prime number 0x1B23 in each iteration
; and returning the lower 8 bits in A
random_number:
    ld hl, (seed)
    ld a, 0x1B
    xor h
    ld h, a
    ld a, 0x23
    xor l
    ld l, a
    and a ; C to zero
    rr h
    rr l
    jr nc, random_number2
    set 7, h
random_number2:
    ld (seed), hl
    ld a, l
    ret


; Creates an empty frame, ready to contain elements like sentences for
; conversations or the inventory.
; HL contains the height and the width respectively, and IXh the color attributes
; It uses the alternate registers

frame_paint:
    ld a, 24
    sub h
    rra ; get Y coordinate
    ld d, a
    ld a, 32
    sub l
    rra ; get X coordinate
    ld e, a
    dec h
    dec l

    push de
    push hl
    ld a, 0x80 ; top-left corner
    call paint_letter
    pop hl
    pop de
    ld c, e

    ld b, l
    push hl
frame1:
    inc e
    push de
    push bc
    ld a, 0x81 ; top corner
    call paint_letter
    pop bc
    pop de
    djnz frame1
    push de
    push bc
    ld a, 0x82 ; top-right corner
    call paint_letter
    pop bc
    pop de
    pop hl

frame2:
    ld e, c
    inc d
    push bc
    push de
    push hl
    ld a, 0x83 ; left corner
    call paint_letter
    pop hl
    pop de
    pop bc
    ld b, l
    push hl
frame3:
    inc e
    push bc
    push de
    ld a, 0x20 ; blank
    call paint_letter
    pop de
    pop bc
    djnz frame3
    push bc
    push de
    ld a, 0x84 ; right corner
    call paint_letter
    pop de
    pop bc
    pop hl
    dec h
    jr nz, frame2

    ld e, c
    push bc
    push de
    push hl
    ld a, 0x85 ; bottom-left corner
    call paint_letter
    pop hl
    pop de
    pop bc

    ld b, l
frame4:
    inc e
    push bc
    push de
    ld a, 0x86 ; bottom corner
    call paint_letter
    pop de
    pop bc
    djnz frame4
    ld a, 0x87 ; bottom-right corner
    call paint_letter
    ret


; reads a key with anti-bouncing
; returns it in A
read_key:
    halt
    call translate_key
    ld a, (translated_key)
    and a
    ret z
    ld (last_key), a
read_key2:
    xor a
    ld (current_key), a ; set to zero
    halt
    call translate_key
    ld a, (translated_key)
    and a
    jr nz, read_key2
    ld a, (last_key)
    ret

clear_last_line:
    ld hl, 0x9700
    ld de, 0x9701
    ld bc, 255
    xor a
    ld (hl), a
    ldir
    ret

set_last_line_color:
    ld hl, 0x9AE0
    ld de, 0x9AE1
    ld bc, 31
    ld (hl), a
    ldir
    ld hl, 0x9AE0
    ld de, 0x5AE0
    ld bc, 32
    ldir
    ret

dump_last_line:
    ; prints the last line into the screen
    ld hl, 0x9700
    ld de, 0x50E0
    ld bc, 256
    ld ixh, e
    ld a, d
dump_last_line2:
    inc a
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ld e, ixh
    ld d, a
    jp pe, dump_last_line2
    ld hl, 0x9AE0
    ld de, 0x5AE0
    ld bc, 32
    ldir
    ret

main_character_look_left:
    ld a, (main_character)
    bit 5, a
    ret z
    ld ix, main_character
    ld b, (ix+2)
    ld c, (ix+1)
    and a
    jp set_character_direction_and_position_ix_bc

main_character_look_right:
    ld a, (main_character)
    bit 5, a
    ret nz
    ld ix, main_character
    ld b, (ix+2)
    ld c, (ix+1)
    scf
    jp set_character_direction_and_position_ix_bc

; Changes the location and direction of a character.
; The character to update and the coordinates
; are set, in that order, in two DEFW after the call.
; it will look to the left if C=1, or to the right if C=0

set_character_direction_and_position:
    pop hl
    ld a, (hl)
    ld ixl, a
    inc hl
    ld a, (hl)
    ld ixh, a
    inc hl
    ld b, (hl)
    inc hl
    ld c, (hl)
    inc hl
    push hl
; Changes the location and direction of a character.
; The character to update must be passed in IX,
; and the coordinates in BC
; it will look to the left if C=1, or to the right if C=0
set_character_direction_and_position_ix_bc:
    push af
    ld a, (ix+5)
    ld iyl, a
    ld a, (ix+6)
    ld iyh, a ; IY points to the upper sprite
    bit 0, (ix+0)
    jr z, set_character_direction_and_position2
    push bc ; update the map location if it is the main character
    ld a, c
    sub MAP_OFFSET_X
    and 0xFC
    ld (current_map_x), a
    ld a, b
    set 1, a
    sub MAP_OFFSET_Y
    and 0xFC
    ld (current_map_y), a
    pop bc
set_character_direction_and_position2:
    ld a, b
    and 0xFC
    set 1, a
    ld b, a
    ld a, c
    and 0xFC
    ld c, a
    ld d, c
    pop af
    ld a, c
    jr c, set_character_direction_left
    ld hl, sprite_base_right
    res 5, (ix+0)
    jr set_character_direction1
set_character_direction_left:
    dec c
    set 5, (ix+0)
    ld hl, sprite_base_left
set_character_direction1:
    ld (iy+4), l
    ld (iy+5), h
    ld (iy+1), c
    ld (ix+1), a
    ld (ix+3), a
    ld (iy+2), b
    ld (ix+2), b
    ld (ix+4), b
    ld a, 5
    bit 0, (ix+0)
    jr nz, set_character_direction3
    inc a
set_character_direction3:
    ld (iy+0), a
    ld a, (ix+7)
    ld iyl, a
    ld a, (ix+8)
    ld iyh, a ; IY point to the lower sprite
    ld (iy+1), c
    ld (iy+2), b ; coordinates
    xor a
    bit 0, (ix+0)
    jr nz, set_character_direction4
    inc a
set_character_direction4:
    ld (iy+0), a
    ld a, (iy+0)
    and 0x3F
    ld (iy+0), a
    ld de, feet_table
    bit 5, (ix+0) ; left or right
    jr z, set_character_direction2
    inc e
    inc e ; it is guaranteed that the feet table is inside a 256-byte page
set_character_direction2:
    ld a, (de)
    ld (iy+4), a
    inc de
    ld a, (de)
    ld (iy+5), a
    ret

; waits until the screen secondary buffer has been copied into the main screen.
repaint_and_wait_until_screen_repainted:
    push hl
    ei
    ld hl, do_repaint_flags
    set 0, (hl) ; REPFLAG_0
repaint_and_wait_until_screen_repainted1:
    bit 0, (hl) ; wait for the buffer to be copied into the screen
    jr nz, repaint_and_wait_until_screen_repainted1
    pop hl
    di
    ret

; Moves a character to a position
; The character to move and the coordinates
; are set, in that order, in two DEFW after the call.
move_character_to:
    pop hl
    ld a, (hl)
    ld ixl, a
    inc hl
    ld a, (hl)
    ld ixh, a
    inc hl
    ld a, (hl)
    and 0xFC
    set 1, a
    ld (ix+4), a
    inc hl
    ld a, (hl)
    and 0xFC
    ld (ix+3), a
    inc hl
    jp (hl)


make_talk_and_wait:
; Makes a character to talk. The character that talks and the setence to say
; are set, in that order, in two DEFW after the call. It also waits until the
; character ends talking and moving. If bit 1 in do_repaint_flags is set,
; will also call paint_map.
    pop hl
    ld a, (hl)
    ld ixl, a
    inc hl
    ld a, (hl)
    ld ixh, a
    inc hl
    ld e, (hl)
    inc hl
    ld d, (hl)
    inc hl
    push hl
    ex de, hl
    call make_talk_hl
make_talk_and_wait1:
    push ix
    ld a, (do_repaint_flags)
    bit 1, a ; REPFLAG_1
    jr z, make_talk_and_wait2
    call paint_map
    call do_talk
    call update_characters
    jr make_talk_and_wait3
make_talk_and_wait2:
    call do_talk
    ei
    halt
    halt
    halt
    halt
    di
make_talk_and_wait3:
    call repaint_and_wait_until_screen_repainted
    pop ix
    ld a, (talking_time)
    and a
    jr nz, make_talk_and_wait1
    bit 2, (ix+0)
    jr nz, make_talk_and_wait1
    ret


move_and_wait:
; Moves a character to a position and wait until it arrives there.
; The character to move and the coordinates
; are set, in that order, in two DEFW after the call.
    pop hl
    ld a, (hl)
    ld ixl, a
    inc hl
    ld a, (hl)
    ld ixh, a
    inc hl
    ld a, (hl)
    and 0xFC
    ld (ix+3), a
    inc hl
    ld a, (hl)
    and 0xFC
    set 1, a
    ld (ix+4), a
    inc hl
    push hl
move_and_wait1:
    push ix
    call paint_map
    call do_talk
    call update_characters
    call repaint_and_wait_until_screen_repainted
    pop ix
    bit 2, (ix+0)
    jr nz, move_and_wait1
    ret


; Enables or disables a sentence in a conversation based on flags
; It receives the flags in the A register, and the following bytes
; as DEFx after the CALL:
;     AND value for the mask,
;     CP value,
;     VALUE_IF_EQUAL
;     VALUE_IF_NOT_EQUAL
;     address of the sentence ID (two bytes),
; If a value is 0xFF, it will mean "do not modify".
; The A register is preserved

set_sentence_id:
    pop hl
    ld b, a ; preserve A
    and (hl) ; apply mask to the flags
    inc hl
    cp (hl)  ; compare masked flags with value
    inc hl   ; it doesn't modify the F register!!! Yahoo!!!!
    ld a, (hl) ; value if EQUAL
    inc hl
    jr z, set_sentence_id1
    ld a, (hl) ; value if NOT EQUAL
set_sentence_id1:
    inc hl
    ld e, (hl)
    inc hl
    ld d, (hl)
    inc hl
    cp 0xFF ; check if it is "do not modify"
    jr z, set_sentence_id2
    ld (de), a
set_sentence_id2:
    ld a, b ; restore A
    jp (hl) ; it is the same than PUSH HL, RET


; Prints the current command. The command ID is passed in A,
; and the command string is passed in HL

print_verb:
    ld (last_writen), a
    ld a, 7
    ld de, 0x1708
    call do_print_sentence_unlimited
    ld (last_print_coords), de
    call dump_last_line
    ret

; sets as visible or invisible an item in the inventory
; receives the following parameters as DEFB or DEFW after the call:
; DEFW address where is the flag to check
; DEFB mask to apply to the value
; DEFB value to put in the inventory if the result of applying the mask is not zero
; DEFW address where to put the value

show_sentence_on_flag:
    pop hl
    ld c, (hl)
    inc hl
    ld b, (hl)
    inc hl
    ld a, (bc)
    and (hl)
    inc hl
    ld a, (hl)
    inc hl
    ; INC HL doesn't modify the flags
    ; so this jr depends on AND (HL)
    jr nz, show_sentence_on_flag2
    xor a
show_sentence_on_flag2:
    ld c, (hl)
    inc hl
    ld b, (hl)
    inc hl
    ld (bc), a
    jp (hl) ; return

; waits a random value between 1 and 28 frames (aprox. between 0 and 4 seconds)
; Setting bit 6 of do_repaint_flags will cancel the wait
task_wait_random:
    call random_number
    and 0x07
    or 0x01
    add a,a
    add a,a
task_wait_random2:
    push af
    call task_yield
    pop af
    ld hl, do_repaint_flags
    bit 6, (hl) ; REPFLAG_6
    res 6, (hl) ; REPFLAG_6
    ret nz ; if cancelled, return
    dec a
    ret z
    jr task_wait_random2


; does a fade into white and returns
do_blank:
    ld b, 8
do_blank_loop:
    halt
    halt
    ld hl, 0x5800 ; attribute zone
    ld de, 736
do_blank0:
    ld a, (hl)
    and 0x07
    cp 7
    jr z, do_blank1
    inc a
do_blank1:
    ld c, a
    ld a, (hl)
    and 0x38
    cp 0x38
    jr z, do_blank2
    add a, 8
do_blank2:
    or c
    ld (hl), a
    inc hl
    dec de
    ld a, d
    or e
    jr nz, do_blank0
    djnz do_blank_loop
    ret

; does a fade from white and returns
do_unblank:
    ld b, 8
do_blank_loop2:
    halt
    halt
    ld hl, 0x5800
    ld de, 736
do_blank3:
    set 7, h
    res 6, h
    ld a, (hl)
    and 0x07
    ld c, a
    res 7, h
    set 6, h
    ld a, (hl)
    and 0x07
    cp c
    jr z, do_blank4
    ld a, (hl)
    dec a
    ld (hl), a
do_blank4:
    set 7, h
    res 6, h
    ld a, (hl)
    and 0x38
    ld c, a
    res 7, h
    set 6, h
    ld a, (hl)
    and 0x38
    cp c
    jr z, do_blank5
    ld a, (hl)
    sub 8
    ld (hl), a
do_blank5:
    inc hl
    dec de
    ld a, d
    or e
    jr nz, do_blank3
    djnz do_blank_loop2
    ret

stop_sound:
    push af
    ld a, (border_sound_byte)
    or 0x07
    out (254), a ; stop any sound
    pop af
    ret

; Translates a key into a command
; The commands are "compatible" with kempston joysticks

translate_key:
    ; First, read the kempston joystick
    in a, (0x1F)
    cp 0xFF
    jr nz, translate_key2
    xor a
translate_key2:
    ld (translated_key), a
IF DODEBUG
    ld (0x56FC), a
ENDIF
    ld a, (current_key)
    and a
    ret z

    push bc
    push hl
    ld b, 13
    ld hl, key_table
translate_key1:
    cp (hl)
    inc hl
    jr z, found_key
    inc hl
    djnz translate_key1
    pop hl
    pop bc
    ret
found_key:
    ld a, (hl)
    ld (translated_key), a
    pop hl
    pop bc
IF DODEBUG
    ld (0x56FC), a ; translated key
ENDIF
    ret
