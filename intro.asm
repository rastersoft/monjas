; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

do_intro:
    ld a, (border_sound_byte)
    and 0x18
    or 0x07
    out (254), a
    ld (border_sound_byte), a
    ld a, 0x38
    call set_last_line_color
    ld hl, do_repaint_flags
    res 1, (hl) ; REPFLAG_1
    scf
    call set_character_direction_and_position ; look to the left in the bedroom
    DEFW main_character
    DEFW 0x641A
    call do_intro1_clear
    ld hl, do_repaint_flags
    set 0, (hl) ; REPFLAG_0
    ld b, 75
do_intro1:
    halt
    djnz do_intro1
    call make_talk_and_wait
    DEFW main_character
    DEFW sentence_intro1
    call do_intro1_clear
    call make_talk_and_wait
    DEFW main_character
    DEFW sentence_intro2
    call do_intro1_clear
    call make_talk_and_wait
    DEFW main_character
    DEFW sentence_intro3

    ld a, (border_sound_byte)
    and 0x18
    out (254), a
    ld (border_sound_byte), a
    ld a, 0x07
    call set_last_line_color

    ; yellow arrives
    ld hl, do_repaint_flags
    set 1, (hl) ; REPFLAG_1
    scf ; set carry
    call set_character_direction_and_position
    DEFW yellow_character
    DEFW 0x642A
    call move_character_to
    DEFW yellow_character
    DEFW 0x641E
    call make_talk_and_wait
    DEFW yellow_character
    DEFW sentence_intro4

    call move_character_to
    DEFW main_character
    DEFW 0x7010

    call make_talk_and_wait
    DEFW main_character
    DEFW sentence_intro5

    call move_character_to
    DEFW yellow_character
    DEFW 0x7014

    call make_talk_and_wait
    DEFW yellow_character
    DEFW sentence_intro6

    call make_talk_and_wait
    DEFW main_character
    DEFW sentence_intro7

    call make_talk_and_wait
    DEFW yellow_character
    DEFW sentence_intro8

    ; blue arrives
    and a ; look left
    call set_character_direction_and_position
    DEFW blue_character
    DEFW 0x6826
    call move_character_to
    DEFW blue_character
    DEFW 0x681A
    call make_talk_and_wait
    DEFW blue_character
    DEFW sentence_intro9

    call make_talk_and_wait
    DEFW yellow_character
    DEFW sentence_intro10

    call move_and_wait
    DEFW yellow_character
    DEFW 0x2A64

    call move_character_to
    DEFW blue_character
    DEFW 0x6810
    call make_talk_and_wait
    DEFW blue_character
    DEFW sentence_intro11

    call make_talk_and_wait
    DEFW main_character
    DEFW sentence_intro12

    call make_talk_and_wait
    DEFW blue_character
    DEFW sentence_intro13

    scf ; look left
    call set_character_direction_and_position
    DEFW main_character
    DEFW 0x7010

    call make_talk_and_wait
    DEFW main_character
    DEFW sentence_intro14

    call make_talk_and_wait
    DEFW blue_character
    DEFW sentence_intro15

    call make_talk_and_wait
    DEFW main_character
    DEFW sentence_intro16

    and a ; look right
    call set_character_direction_and_position
    DEFW main_character
    DEFW 0x7010
    call make_talk_and_wait
    DEFW main_character
    DEFW sentence_intro17

    call make_talk_and_wait
    DEFW blue_character
    DEFW sentence_intro18

    call move_and_wait
    DEFW blue_character
    DEFW 0x2668

    and a
    call set_character_direction_and_position
    DEFW blue_character
    DEFW 0x4016

    call make_talk_and_wait
    DEFW main_character
    DEFW sentence_intro19

    ret

do_intro1_clear:
    ld hl, 0x8000
    ld de, 0x8001
    ld bc, 6144
    ld (hl), 0
    ldir
    ld hl, 0x9800
    ld de, 0x9801
    ld bc, 767
    ld (hl), 0x38
    ldir ; clear the screen
    ld hl, sprite_base_left
    ld d, MAP_OFFSET_Y - 5
    ld e, MAP_OFFSET_X - 1
    ld iyl, 0x58
    call paint_sprite
    ld hl, sprite_feet_left1
    ld d, MAP_OFFSET_Y
    ld e, MAP_OFFSET_X - 1
    ld iyl, 0x58
    call paint_sprite
    and a
    ret
