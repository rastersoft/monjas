org 23990

; FREE SIZE: 8778 bytes

do_repaint_flags EQU 0
set_character_direction_and_position EQU 1
main_character EQU 2
make_talk_and_wait EQU 3
yellow_character EQU 4
move_character_to EQU 5
blue_character EQU 6
move_and_wait EQU 7
sprite_base_left EQU 8
MAP_OFFSET_Y EQU 9
MAP_OFFSET_X EQU 10
paint_sprite EQU 11
sprite_feet_left1 EQU 12
task_yield:
current_key EQU 13
current_map_address EQU 14
MAP_CHARACTER_OFFSET EQU 15
final_map_address EQU 16
MAP_WIDTH EQU 17
talking_time EQU 18
MAX_MAP_Y EQU 19
task_make_talk_and_wait EQU 20
check_coords EQU 21
talking_length EQU 22
talking_max_characters EQU 23
clear_last_line EQU 24
task_ask_talk EQU 25
make_talk_main EQU 25
make_talk EQU 25
task_wait_B_frames EQU 25

white_character EQU 8
captain_sprites EQU 8
task_move_to_and_wait_for_walk EQU 8
main_character_sprites EQU 98
TILE_INDEX_LANDING EQU 8

map_array EQU 8
entry_spaceship EQU 8
entry_spaceship2 EQU 8

print_verb EQU 9
refresh_objects_characters EQU 9
red_character EQU 9
entry_chuck_the_plant EQU 8
engineer_chicken EQU 8

VERB_RETURN EQU 26
VERB_LOOK EQU 27
VERB_TAKE EQU 28
VERB_TALK EQU 29
VERB_USE EQU 30
VERB_GIVE EQU 31

last_print_coords EQU 8
do_print_sentence_unlimited EQU 8
dump_last_line EQU 8
do_talk_to EQU 34
do_look EQU 45
do_use EQU 36

object_detected EQU 37
character_detected EQU 38
show_sentence_on_flag EQU 39
flags1 EQU 40
flags2 EQU 40
flags3 EQU 40
flags4 EQU 40
flags5 EQU 40
flags6 EQU 40

; galena coordinates must be divided by two
GALENA_POS_X            EQU 8
GALENA_POS_Y            EQU 18

; character definition
CHARACTER_YELLOW        EQU 3
CHARACTER_RED           EQU 4
CHARACTER_BLUE          EQU 5
CHARACTER_GREEN         EQU 6
CHARACTER_WHITE         EQU 7
CHARACTER_MARVIN        EQU 8

; object definition
OBJECT_REACTOR          EQU 10
OBJECT_RADAR            EQU 11
OBJECT_GRID             EQU 12
OBJECT_BOX              EQU 13
OBJECT_POT              EQU 14
OBJECT_PLANT            EQU 15
OBJECT_LOCKER_RED       EQU 16
OBJECT_LOCKER_BLUE      EQU 17
OBJECT_LOCKER_MAIN      EQU 18
OBJECT_LOCKER_YELLOW    EQU 19
OBJECT_LOCKER_GREEN     EQU 20
OBJECT_DIARY            EQU 21
OBJECT_USB_CABLE        EQU 22
OBJECT_RUBBER_CHICKEN   EQU 23
OBJECT_GALENA_DIODE     EQU 24
OBJECT_CHICKEN          EQU 25
OBJECT_YELLOWCHICKEN    EQU 26
OBJECT_GALENA_DETECTOR  EQU 27
OBJECT_GALENA_CRISTAL   EQU 28
OBJECT_IODINE           EQU 29
OBJECT_BOOZE            EQU 30
OBJECT_GRID_IODINE      EQU 31

set_sentence_id EQU 8

do_print_centered EQU 8
main_intro EQU 8
main_loop EQU 8

border_sound_byte EQU 8
set_last_line_color EQU 8
end_animation EQU 8
main_loop_init EQU 8

main_character_look_left EQU 8
main_character_look_right EQU 8
translated_key EQU 8
do_open_locker EQU 8

floating_bus_value EQU 7

include "sentences.asm"
include "font.asm"
include "char_borders.asm"
include "commands.asm"
include "give_command.asm"
include "take_command.asm"
include "intro.asm"
include "menu.asm"

END main_loop
