; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

ask_command2:
    call task_yield

; Shows a menu with all the available commands, allowing the player
; to choose an action using any of the verbs over the available
; objects and characters

ask_command:
    ld a, (talking_time)
    and a
    jr z, ask_command_no_talking ; don't show a menu while talking
                                 ; wait until the key has been released
    ld a, (talking_length)
    ld b, a
    ld a, (talking_max_characters)
    cp b
    ret c ; don't cancel a sentence being talked unless it is complete
    xor a
    ld (talking_time), a
    ret
ask_command_no_talking:
    ld a, (translated_key)
    and a
    jr nz, ask_command2 ; wait until the key has been released
    ld hl, do_repaint_flags
    res 2, (hl) ; REPFLAG_2
    set 5, (hl) ; REPFLAG5
    ld a, (border_sound_byte)
    and 0x18
    ld (border_sound_byte), a
    out (254), a
    call clear_last_line
    ld c, 6
    call task_ask_talk
    DEFW command_return
    DEFB VERB_RETURN
    DEFW command_look
    DEFB VERB_LOOK
    DEFW command_take
    DEFB VERB_TAKE
    DEFW command_talk
    DEFB VERB_TALK
    DEFW command_use
    DEFB VERB_USE
    DEFW command_give
    DEFB VERB_GIVE
    cp VERB_TAKE
    jp z, do_take
    cp VERB_LOOK
    jp z, do_look
    cp VERB_TALK
    jp z, do_talk_to
    cp VERB_USE
    jp z, do_use
    cp VERB_GIVE
    jp z, do_give
    ret

; shows a menu with all the items that are near the main character
; and in the inventory. Returns the ID in the A register, or 0
; if there are no elements

ask_inventory:
    call task_yield
    ; Fill the object that is near
    ld a, (object_detected)
    ld (current_object_name+2), a
    ld hl, (object_detected+1)
    ld (current_object_name), hl
    ; And the character near
    ld a, (character_detected)
    ld (current_character_name+2), a
    ld hl, (character_detected+1)
    ld (current_character_name), hl

    ; Check if it must show Chuck the plant
    call show_sentence_on_flag
    DEFW flags4
    DEFB 0x20 ; FLAG4_5
    DEFB OBJECT_PLANT
    DEFW ask_inventory_plant+2

    ; Check if it must show the diary
    call show_sentence_on_flag
    DEFW flags5
    DEFB 0x02 ; FLAG5_1
    DEFB OBJECT_DIARY
    DEFW ask_inventory_diary+2

    ; Check if it must show the USB cable
    call show_sentence_on_flag
    DEFW flags4
    DEFB 0x02 ; FLAG4_1
    DEFB OBJECT_USB_CABLE
    DEFW ask_inventory_usb_cable+2

    ; Check if it must show the rubber chicken
    call show_sentence_on_flag
    DEFW flags3
    DEFB 0x02 ; FLAG3_1
    DEFB OBJECT_RUBBER_CHICKEN
    DEFW ask_inventory_rubber_chicken+2

    ; Check if it must show the galena diode
    call show_sentence_on_flag
    DEFW flags5
    DEFB 0x08 ; FLAG5_3
    DEFB OBJECT_GALENA_DIODE
    DEFW ask_inventory_galena_diode+2

    ; Check if it must show the yellow chicken
    call show_sentence_on_flag
    DEFW flags5
    DEFB 0x40 ; FLAG5_6
    DEFB OBJECT_YELLOWCHICKEN
    DEFW ask_inventory_yellowchicken+2

    ; Check if it must show the galena detector
    call show_sentence_on_flag
    DEFW flags2
    DEFB 0x10 ; FLAG2_4
    DEFB OBJECT_GALENA_DETECTOR
    DEFW ask_inventory_galena_detector+2

    ; Check if it must show the galena cristal
    ld a, (flags6)
    call set_sentence_id
    DEFB 0x06 ; FLAG6_21
    DEFB 0x04 ; if bit 2 set and bit 1 reset, show sentence
    DEFB OBJECT_GALENA_CRISTAL
    DEFB 0x00
    DEFW ask_inventory_galena_cristal+2

    ; Check if it must show the iodine
    call show_sentence_on_flag
    DEFW flags6
    DEFB 0x08 ; FLAG6_3
    DEFB OBJECT_IODINE
    DEFW ask_inventory_iodine+2

    ; Check if it must show the booze
    call show_sentence_on_flag
    DEFW flags3
    DEFB 0x08 ; FLAG3_3
    DEFB OBJECT_BOOZE
    DEFW ask_inventory_booze+2

    ld c, 13 ; number of elements

    ; check if the list is empty
    ld b, c
    dec b ; don't check the RETURN command
    ld hl, current_object_name+2
ask_inventory3:
    ld a, (hl)
    and a
    jr nz, ask_inventory2
    inc hl
    inc hl
    inc hl
    djnz ask_inventory3
    xor a
    ret

ask_inventory2:
    call task_ask_talk
current_object_name:
    DEFW 0
    DEFB 0
current_character_name:
    DEFW 0
    DEFB 0
ask_inventory_plant:
    DEFW name_plant
    DEFB OBJECT_PLANT
ask_inventory_diary:
    DEFW name_diary
    DEFB OBJECT_DIARY
ask_inventory_usb_cable:
    DEFW name_usb_cable
    DEFB OBJECT_USB_CABLE
ask_inventory_rubber_chicken:
    DEFW name_rubber_chicken
    DEFB OBJECT_RUBBER_CHICKEN
ask_inventory_galena_diode:
    DEFW name_galena_diode
    DEFB OBJECT_GALENA_DIODE
ask_inventory_yellowchicken:
    DEFW name_yellowchicken
    DEFB OBJECT_YELLOWCHICKEN
ask_inventory_galena_detector:
    DEFW name_galena_detector
    DEFB OBJECT_GALENA_DETECTOR
ask_inventory_galena_cristal:
    DEFW name_galena_cristal
    DEFB OBJECT_GALENA_CRISTAL
ask_inventory_iodine:
    DEFW name_iodine
    DEFB OBJECT_IODINE
ask_inventory_booze:
    DEFW name_booze
    DEFB OBJECT_BOOZE

    DEFW command_return
    DEFB VERB_RETURN
    ret

