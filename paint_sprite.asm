; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

; These functions are here because they are used only
; when X or Y are negative

paint_check_x_negative:
    add a, e; adding e + (-x) gives the new width
    ret nc ; if C is not set, or Z is set, it means that the sprite is 100% out of the screen
    ret z
    jp paint_continue

paint_y_negative:
    ld a, d
    add a, b; adding b + (-y) gives the new height
    ret nc ; if C is not set, or Z is set, it means that the sprite is 100% out of the screen
    ret z
    ld b, a ; this is the new height
    push bc
    push de
    ld a, c
    add a, a
    add a, a
    add a, a ; multiply width by 8
    exx
    bit 6, b ; if it has mask, multiply it by 16, not 8
    exx
    jp z, paint1 ; it is more common to not have a mask, so optimize the jump for that case
    add a, a
paint1:
    add a, c ; in this program, we always have attributes
    ld e, a ; width * 9 or width * 17
	ld a, d
	neg ; a is now the number of characters to jump over
    ld b, a
    ld d, 0
paint1b:
    ; multiply by several sums, because the maximum number is 7, so it doesn't compensate to use rotations
    add hl, de ; hl + (-y) * width * (9 or 17, depending if it has mask or not)
    djnz paint1b
    pop de
    pop bc
    ld d, 0 ; new Y coordinate is 0
    jp paint2

paint_x_negative:
    ld a, e
    add a, c; adding e + (-x) gives the new width
    ld c, a ; this is the new width
    ld a, e
    neg ; a is now the number of characters to jump over
    push de
    ld e, a
    ld d, 0
    add hl, de
    exx
    bit 6, b ; has mask
    exx
    jp z, paint2b
    add hl, de
paint2b:
    push de
    exx
    pop de ; increment between scanlines in the sprite
    exx
    pop de
    ld e, 0 ; new X coordinate is 0
    ld iyh, 1
    jp do_paint

; --------
; paints an sprite with mask and attributes
; at YX = DE, with pixels and attributes
; interleaved, stored in the address pointed
; by HL.
; if an attribute has FLASH active, it won't
; be painted (thus preserving the current
; atribute)
; if an attribute has FLASH and BRIGHT active,
; it will be replaced by the value stored in IYl.
; These two cases will work only if the sprite
; has mask.

paint_sprite:
    ld a, (hl)
    exx
    ld b, a ; store flags in B'
    exx
    ld b, a
    rra
    rra
    rra
    and 0x07
    inc a
    ld c, a ; get the width

; check if the sprite is outside the screen horizontally
    bit 7, e
    jr nz, paint_check_x_negative ; the most common case is X >= 0
    ld a, e
    cp 32
    ret nc ; return if it is fully horizontally outside the screen (to the right)

paint_continue:
    inc hl ; HL points to the pixel data
    ld iyh, 0 ; flag for detecting if the sprite is horizontally outside of the screen
    ld a, b
    and 0x07
    inc a
    ld b, a ; get the height ; now B has the height, and C the width

    ; Adjust the coordinates in case it is outside by the top or the bottom
    bit 7, d ; check if Y is negative
    jr nz, paint_y_negative

    ld a, d
    cp height
    ret nc ; return if it is fully outside the screen
    add a, b ; check if it is fully inside the screen
    cp height+1
    ; if C is set, the sprite is 100% inside the screen
    jp c, paint2
    ld a, height
    sub d
    ld b, a

paint2:
    bit 7, e ; check if X is negative
    jr nz, paint_x_negative
    ld a, e
    add a, c ; check if it is fully inside the screen
    cp 33
    ; if C is set, the sprite is 100% inside the screen
    jp c, do_paint
    sub 32
    exx
    ld d, 0
    ld e, a ; increment between scanlines in the sprite
    exx
    ld a, 32
    sub e
    ld c, a
    ld iyh, 2

do_paint:
    set 7, d ; now DE points to the screen destination
    ; now we have in B the height in characters, and in C the width in characters
    ; HL points to sprite data, and DE points to pixel screen buffer destination

    ex de, hl
   ; calculate the attribute destination address from the pixel destination address
    push hl
    ld a, l
    and 0x1F
    ld l, a
    ld a, h
    rrca
    rrca
    rrca
    ld h, a
    and 0xE0
    or l
    ld l, a
    ld a, h
    and 3
    or 0x98
    ld h, a ; now HL contains the attribute address, and the pixel address is in the stack
    ex (sp), hl ; swap both and start

    ; now we have in B the height in characters, and in C the width in characters
    ; DE points to sprite data, HL points to pixel screen buffer destination,
    ; the color attribute destination is in the stack, and DE' contains the
    ; horizontal increment for the sprite scanlines (needed only when the sprite
    ; is, horizontally, partly outside the screen; in other case it is zero)

    exx
    bit 6, b
    exx
    jp z, paint_without_mask
    push de
    ld a, 32
    sub c
    ld d, 0
    ld e, a ; now DE has the increment for next scanline
    exx
    pop hl ; points to the sprite data
    ld a, iyh
    and a
    jr nz, paint_with_mask_cut

    exx
; Loop to paint the pixels of the sprite with interleaved attributes
; when the sprite is fully inside the screen horizontally
paint_with_mask:
    ld iyh, c ; store the width in IYh
    ld a, b
paint_with_mask2:
    ex af, af'
    ld c, 8
paint5b:
    ld b, iyh ; initialize inner loop counter from IYh (width)
paint5:
    ld a, (hl) ; original byte
    exx
    and (hl) ; apply mask
    inc hl
    or (hl) ; apply sprite data
    inc hl
    exx
    ld (hl), a ; store final byte
    inc hl
    djnz paint5
    add hl, de ; jump to the next scanline
    dec c
    jp nz, paint5b
    ; paint the attributes
    ld b, iyh ; initialize inner loop counter from IYh (width)
    ex (sp), hl ; load the attributes destination address in HL.
                ; It is faster than using IX because the sprites with mask are all wider than 2 characters
    ld c, 0x40
paint5c:
    exx
    ld a, (hl)
    inc hl ; load the attribute
    exx
    and a
    ; FLASH off -> store the attribute as-is
    jp p, paint5d
    and c
    ; FLASH on, BRIGHT off -> ignore the attribute, keep the current one
    jr z, paint5e
    ; FLASH on, BRIGHT on -> replace the attribute
    ld a, iyl
paint5d:
    ld (hl), a ; store the new attribute
paint5e:
    inc hl
    djnz paint5c
    add hl, de ; jump to the next scanline
    ex (sp), hl ; load the pixels destination address in HL
    ex af, af'
    dec a
    jp nz, paint_with_mask2
    pop hl ; remove the attributes address from the stack
    ret

; Loop to paint the pixels of the sprite with interleaved attributes
; but cut left or right because it is outside the screen
paint_with_mask_cut:
    sla e ; since it has mask, we must jump over twice as many bytes (but D is always zero because an sprite has a maximum width of 8)
    ld b, a ; keep IYh (which is also in A) in B' because it will be useful in paint_with_mask_cut
    exx
    ld iyh, c
    ld a, b
paint_with_mask_cut2:
    ex af, af'
    ld c, 8 ; use C as the middle loop counter
paint5b2:
    ld b, iyh ; initialize inner loop counter from IYh
paint52:
    ld a, (hl) ; original byte
    exx
    and (hl) ; apply mask
    inc hl
    or (hl) ; apply sprite data
    inc hl
    exx
    ld (hl), a ; store final byte
    inc hl
    djnz paint52
    add hl, de ; jump to the next scanline
    exx
    add hl, de ; jump over the pixels and the mask
    exx
    dec c
    jp nz, paint5b2
    exx
    sra e ; the color attributes has half as many bytes as the pixels+mask
    bit 0, b
    jr z, paint5f2
    ; CARRY is always zero because we did first an SLA, which added a
    ; zero to the right. That zero is now in the CARRY after the previous SRA E
    sbc hl, de ; make it point to the attributes
    sla e
paint5f2
    exx
    ; paint the attributes
    ld b, iyh
    ex (sp), hl
    ld c, 0x40
paint5c2:
    exx
    ld a, (hl)
    inc hl ; load the attribute
    exx
    and a
    jp p, paint5d2
    and c
    jr z, paint5e2
    ld a, iyl
paint5d2:
    ld (hl), a ; save the attribute
paint5e2:
    inc hl
    djnz paint5c2
    add hl, de ; jump to the next scanline
    exx
    add hl, de
    bit 0, b
    jr nz, paint5g2
    sla e
paint5g2:
    exx
    ex (sp), hl
    ex af, af' ; restore the outer loop counter
    dec a
    jp nz, paint_with_mask_cut2
    pop hl ; remove the attributes address from the stack
    ret

paint_without_mask:
    pop ix
    push de
    ld a, c ; in this game, all the sprites without mask are 4 characters wide
    cp 4
    jr nz, paint_without_mask_cut
    ld a, b
    ld de, 4
    add hl, de ; push words "backwards"
    ld e, 32 ; D is already 0
    exx
    pop hl ; origin
    ld de, 4 ; in this game, all the sprites without mask are 4 characters wide
    add ix, de ; attributes destination
    di ; interrupts must be disabled because an INT would store the return address in SP, which is changed
    ld (old_stack), sp ; preserve SP. We can reuse this address because it is used only during asynchronous tasks
    ld c, a
paint4d2:
    ld b, 8
paint4c2:
    ld sp, hl ; HL contains the source address
    add hl, de ; must be done before the POP AF because ADD modifies F register
    exx
    pop bc
    pop af ; read 4 bytes from the sprite
    ld sp, hl ; HL' contains the destination address
    push af
    push bc ; and store it in the screen buffer
    add hl, de ; must be done after the PUSH AF because ADD modifies F register
    exx
    djnz paint4c2
    ld sp, hl
    add hl, de
    exx
    pop bc
    pop af
    ld sp, ix
    push af
    push bc ; color attributes
    add ix, de ; jump to the next scanline
    exx
    dec c
    jp nz, paint4d2 ; the condition is true more times than false, so it is better a JP
    ld sp, (old_stack)
    ret

paint_without_mask_cut:
    ld de, 32
    cp 1
    jr z, paint_without_mask_1
    inc hl
    inc hl
    cp 2
    jr z, paint_without_mask_2
paint_without_mask_3:
    ld a, b
    inc hl
    exx
    pop hl ; origin
    ld de, 3
    add ix, de
    inc e ; it is faster than three IX and an ld de, 4
    di ; interrupts must be disabled
    ld (old_stack), sp ; preserve SP. We can reuse this address because it is used only during asynchronous tasks
    ld c, a
paint4d2_3:
    ld b, 8
paint4c2_3:
    ld sp, hl ; HL contains the source address
    add hl, de ; must be done before the POP AF because ADD modifies F register
    exx
    pop af ; read 2 bytes from the sprite
    dec sp
    pop bc ; read 2 extra bytes, but one is overlapped
    ld sp, hl ; HL' contains the destination address
    push bc
    inc sp
    push af ; and store them in the screen buffer
    add hl, de ; must be done after the PUSH AF because ADD modifies F register
    exx
    djnz paint4c2_3
    ld sp, hl
    add hl, de
    exx
    pop af
    dec sp
    pop bc
    ld sp, ix
    push bc
    inc sp
    push af ; color attributes
    add ix, de ; jump to the next scanline
    exx
    dec c
    jp nz, paint4d2_3 ; the condition is true more times than false, so it is better a JP
    ld sp, (old_stack)
    ret

paint_without_mask_1:
    ld a, b
    exx
    pop hl ; origin
    ld de, 4
    ld c, a
paint4d2_1:
    ld b, 8
paint4c2_1:
    ld a, (hl)
    add hl, de
    exx
    ld (hl), a
    add hl, de
    exx
    djnz paint4c2_1
    ld a, (hl)
    add hl, de
    exx
    ld (ix+0), a
    add ix, de ; jump to the next scanline
    exx
    dec c
    jp nz, paint4d2_1 ; the condition is true more times than false, so it is better a JP
    ret


paint_without_mask_2:
    ld a, b
    exx
    pop hl ; origin
    inc ix
    inc ix ; attributes destination
    ld de, 4
    di ; interrupts must be disabled
    ld (old_stack), sp ; preserve SP. We can reuse this address because it is used only during asynchronous tasks
    ld c, a
paint4d2_2:
    ld b, 8
paint4c2_2:
    ld sp, hl ; HL contains the source address
    add hl, de ; must be done before the POP AF because ADD modifies F register
    exx
    pop af ; read 4 bytes from the sprite
    ld sp, hl ; HL' contains the destination address
    push af ; and store it in the screen buffer
    add hl, de ; must be done after the PUSH AF because ADD modifies F register
    exx
    djnz paint4c2_2
    ld sp, hl
    add hl, de
    exx
    pop af
    ld sp, ix
    push af ; color attributes
    add ix, de ; jump to the next scanline
    exx
    dec c
    jp nz, paint4d2_2 ; the condition is true more times than false, so it is better a JP
    ld sp, (old_stack)
    ret
