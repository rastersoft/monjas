MAIN_DEPS=main.asm spacesuit_interleaved.asm definitions.asm map.asm int_im2.asm paint_sprite.asm\
    character_manager.asm task_manager.asm pushpop.asm talk.asm utils.asm font.asm\
    detect_objects.asm intro.asm commands.asm conversations.asm char_borders.asm\
    behavior_main_character.asm talk_command.asm use_command.asm take_command.asm look_command.asm\
    copy_attributes.asm sprites_entries.asm give_command.asm end_animation.asm menu.asm\

TEST_DEPS = sentences.asm intro.asm font.asm char_borders.asm commands.asm give_command.asm\
    take_command.asm menu.asm test_size.asm

all: english spanish

english: monjas_bin_english.tap test_eng.bin screen.tap loader.tap monjas_eng.z80
	@cat loader.tap screen.tap monjas_bin_english.tap > monjas_english.tap
	@ls -l main_english.bin test_eng.bin
	@echo "Max for main.bin: 41545 bytes"
	@echo "Max for test.bin:  8730 bytes"


spanish: monjas_bin_spanish.tap test_spa.bin screen.tap loader.tap monjas_spa.z80
	@cat loader.tap screen.tap monjas_bin_spanish.tap > monjas_spanish.tap
	@ls -l main_spanish.bin test_spa.bin
	@echo "Max for main.bin: 41545 bytes"
	@echo "Max for test.bin:  8730 bytes"

# compress sentences

compress_spanish:
	@pypy3 ./convert_sentences.py sentences_origin.asm sentences_spa.asm
	@cp sentences_spa.asm sentences.asm

compress_english:
	@pypy3 ./convert_sentences.py sentences_english.asm sentences_eng.asm
	@cp sentences_eng.asm sentences.asm

# compile the BASIC loader

loader.tap: bas2tap loader.bas
	@./bas2tap -a1 -sMONJAS loader.bas loader.tap

# convert the loading screen and generate the .TAP file

screen.tap: screen.png pic_converter.py bin2z80
	@echo Converting loading screen, this will need some minutes
	@./pic_converter.py screen.png screen.asm
	@pasmo --equ USE_HALT=1 screen.asm screen.bin
	@./bin2z80 screen.bin screen.z80 16384 23296
	@pasmo --tap --equ USE_HALT=0 screen.asm screen.tap

# compile .Z80 snapshot

monjas_spa.z80: main_spanish.bin bin2z80
	@./bin2z80 main_spanish.bin monjas_spa.z80 23990 23990

monjas_eng.z80: main_english.bin bin2z80
	@./bin2z80 main_english.bin monjas_eng.z80 23990 23990

# create .TAP files of the main binary

monjas_bin_english.tap: main_english.bin
	@pasmo --tap main.asm monjas_bin_english.tap

monjas_bin_spanish.tap: main_spanish.bin
	@pasmo --tap main.asm monjas_bin_spanish.tap

# compile main binary

main_spanish.bin: sentences_spa.asm $(MAIN_DEPS)
	@cp sentences_spa.asm sentences.asm
	@pasmo main.asm main_spanish.bin

main_english.bin: sentences_eng.asm $(MAIN_DEPS)
	@cp sentences_eng.asm sentences.asm
	@pasmo main.asm main_english.bin

# compile size tests

test_eng.bin: sentences_eng.asm $(TEST_DEPS)
	@cp sentences_eng.asm sentences.asm
	@pasmo test_size.asm test_eng.bin

test_spa.bin: sentences_spa.asm $(TEST_DEPS)
	@cp sentences_spa.asm sentences.asm
	@pasmo test_size.asm test_spa.bin

# compile tools

bin2z80: bin2z80.c
	@gcc -o bin2z80 bin2z80.c

bas2tap: bas2tap.c
	@gcc -o bas2tap bas2tap.c -lm

clean:
	rm -f *.bin *.z80 *.tap screen.asm bas2tap bin2z80 sentences.asm
