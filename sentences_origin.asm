sentence_intro1:
    DEFB "Bitacora personal: empieza\n"
    DEFB "otro prometedor dia de\n"
    DEFB "mision en el espacio.",0
sentence_intro2:
    DEFB "¿Que nueva aventura me deparara\n"
    DEFB "hoy? ¿Luchar contra temibles\n"
    DEFB "seres extraterrestres...?",0
sentence_intro3:
    DEFB "¿Rescatar una nave?\n"
    DEFB "¿O tal vez...?",0
sentence_intro4:
    DEFB "¡Eh, imbecil! ¡El vater\n"
    DEFB "no se va a limpiar solo!",0
sentence_intro5:
    DEFB "¡Dejame!\n¡Estoy ocupado!",0
sentence_intro6:
    DEFB "¡Juas! ¿Sigues con tu estupido\n"
    DEFB "diario? ¡Capitán espacial Ros!",0
sentence_intro7:
    DEFB "¡Oye!\n¡Devuelvemelo!",0
sentence_intro8:
    DEFB "¡Ups! Vaya, le\n"
    DEFB "di al reset. Juas.",0
sentence_intro9:
    DEFB "Pierdete,\nAmarill.",0
sentence_intro10:
    DEFB "¿Y que haras si no? ¿Abrirme\n"
    DEFB "otro expediente? Juas.",0
sentence_intro11:
    DEFB "Pasa de ese psicopata. Tarde o\n"
    DEFB "temprano le alcanzara el karma.",0
sentence_intro12:
    DEFB "Me ha borrado\nmi diario...",0
sentence_intro13:
    DEFB "Ros... ¿En serio sigues\n"
    DEFB "con esas fantasias?",0
sentence_intro14:
    DEFB "¡No son fantasias! ¡Ahi afuera\n"
    DEFB "hay grandes aventuras! ¡Y yo\n"
    DEFB "las vivire algún dia!",0
sentence_intro15:
    DEFB "Esto no es una pelicula, Ros, es\n"
    DEFB "la vida real. Solo hay trabajo,\n"
    DEFB "y algo de diversion ocasional.",0
sentence_intro16:
    DEFB "No quiero aceptarlo. La vida\n"
    DEFB "tiene que ser algo más que\n"
    DEFB "languidecer en una...",0
sentence_intro17:
    DEFB "Una maldita estación espacial\n"
    DEFB "perdida que se cae a cachos.",0
sentence_intro18:
    DEFB "Pon los pies en el suelo, Ros...\n"
    DEFB "Te lo digo por tu bien.",0
sentence_intro19:
    DEFB "Conseguiré salir de\n"
    DEFB "esta estación como sea.",0



sentence_poor_chuck:
    DEFB "¡No! ¡Pobre Chuck!",0



sentence_not_now:
    DEFB "Ahora no tengo ganas.\n"
    DEFB "En todos los sentidos.",0

sentence_too_far:
    DEFB "Mejor no me alejo mas.",0

sentence_nothing_to_take:
    DEFB "No hay nada que coger.",0
sentence_cant_take_that:
    DEFB "No puedo coger eso.",0
sentence_cant_do_that:
    DEFB "No puedo hacer eso.",0
sentence_cant_take_people:
    DEFB "No puedo coger personas.",0
sentence_nothing_to_see:
    DEFB "No veo nada.",0
sentence_nothing_to_use:
    DEFB "No hay nada que pueda usar.",0
sentence_nothing_to_talk_to:
    DEFB "La psicóloga me ha recomendado\n"
    DEFB "dejar de hablar al aire.",0
sentence_rubber_chicken_alone:
    DEFB "Me haría falta un\n"
    DEFB "cable entre dos postes.",0
sentence_rubber_chicken_pot:
    DEFB "¡Aparta eso de la cocina!\n"
    DEFB "¿Quieres que explotemos?",0
sentence_rubber_chicken_pot2:
    DEFB "(Cocina como nadie, pero\n"
    DEFB "le encanta el drama).",0
sentence_rubber_chicken_pot3:
    DEFB "¡Te he oido!",0


sentence_no_one_near:
    DEFB "¿Darle a quien?\nNo hay nadie cerca.",0


sentence_dont_give:
    DEFB "Mejor no.",0


sentence_take_box:
    DEFB "Pesa demasiado.",0
sentence_take_pot:
    DEFB "No quiero escaldarme.",0

sentence_already_found_galena:
    DEFB "¿Para qué? Ya\n"
    DEFB "conseguí la galena.",0

sentence_look_grid:
    DEFB "Me ha parecido\nver moverse algo.",0
sentence_look_box:
    DEFB "Es un contenedor fabricado con\n"
    DEFB "fibra de celulosa y sellado con\n"
    DEFB "cilindros de acero afilados\n"
    DEFB "insertados por percusión.",0
sentence_look_pot:
    DEFB "¿Cómo es que el agua no se\n"
    DEFB "evapora si estamos en el vacío?",0
sentence_look_plant:
    DEFB "Es Chuck. Está muy desmejorada\npor la exposición al vacío.",0
sentence_talk_plant:
    DEFB "¡Hola Chuck!",0
sentence_look_rubber_chicken:
    DEFB "Que raro, tiene una\npolea en el medio.",0
sentence_look_diary:
    DEFB "Es mi diario electrónico.\n"
    DEFB "Está vacío. Tiene un conector\n"
    DEFB "USB en un lateral.",0
sentence_look_diary2:
    DEFB "Es mi diario electrónico con\n"
    DEFB "los mapas estelares.",0
sentence_look_usb_cable:
    DEFB "Un cable USB normal.",0
sentence_look_galena_diode:
    DEFB "Es casi todo\ngalena cristalina.",0
sentence_look_radar_empty:
    DEFB "El radar solo cubre un sector,\n"
    DEFB "pero sus mapas cubren nueve y\n"
    DEFB "son ampliables mediante USB.",0
sentence_look_radar_empty2:
    DEFB "No hay nada en el radar.",0
sentence_look_radar_active:
    DEFB "¡Una nave quiere aterrizar!\n"
    DEFB "Avisaré a Verd.",0
sentence_look_reactor:
    DEFB "Es el reactor nuclear de la\n"
    DEFB "estación. Nunca ha funcionado\n"
    DEFB "demasiado bien.",0
look_detector_1:
    DEFB "¡Detector de galena Mikasa!",0
look_detector_2:
    DEFB "Gracias por adquirir el\n"
    DEFB "fabuloso detector de galena\n"
    DEFB "MiKasa.",0
look_detector_3:
    DEFB "Con él, su trabajo en la mina\n"
    DEFB "será tan sencillo y divertido\n"
    DEFB "como un paseo por el campo.",0
look_detector_4:
    DEFB "Ahora, compatible con\n"
    DEFB "personas sordas.",0
sentence_look_galena_cristal:
    DEFB "Es un cristal de galena\n"
    DEFB "del tamaño de un puño.",0
sentence_look_iodine:
    DEFB "Es un bote de yodo\n"
    DEFB "desinfectante.\n"
    DEFB "¡Agh, pringa!",0
sentence_look_chicken:
    DEFB "Mmmm... se ve apetitoso.",0


sentence_a_ship:
    DEFB "¡La alerta del radar!\n"
    DEFB "¡Y es el turno de Verd...!",0
sentence_radar_task_for_green:
    DEFB "Silenciaré la alarma, pero\n"
    DEFB "esto es trabajo para Verd.",0
sentence_take_radar:
    DEFB "No soy TAN cleptómano.",0
sentence_take_radar2:
    DEFB "(además, está atornillado)",0

sentence_take_reactor:
    DEFB "¿En serio crees que me\n"
    DEFB "voy a meter un reactor\n"
    DEFB "nuclear en el bolsillo?",0
sentence_use_reactor:
    DEFB "Mejor no lo toco. Quema.",0

sentence_look_locker_yellow:
    DEFB "Es la taquilla de Amarill. Aquí\n"
    DEFB "guarda toda su basura antigua.\n"
    DEFB "Hmmm... ¿Eso es una\nradio de galena?",0
sentence_take_locker_yellow1:
    DEFB "No quiero nada de Amarill.",0
sentence_take_locker_yellow2:
    DEFB "Cogeré sólo el\ndiodo de galena.",0

sentence_look_locker_red_with_cable:
    DEFB "Esta es la taquilla de Roj.\n"
    DEFB "Está todo muy ordenado. Hay\n"
    DEFB "varios libros de cocina, un\n"
    DEFB "pendrive y un cable USB.",0
sentence_look_locker_red_no_cable:
    DEFB "Esta es la taquilla de Roj.\n"
    DEFB "Está todo muy ordenado. Hay\n"
    DEFB "varios libros de cocina y un\n"
    DEFB "pendrive.",0
sentence_take_locker_red1:
    DEFB "No debería cogerlo\n"
    DEFB "sin permiso.",0
sentence_take_locker_red2:
    DEFB "Cogeré el cable.",0
sentence_take_locker_red3:
    DEFB "No puedo coger nada más.",0



sentence_look_locker_green_with_chicken:
    DEFB "Es la taquilla de Verd. Está\n"
    DEFB "llena de botellas vacías y...\n"
    DEFB "¡un pollo de goma!",0
sentence_look_locker_green_no_chicken:
    DEFB "Es la taquilla de Verd. Está\n"
    DEFB "llena de botellas vacías.",0
sentence_take_locker_green:
    DEFB "Cogeré sólo el pollo.\n"
    DEFB "No creo que le importe.",0
sentence_take_locker_green1:
    DEFB "No necesito nada\nde todo eso.",0
sentence_take_locker_green2:
    DEFB "No necesito una botella vacía.",0

sentence_look_locker_blue:
    DEFB "Es la taquilla de Azu. Está\n"
    DEFB "llena de libros de medicina y...\n"
    DEFB "¿novelas eróticas?",0
sentence_take_locker_blue:
    DEFB "No me interesan.",0

sentence_look_yellowchicken:
    DEFB "Pues se ve apetitoso...",0
sentence_take_yellowchicken:
    DEFB "No creo que\nle importe ya...",0

sentence_look_locker_main_with_diary:
    DEFB "Es mi taquilla. Sólo está\n"
    DEFB "mi diario electrónico.",0
sentence_look_locker_main_no_diary:
    DEFB "Es mi taquilla. Está vacía.",0
sentence_take_locker_main1:
    DEFB "Cogeré el diario.",0

sentence_land_ship1:
    DEFB "Aquí estacion Tipo-Monjas\n"
    DEFB "Epsilon 9. Responda.",0
sentence_land_ship2:
    DEFB "Aquí nave de carga Rayos-X\n"
    DEFB "Delta 1 pidiendo permiso para\n"
    DEFB "aterrizaje de emergencia.",0
sentence_land_ship3:
    DEFB "Aquí estación Tipo-Monjas\n"
    DEFB "Epsilon 9. Mensaje recibido.\n"
    DEFB "Tiene permiso para\naterrizar en el hangar.",0
sentence_land_ship4:
    DEFB "¡Gracias! No sabía cuanto\ntiempo más podría resistir.",0
sentence_land_ship5:
    DEFB "Bienvenido a la...",0
sentence_land_ship6:
suspensive_dots:
    DEFB "...",0
sentence_land_ship7:
    DEFB "FLUSHHHHhhhhh...",0
sentence_land_ship8:
    DEFB "Gracias, ya no podía\naguantar más.",0


sentence_look_green:
    DEFB "Borracho, como siempre.\nQue triste...",0
talk_green_1:
    DEFB "Hola verd.",0
talk_green_2:
    DEFB "¡¡¡¡Bero shi essshhh\n-hic- Rosssss!!!!",0
talk_green_3:
    DEFB "¿Otra vez de borrachera?",0
talk_green_4:
    DEFB "Io no -hic- sssstoy borrashhh.\n"
    DEFB "Eresssshhh tú, que esh... -hic-\n"
    DEFB "eshtash borrossshhh.",0
talk_green_5:
    DEFB "Me pregunto donde esconderás\n"
    DEFB "tanto alcohol...",0
talk_green_6:
    DEFB "Essshooo nun... ca\n"
    DEFB "looo sshhhaabrassss.",0
talk_green_7:
    DEFB "Hay una nave en el radar.",0
talk_green_8:
    DEFB "Puessshhh io veo... -hic-\ndosssshhhh. Jijijiji.",0
talk_green_9:
	DEFB "Será mejor que hable con Azu.",0
talk_green_10:
    DEFB "¿Me das un trago?",0
talk_green_11:
    DEFB "Essshhhhto essss -hic- sholo\n"
    DEFB "para -hic- gennnte\n"
    DEFB "adul... -hic- ta.",0
talk_green_12:
    DEFB "Azu ha encontrado un montón\n"
    DEFB "de botellas con alcohol...",0
talk_green_15:
    DEFB "¡Nooo... mi teshhooorooo...!\n"
    DEFB "Vooy a veeer... Peero nooo...\n"
    DEFB "meee -hic- sssshigasssshhh...",0
talk_green_16:
	DEFB "¡¡¡Te diiii -hic- jeee quee noo\n"
    DEFB "-hic- me sshhiguiiiiesrasshh!!!",0
talk_green_17:
    DEFB "Naaahhhh... essshhtaba tooodo\n"
    DEFB "en shu shhhitioooo.",0
talk_green_18:
    DEFB "Hasta luego.",0
talk_green_19:
    DEFB "-hic-",0

sentence_green_iodine1:
    DEFB "Ups... Perdón.",0
sentence_green_iodine2:
	DEFB "No te preee... ocupesh... con\n"
    DEFB "el vo... omito... no she nota.",0
sentence_green_iodine3:
    DEFB "Pero el vomito esta\n"
    DEFB "dentro del traje...",0

sentence_use_iodine_in_object:
    DEFB "No quiero mancharlo.",0
sentence_use_iodine_in_person:
    DEFB "No veo ninguna herida.",0
sentence_use_iodine_in_white:
    DEFB "No quiero estropear el blanco\n"
    DEFB "nuclear de su traje.",0
sentence_give_iodine_to_white:
    DEFB "¿Servirá para limpiar\n"
    DEFB "el cristal de galena?",0
sentence_give_iodine_to_white2:
    DEFB "¿Limpiar algo con YODO?",0

sentence_eat_diary:
    DEFB "Me pregunto si el diario\n"
    DEFB "pasaría por su garganta...",0
sentence_eat_diary2:
    DEFB "Nah... seguro que acabaría\n"
    DEFB "en mi estómago... o más abajo.",0

sentence_transferred_data_to_diary:
    DEFB "He copiado los mapas en mi\n"
    DEFB "diario. Espero que no se\n"
    DEFB "entere la SGAE.",0

sentence_need_usb:
    DEFB "No puedo conectarlo\n"
    DEFB "sin un cable adecuado.",0

sentence_look_blue:
    DEFB "Es Azu, medico y comandante\n"
    DEFB "de la estacion.",0
talk_blu_1:
    DEFB "Buenas, comandante.",0
talk_blu_2:
    DEFB "Sólo cosas urgentes, Ros.\nTengo mucho lío.",0
talk_blu_3:
    DEFB "Amarill me ha borrado\nmi diario personal.",0




talk_blu_4:
    DEFB "Luego le abriré otro expediente,\n"
    DEFB "pero ya sabes que no puedo hacer\n"
    DEFB "mas, es familia de los jefes.",0




talk_blu_5:
    DEFB "Se acerca una nave.",0
talk_blu_6:
    DEFB "Avisa a Verd, es su turno.",0
talk_blu_6b:
    DEFB "Verd está borracho...",0
talk_blu_6c:
    DEFB "Entonces hazlo tú,\nyo tengo mucho lío.",0
talk_blu_7:
    DEFB "¿Qué hace?",0
talk_blu_8:
    DEFB "Cosas urgentes, Ros.\nTengo mucho lío.",0
talk_blu_9:
    DEFB "¿Tenemos galena?",0
talk_blu_10:
    DEFB "Esto es la enfermería, Ros.\n"
    DEFB "Y no somos una estación minera.",0
talk_blu_11:
    DEFB "¿Tenemos alcohol?",0
talk_blu_12:
    DEFB "Sólo hay tintura de Yodo.\n"
    DEFB "Toma un bote y cúrate tú.\n"
    DEFB "Yo tengo mucho lío.",0
talk_blu_17:
    DEFB "No se preocupe, no es urgente.",0
talk_blu_18:
    DEFB "Entonces déjame trabajar, por\n"
    DEFB "favor. Tengo mucho lío.",0


sentence_look_red:
    DEFB "Roj cocina como nadie.",0
talk_red_1:
    DEFB "Hola Roj.",0
talk_red_2:
    DEFB "Hola Ros. No puedes picar nada.\nEstamos justos de comida.",0
talk_red_2b:
    DEFB "Pero si no he\npedido nada...",0
talk_red_2c:
    DEFB "Ya, pero conozco muy\n"
    DEFB "bien a tu estómago.",0
talk_red_3:
    DEFB "¿Falta mucho para comer?",0
talk_red_4:
    DEFB "¿Ya tienes hambre? Si\nacabamos de desayunar...",0
talk_red_5:
    DEFB "¿Puedo usar el horno?",0
talk_red_6:
    DEFB "Sabes que lo estropeaste\n"
    DEFB "intentando cargar el móvil. Y\n"
    DEFB "Amarill aún no lo ha arreglado.",0
talk_red_7:
	DEFB "Es que en aquella\nweb decían que...",0
talk_red_8:
    DEFB "Necesito un pincho USB.\n"
    DEFB "¿Me puedes dejar uno?",0
talk_red_9:
    DEFB "Tengo uno, pero no puedo\n"
    DEFB "dejártelo, lo siento.",0
talk_red_10:
    DEFB "¿Tienes un cable USB?",0
talk_red_11:
    DEFB "Hay uno en mi taquilla,\n"
    DEFB "puedes cogerlo.",0
talk_red_12:
    DEFB "¿Puedo coger ese pollo?",0
talk_red_13:
    DEFB "No, que te lo comes.",0
talk_red_14:
    DEFB "Venga, va...",0
talk_red_15:
    DEFB "Ni se te ocurra. Es la\n"
    DEFB "comida para hoy.",0
talk_red_16:
    DEFB "No quería nada.",0
talk_red_17:
    DEFB "De eso sí te puedo ofrecer.\n"
    DEFB "Sírvete cuanto quieras.",0
talk_red_18:
    DEFB "¿Podemos invitar al\n"
    DEFB "capitán Blanc a comer?",0
talk_red_19:
    DEFB "Azu ha prohibido los invitados.\n"
    DEFB "Estamos muy justos de comida.",0


sentence_look_yellow:
    DEFB "Siempre me ha odiado,\nno se por qué.",0
talk_yellow_1:
    DEFB "Hola.",0
talk_yellow_2:
    DEFB "Piérdete Imbécil.\nO te aso en el reactor.",0
talk_yellow_3:
    DEFB "El horno de la cocina no\nfunciona.",0
talk_yellow_4:
    DEFB "¡Otra vez! Ya estoy harto de\n"
    DEFB "Roj y su maldito horno.\n"
    DEFB "¡No se puede arreglar!",0
talk_yellow_5:
    DEFB "Adiós.",0
talk_yellow_6:
    DEFB "Por fin dices\nalgo interesante.",0
talk_yellow_7:
    DEFB "¡No toques el reactor!",0
talk_yellow_8:
    DEFB "Oh, oh...",0
talk_yellow_9:
    DEFB "¡¿Pero qué...?!",0
talk_yellow_10:
    DEFB "¡¡¡Eso ha sido el reactor!!!",0
talk_yellow_11:
    DEFB "Ups...",0

sentence_look_white:
    DEFB "¿Con qué lavará\nsu traje espacial?",0
talk_white_1:
    DEFB "Bienvenido.",0
talk_white_2:
    DEFB "Gracias. Por\n"
    DEFB "poco no lo consigo.",0
talk_white_3:
    DEFB "Por cierto, no he escuchado el\n"
    DEFB "lavabo, sólo la cisterna...",0
talk_white_4:
    DEFB "¿Eres consciente de\n"
    DEFB "que no tenemos manos?",0
talk_white_5:
    DEFB "¿Necesita algo?",0
talk_white_6:
    DEFB "Tengo que hacer algunas\n"
    DEFB "reparaciones. Y algo de\n"
    DEFB "comer también me vendría bien.",0
talk_white_6b:
    DEFB "Me temo que tenemos la\n"
    DEFB "comida muy racionada.",0
talk_white_7:
    DEFB "Bonita nave. ¿De carga, dijo?\n"
    DEFB "¿Qué tipo de carga?",0
talk_white_8:
    DEFB "Pues un poco de todo. Un poco\n"
    DEFB "de esto, otro de aquello...",0
talk_white_9:
    DEFB "Se ve un poco\n"
    DEFB "pequeña...",0
talk_white_10:
    DEFB "Es que sólo transporto estos\n"
    DEFB "y aquellos que sean pequeños.",0
talk_white_11:
    DEFB "Hmmm... Oiga ¿Tendría espacio\n"
    DEFB "para un acompañante?",0
talk_white_12:
    DEFB "¿Un acompañante...? Hmmm...\n"
    DEFB "Puede ser, pero con este hambre\n"
    DEFB "no hay quien tome una decisión.",0
talk_white_13:
    DEFB "¿Qué necesitaba para\n"
    DEFB "poder llevarme?",0
talk_white_14:
    DEFB "Me falta aún:",0
talk_white_15:
    DEFB "Un cristal de galena.",0
talk_white_16:
    DEFB "Alcohol.",0
talk_white_17:
    DEFB "Un mapa estelar.",0
talk_white_18:
    DEFB "Un generador de oxígeno.",0

talk_white_19:
	DEFB "Pues ya podemos\n"
    DEFB "irnos. ¿Vamos?",0
talk_white_20:
	DEFB "¡¡¡Claro!!!",0
talk_white_21:
    DEFB "Tengo cosas que hacer.",0
talk_white_22:
    DEFB "Hasta luego.",0



talk_white_generator1:
    DEFB "¿Servirá para\ngenerar oxígeno?",0
talk_white_generator2:
    DEFB "Buena idea.",0
talk_white_generator3:
    DEFB "Creo que es mejor que\n"
    DEFB "seamos sólo amigos.",0

talk_white_cristal1:
    DEFB "Tengo el cristal de galena.",0
talk_white_cristal2:
    DEFB "Ese cristal no sirve, es muy\n"
    DEFB "pequeño. Necesito uno grande.",0
talk_white_cristal3:
    DEFB "Es perfecto, pero está sucio.\n"
    DEFB "Hay que limpiarlo con algún\n"
    DEFB "disolvente. ¿Tienes alcohol?",0
talk_white_cristal4:
	DEFB "No se... voy a buscar.",0

talk_white_food1:
    DEFB "Ehm... Encontré\nesta comida.",0
talk_white_food2:
    DEFB "¡Oh! ¿Qué es?",0
talk_white_food3:
    DEFB "Es... uña de vaca.",0
talk_white_food4:
    DEFB "Sin duda el mejor bocado del\n"
    DEFB "mundo. No hay faisán que así\n"
    DEFB "me sepa. ¡Delicioso!",0
talk_white_food5:
    DEFB "¿Entonces,\nhay trato?",0
talk_white_food6:
    DEFB "El caso es que el cristal de\n"
    DEFB "galena del motor se ha dañado,\n"
    DEFB "y necesitaré uno nuevo.",0
talk_white_food7:
    DEFB "Y el sistema vital es justo\n"
    DEFB "para uno, así que necesitaría\n"
    DEFB "un generador de oxígeno extra.",0
talk_white_food8:
    DEFB "Además, mi mapa estelar es muy\n"
    DEFB "limitado, no podría llevarte\n"
    DEFB "muy lejos.",0
talk_ill_see_what_i_can_do:
    DEFB "Vaya... veré qué\n"
    DEFB "puedo hacer.",0
talk_white_food10:
    DEFB "Esa goma está demasiado\n"
    DEFB "cruda para mi gusto.",0

talk_white_maps1:
    DEFB "Ya tengo los mapas.",0
talk_white_maps2:
    DEFB "Vaya, edición de hace veinte\n"
    DEFB "años. Os va lo vintage...",0

sentence_give_no_maps:
    DEFB "¿Un diario vacío?\n"
    DEFB "No se qué hacer con él...",0


sentence_look_marvin:
    DEFB "Es Marvin, nuestro\n"
    DEFB "vecino marciano.",0
talk_marvin_1:
    DEFB "Hola Marvin.",0
talk_marvin_2:
    DEFB "Hola Ros.",0
talk_marvin_3:
	DEFB "¿Qué haces?",0
talk_marvin_4:
    DEFB "Descansar.\n"
    DEFB "Me he jubilado.",0
talk_marvin_5:
	DEFB "¿Has dejado la mina?",0
talk_marvin_6:
    DEFB "Sí. Doscientos años con\n"
    DEFB "el pico es mi límite.",0
talk_marvin_7:
	DEFB "¿Y qué vas a hacer\n"
    DEFB "a partir de ahora?",0
talk_marvin_8:
    DEFB "Descansar. Y también descansar.\n"
    DEFB "Y puede que incluso descanse.\n"
    DEFB "¿Te he dicho ya 'descansar'?",0
talk_marvin_9:
	DEFB "¿Me puedes conseguir\n"
    DEFB "un cristal de Galena?",0
talk_marvin_10:
	DEFB "Lo siento, estoy\n"
    DEFB "retirado. Tendrás que\n"
    DEFB "buscarlo tú mismo.",0
talk_marvin_11:
    DEFB "No se cómo buscar\n"
    DEFB "un cristal.",0
talk_marvin_12:
	DEFB "Toma, usa mi detector\n"
    DEFB "de galena. Yo ya no\n"
    DEFB "lo necesito.",0
talk_marvin_13:
    DEFB "Gracias... creo.",0
talk_marvin_14:
	DEFB "Hasta luego Marvin.",0
talk_marvin_15:
	DEFB "Disfruta.",0


sentence_take_galena_cristal:
    DEFB "¡Agh, me he roto una uña!\n"
    DEFB "Pero ya lo tengo.",0

sentence_already_have_galena_cristal:
    DEFB "No necesito más galena.",0

sentence_gave_alcohol_1:
    DEFB "Alcohol.",0
sentence_gave_alcohol_2:
    DEFB "Mmm... Sí, creo\n"
    DEFB "que servirá.",0


sentence_end1:
    DEFB "Bueno ¿y ahora\n"
    DEFB "a donde vamos?",0
sentence_end2:
    DEFB "Yo, aún no lo he\n"
    DEFB "decidido. Pero tú...",0
sentence_end3:
    DEFB "Esto no pinta\n"
    DEFB "nada bien...",0
sentence_end4:
    DEFB "Apaga el ordenador\n"
    DEFB "y vete a la cama.",0

; object and character names
name_yellow:
    DEFB "Amarill",0
name_red:
    DEFB "Roj",0
name_green:
    DEFB "Verd",0
name_blue:
    DEFB "Azu",0
name_white:
    DEFB "Blanc",0
name_marvin:
    DEFB "Marvin",0
name_reactor:
    DEFB "Reactor",0
name_radar:
    DEFB "Radar",0
name_grid:
    DEFB "Rejilla",0
name_box:
    DEFB "Caja",0
name_pot:
    DEFB "Olla",0
name_plant:
    DEFB "Chuck, la planta",0
name_locker:
    DEFB "Taquilla",0
name_diary:
    DEFB "Diario electrónico",0
name_usb_cable:
    DEFB "Cable USB",0
name_rubber_chicken:
    DEFB "Pollo de goma",0
name_galena_diode:
    DEFB "Diodo de galena",0
name_chicken:
    DEFB "Pollo",0
name_yellowchicken:
    DEFB "Amarill Al-Ast",0
name_galena_detector:
    DEFB "Detector de galena",0
name_galena_cristal:
    DEFB "Cristal de galena",0
name_iodine:
    DEFB "Bote de yodo",0
name_booze:
    DEFB "Aguardiente",0

sentence_look_booze:
    DEFB "No se cómo puede Verd beberse\n"
    DEFB "este desatascador de cañerías.",0




sentence_look_tinted_grid:
    DEFB "¡Es el zulo de Verd!",0




sentence_take_alcohol:
    DEFB "Cogeré una botella.",0
sentence_already_took_alcohol:
    DEFB "No necesito más.",0

sentence_swap_chicken1:
    DEFB "(Nada por aquí,\n"
    DEFB "nada por allá...)",0
sentence_swap_chicken2:
    DEFB "Ni lo intentes, te\n"
    DEFB "estoy vigilando.",0

sentence_cover_1:
    DEFB "Escape from M.O.N.J.A.S.",0
sentence_cover_2:
    DEFB "1. Jugar",0
sentence_cover_3:
    DEFB "2. Saltarse la intro",0
sentence_cover_5:
    DEFB "\c2021 Raster Software Vigo",0
sentence_cover_6:
    DEFB "http://www.rastersoft.com",0
sentence_cover_7:
    DEFB "Sinclair1/OPQA/Kempston: mover",0
sentence_cover_8:
    DEFB "Space/Enter/0/M/Z: menu",0


; verbs/commands
command_take:
    DEFB "Coger ",0
command_look:
    DEFB "Mirar ",0
command_talk:
    DEFB "Hablar ",0
command_use:
    DEFB "Usar ",0
command_give:
    DEFB "Dar ",0
command_return:
    DEFB "Volver",0
