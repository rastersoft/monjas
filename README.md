# ESCAPE FROM M.O.N.J.A.S.

## (Mars non-Orbiting Neutral Joint-Assignment Station)

This is the source code of Escape from M.O.N.J.A.S., a Lucasfilm-like
game for the Sinclair Spectrum 48K.

In this game, you work in the M.O.N.J.A.S.-type Epsylon 9 space station,
but you are tired of it and want to live big adventures outside.

You can download .TAP files ready to use from: <https://www.rastersoft.com/programas/monjas.html>

Note: in Spectrum +2A and +3 it must be loaded in 128K mode.

## License

This game, its source code and all the resources are distributed under the
EXPAT License (also known as MIT or X11 License). The details are in the
COPYING file. There are these exceptions to this rule:

* bas2tap.c: It's a program from ThunderWare Research Center (Martijn van der
             Heide) distributed under GPLv2 or later version.
* pic_converter.py: Distributed under GPLv3 only.
* bin2z80.py: Distributed under GPLv3 only.
* convert_sentences.py: Distributed under GPLv3 only.

## Compiling

You need PASMO, GCC and python3 to compile it. It seems that you can't compile
it from Windows (there seems to be some kind of incompatibility between my
Makefile file and the make command in CYGWIN). You can compile an specific language by
typing **make english** or **make spanish**. Doing **make** will create both.
The output will be something like this:

    Converting loading screen from PNG into ASM
    Creating output file loader.tap
    Done! Listing contains 1 line.
    -rw-r--r--. 1 raster raster 41511 abr 10 11:52 main_english.bin
    -rw-r--r--. 1 raster raster  8433 abr 10 11:52 test_eng.bin
    Max for main.bin: 41537 bytes
    Max for test.bin:  8730 bytes
    -rw-r--r--. 1 raster raster 41511 abr 10 11:52 main_spanish.bin
    -rw-r--r--. 1 raster raster  8624 abr 10 11:52 test_spa.bin
    Max for main.bin: 41537 bytes
    Max for test.bin:  8730 bytes

Compare the sizes of **main_XXXXX.bin** and **test_XXX.bin** with the maximum values. If
the size of **test_XXX.bin** is greater than 8730, then it won't work and you must
compress again the texts until the size is smaller. But if all the values are
right, then the .z80 and the .tap files are fine and can be played.

It uses the **bas2tap** program, by Martijn van der Heide (ThunderWare Research
Center) to generate the loader. The source code (under GPLv2 or later license),
is included in this repo. All the other utilities (pic_coverter.py to transform
a 256x192 picture into an .asm file ready to be compiled in a .tap file, or the
convert_sentences.py to compress the texts, are mine and distributed exclusively
under GPLv3 license).

### Compressing texts

Due to the big size of the texts in this game, they are compresed using a variant
of LZSS. If a byte has its seventh bit set to 0, it is managed as an ASCII code;
but if it is set to 1, then that byte and the next are joined, and a pair
(length, offset) is extracted. LENGTH is made with the bits 14, 13 and 12, and
the others, from 11 to 0, are assigned to OFFSET, and it means: copy LENGTH+3
bytes from START_ADDRESS + OFFSET. The compressor guarantees that there is
no recursivity (thus, the block pointed by (length, offset) will contain only
ASCII characters), and that the beginning of a sentence won't be compressed
(thus it is possible to uncompress a sentence without uncompressing the previous
ones). These characteristics allow to uncompress the texts on-the-fly.

The compressor is in the file **convert_sentences.py**, and it also replaces
some spanish characters (like ¡ ¿ and ñ) with ASCII ones, which are also replaced
in the character set, thus allowing to show those characters without having to
implement the whole LATIN-1.

The english and spanish texts are already available as compressed files in
**sentences_eng.asm** and **sentences_spa.asm**, and the original, uncompressed
texts are in **sentences_english.asm** and **sentences_english.asm**.

To recompress the texts just use

    make compress_spanish

for the spanish translation, or with

    make compress_english

for the english translation. This process requires a lot of time to ensure that
the compressor achieve the optimal size: since the compress ratio depends on the
order of the sentences, the compressor uses an iterative algorithm, swapping
the position of the sentences in the source code, recompressing, and comparing
the old size with the new. It uses a simulated annealing algorithm, where
sometimes it is allowed "to go backwards", but it will keep in memory always
the best compression value achieved.

Pressing Ctrl+C once will *reset* the algorithm
and will start again, but it will also remember the best ratio achieved and will
overwrite the current value only when a better ratio is achieved. This is useful
to "try another paths" if the compression stalls for a long time, but without
losing the job done.

Pressing Ctrl+C twice will save again the best compressed text and exit. The
time difference between both Ctrl+C presses must be less that 5 seconds. If not,
each press will just reset the algorithm.

The compressor requires pypy3. Don't try to use regular python3, it is too slow.

## Author

Sergio Costas  
Raster Software Vigo  
<https://www.rastersoft.com>  
rastersoft@gmail.com

## Thanks to

Michael Skinner, for supervising, reviewing and fixing the english
translation.

Pablo and Fernando Costas, Nacho and Fran Lamas, and Lars Höög for
testing the game.

And a very special thanks to Einar Saukas for pointing bugs, doing
great suggestions to enhance it, and helping so much to polish it.
