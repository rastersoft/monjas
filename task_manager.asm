; Copyright 2021 Sergio Costas Rodriguez (Raster Software Vigo)

; Permission is hereby granted, free of charge, to any person obtaining a copy of
; this software and associated documentation files (the "Software"), to deal in
; the Software without restriction, including without limitation the rights to
; use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is furnished to do
; so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

; this routine manages the tasks. Each task is called with IY pointing to its
; data block, but it is also stored between calls, so it can be overwritten if
; needed.
;
; Each task can call TASK_YIELD when it ended its job and wants to wait until
; the next frame. When that function is called, that task is slept and the next
; is called, until all the tasks have run. Then, a new frame is painted.
; After the frame has been painted, each task is called again, and continue
; running after the previous call to TASK_YIELD.
;
; Each task have its own stack while running, and its contents
; will be preserved between runs. But the registers won't be preserved after a call
; to TASK_YIELD. Also, the tasks MUST NEVER RETURN.
;
; If this function is called with the interrupts enabled, the ISR must use its own
; stack, to avoid overflowing the task's stacks (which are small).

task_run:
IF DEBUG_TASKS
    call reset_debug
    ld a, 255
    call do_debug
ENDIF
    ld (old_stack), SP
    ld iy, task_list
task_loop:
    ld a, (iy+0)
    and a
    jr z, task_end
    ld l, (iy+1)
    ld h, (iy+2)
    ld sp, hl
    ld (task_pointer), iy
    ret
task_end:
    ld sp, (old_stack)
    ret
task_yield:
    ld iy, (task_pointer)
    ld hl, 0
    add hl, sp ; get the current stack
    ld (iy+1), l
    ld (iy+2), h

IF DEBUG_TASKS
IF DODEBUG
    ; this piece of code searchs each stack to check how much
    ; free space remained. This is done by checking how many bytes with
    ; zero value are from the end. This works because the stacks are
    ; initialized to zero, but when a value is inserted, it will remain there,
    ; because POPing a value only increases SP.
    ; If the free space is 1 or 0, that means that, at least, the task have used
    ; the 100% of its stack (or, worse, it overwrote the top of the stack of the
    ; previous task), so it is a good idea to increase that stack until there are,
    ; at least, two free bytes.
    ld hl, (task_pointer)
    inc hl
    inc hl
    inc hl ; jump over the size and the stored SP pointer
    ld d, 0
task_check_size:
    ld a, (hl)
    and a
    jr nz, task_end_check_size
    inc d
    inc hl
    jr task_check_size
task_end_check_size:
    ld a, d
    ld SP, (old_stack)
    call do_debug
ENDIF
ENDIF

    ld e, (iy+0)
    ld d, 0
    add iy, de ; jump to next entry
    jp task_loop


; Tasks


; task2 manages the engineer
task2:
    call task_wait_random
    call task2_check_if_go_kitchen
    call task_check_if_in_conversation
    call task_move_to_and_wait_for_walk
    DEFW yellow_character
    DEFW 0x2012
    call task_wait_random
    call task2_check_if_go_kitchen
    call task_check_if_in_conversation
    call task_move_to_and_wait_for_walk
    DEFW yellow_character
    DEFW 0x1812
    jp task2

task2_check_if_go_kitchen:
    ld a, (flags4)
    bit 7, a ; FLAG4_7
    ret z
    ld hl, talk_yellow_4
    ld ix, yellow_character
    call make_talk_hl
    call task_move_to_and_wait_for_walk
    DEFW yellow_character
    DEFW 0x202A
    call task2_check_if_roasted
    call task_move_to_and_wait_for_walk
    DEFW yellow_character
    DEFW 0x5846
    call task2_check_if_roasted
    call task_move_to_and_wait_for_walk
    DEFW yellow_character
    DEFW 0x7846
    call task2_check_if_roasted
    ld b, 40
    call task_wait_B_frames
    call task2_check_if_roasted
    call task_move_to_and_wait_for_walk
    DEFW yellow_character
    DEFW 0x5828
    call task2_check_if_roasted
    call task_move_to_and_wait_for_walk
    DEFW yellow_character
    DEFW 0x2028
    call task2_check_if_roasted
    ld hl, flags4
    res 7, (hl) ; FLAG4_7
    call task_move_to_and_wait_for_walk
    DEFW yellow_character
    DEFW 0x2010
    call task2_check_if_roasted
    ret

task2_check_if_roasted:
    ld a, (flags3)
    bit 2, a ;FLAG3_2
    ret z
task2_check_if_roasted2:
    call task_yield
    jp task2_check_if_roasted2


; Task 3 manages the pot
task3:
    ld hl, sprite_pot1
    ld (entry_pot+4), hl
    ld b, 3
task3_1:
    push bc
    call random_number
    and 0x03
    or 0x38
    ld (entry_pot+3),a
    call task_yield
    pop bc
    djnz task3_1
    ld hl, sprite_pot2
    ld (entry_pot+4), hl
    ld b, 3
task3_2:
    push bc
    call random_number
    and 0x03
    or 0x38
    ld (entry_pot+3),a
    call task_yield
    pop bc
    djnz task3_2
    jp task3

; Task 4 manages the chef
task4:
    call task_wait_random
    call random_number
    bit 5, a
    jr z, task4_5
    call task_check_if_in_conversation
    call task_move_to_and_wait_for_walk
    DEFW red_character
    DEFW 0x6C42
    jr task4_2
task4_5:
    bit 4, a
    jr z, task4_6
    call task_check_if_in_conversation
    call task_move_to_and_wait_for_walk
    DEFW red_character
    DEFW 0x744E
    jr task4_2
task4_6:
    call task_check_if_in_conversation
    call task_move_to_and_wait_for_walk
    DEFW red_character
    DEFW 0x704A
task4_2:
    call task_wait_random
    ; return to the kitchen
    call task_check_if_in_conversation
    call task_move_to_and_wait_for_walk
    DEFW red_character
    DEFW 0x7442
    jr task4

; Task 5 manages the radar

task5:
    call task_yield
    ld a, (flags1)
    and 0x03 ; FLAG1_10
    cp 0x01
    jr nz, task5
task5_b1:
    ld a, (frame_counter)
    bit 2, a
    jr nz, task5_b2
    ld a, 255
    ld (entry_radar_screen), a
    call task_yield ; this delay is mandatory to synchronize the radar and the border
task5_b0:
    ld a, (border_sound_byte)
    and 0x18
    ld (border_sound_byte), a
    jr task5_b3
task5_b2:
    ld a, 1
    ld (entry_radar_screen), a
    call task_yield
    ld a, (do_repaint_flags)
    and 0x18
    jr nz, task5_b0 ; do not produce radar sound during an animation or a conversation REPFLAG_3 REPFLAG_4
    ld a, (flags6)
    bit 7, a ; FLAG6_7
    jr nz, task5_b3
    ld a, (border_sound_byte)
    and 0x18
    or 0x02
    ld (border_sound_byte), a
task5_b3:
    call task_yield
    ld a, (flags1)
    and 0x03 ; FLAG1_10
    cp 0x01
    jr z, task5_b1
    ld a, 255
    ld (entry_radar_screen), a
task5_b4:
    call task_yield
    jp task5_b4


; Task 6 manages the reactor
task6:
    call task_yield
    ld a, (flags5)
    bit 4, a ; FLAG5_4
    jr z, task6
    ld ix, entry_nucelar_reactor
    ld a, (ix+1)
    xor 0x01
    ld (ix+1), a
    ld a, (ix+7)
    xor 0x01
    ld (ix+7), a
    jp task6


; Task 7 manages the galena detector
task7:
    call task_yield
    ld a, (flags5)
    bit 7, a; FLAG5_7
    jr z, task7
    ld c, 0
task7_0:
    ld a, (main_character_x)
    and 0xF8
    rrca
    rrca
    rrca
    sub GALENA_POS_X
    jp p, task7_1
    neg
task7_1:
    ld b, a
    ld a, (main_character_y)
    and 0xF8
    rrca
    rrca
    rrca
    sub GALENA_POS_Y
    jp p, task7_2
    neg
task7_2:
    ld hl, flags6 ; FLAG6_1
    add a, b ; proportional to distance
    jr z, task7_3
    res 1, (hl) ; no galena cristal
    jr task7_4
task7_3:
    set 1, (hl)
task7_4:
    inc c
    cp c
    jr c, task7_7
    ld a, (border_sound_byte) ; A > C
    and 0x18
    ld (border_sound_byte), a
    jr task7_8
task7_7:
    ld a, (border_sound_byte) ; A > C
    and 0x18
    or 0x02
    ld (border_sound_byte), a
    ld c, 0
task7_8:
    push bc
    call task_yield
    pop bc
    ld a, (flags5)
    bit 7, a; FLAG5_7
    jr nz, task7_0
    ld hl, flags6
    res 1, (hl) ; FLAG6_1 Stop detecting galena
    ld a, (border_sound_byte)
    and 0x18
    ld (border_sound_byte), a ; stop any sound
    jr task7

; task8 manages Marvin
task8:
    ld hl, sprite_marvin_right
    ld (marvin_sprites + 10), hl
    ld b, 8
    call task_wait_B_frames
    ld hl, sprite_marvin_right0
    ld (marvin_sprites + 10), hl
    ld b, 8
    call task_wait_B_frames
    jr task8
